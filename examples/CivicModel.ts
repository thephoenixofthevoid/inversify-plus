import { fstat } from 'fs';
import { readFileSync, writeFileSync } from 'jsonfile';
import {
    createModelSchema,
    primitive,
    reference,
    serializeAll,
    list,
    object,
    identifier,
    serialize,
    deserialize,
    getDefaultModelSchema,
    serializable
} from "serializr"
import { Newable } from './inversify'


class Coordinates {
    @serializable                    chromosome                    : string
    @serializable                    start                         : number
    @serializable                    stop                          : number
    @serializable                    reference_bases               : string
    @serializable                    variant_bases                 : string
    @serializable                    representative_transcript     : string
    @serializable                    chromosome2                   : string
    @serializable                    start2                        : number
    @serializable                    stop2                         : number
    @serializable                    representative_transcript2    : string
    @serializable                    ensembl_version               : number
    @serializable                    reference_build               : string
}


class VariantType {
    @serializable    (identifier())  id                            : number
    @serializable                    name                          : string
    @serializable                    display_name                  : string
    @serializable                    so_id                         : string
    @serializable                    description                   : string
    @serializable                    url                           : string

    variants: Set<Variant> = new Set();
}

class Variant {
    @serializable    (identifier())  id                            : number
    @serializable                    type                          : string
    @serializable                    entrez_name                   : string
    @serializable                    entrez_id                     : number
    @serializable                    name                          : string
    @serializable                    description                   : string
    @serializable                    gene_id                       : number
    @serializable                    civic_actionability_score     : number
       
    // Relations
    @serializable   (list(object(VariantType)))  variant_types: VariantType[]
    @serializable   (object(Coordinates))         coordinates?: Coordinates
}

class Gene {

    @serializable   (identifier())               id                 : number
    @serializable                                name               : string
    @serializable                                description        : string
    @serializable                                type               : string
    @serializable                                entrez_id          : number
    @serializable   (list(object(Variant)))      variants           : Variant[]
    @serializable   (list(primitive()))          aliases            : string[]

}


class PublicationDate {
    @serializable year: number
    @serializable month: number
    @serializable day: number
}

class ClinicalTrials {
    @serializable (identifier())                 nct_id             : number
    @serializable                                name               : string
    @serializable                                description        : string
    @serializable                                clinical_trial_url : string
}

class Source {
    @serializable(object(PublicationDate))
    publication_date: PublicationDate

    @serializable(list(object(ClinicalTrials)))     
    clinical_trials: ClinicalTrials[]
    
    @serializable(identifier()) id : number
    @serializable name             : string
    @serializable source_type      : string
    @serializable open_access      : boolean
    @serializable is_review        : boolean
    @serializable journal          : string
    @serializable source_url       : string
    @serializable citation         : string
    @serializable citation_id      : string
    @serializable asco_abstract_id : string
    @serializable pmc_id           : string
    @serializable status           : string
    @serializable full_journal_title: string
}

class Disease {
    @serializable(identifier())   id            : number
    @serializable                 name          : string
    @serializable                 display_name  : string
    @serializable                 doid          : string
    @serializable                 url           : string
}


class Phenotype {
    @serializable (identifier())  id            : number
    @serializable                 ncit_id       : string
    @serializable                 hpo_class     : string
    @serializable                 url           : string
}

class Drug {
    @serializable (identifier())        id          : number
    @serializable                       ncit_id     : string
    @serializable                       name        : string
    @serializable (list(primitive()))   aliases     : string[]
}

class VariantGroup {
    @serializable   (identifier())               id                 : number
    @serializable                                name               : string
    @serializable                                description        : string
    @serializable                                type               : string
    @serializable   (list(object(Variant)))      variants           : Variant[]
}

class Evidence {

    @serializable   (identifier())              id                   : number
    @serializable   (object(Disease))           disease              : Disease
    @serializable   (object(Source))            source               : Source
    @serializable   (list(object(Drug)))        drugs                : Drug[]
    @serializable   (list(object(Phenotype)))   phenotypes           : Phenotype[]
    @serializable                               name                 : string
    @serializable                               description          : string
    @serializable                               type                 : string
    @serializable                               rating               : number
    @serializable                               evidence_level       : string
    @serializable                               evidence_type        : string
    @serializable                               clinical_significance: string
    @serializable                               evidence_direction   : string
    @serializable                               variant_origin       : string
    @serializable                               drug_interaction_type: string
    @serializable                               status               : string
    @serializable                               open_change_count    : number
    @serializable                               variant_id           : number

    variant?: Variant
}


class Assertions {

    @serializable(identifier()) id: number
    @serializable type: string

    @serializable(object(Gene)) gene: Gene
    @serializable(object(Variant)) variant: Variant
    @serializable(object(Disease)) disease: Disease
    @serializable(list(object(Drug))) drugs: Drug[]

    @serializable name: string
    @serializable summary: string
    @serializable description: string
    @serializable evidence_type: string
    @serializable evidence_direction: string
    @serializable clinical_significance: string
    @serializable fda_regulatory_approval: boolean
    @serializable status: string
    @serializable evidence_item_count :number
    @serializable open_change_count :number
    @serializable pending_evidence_count:number
}




const dataset: any = new Map()


function createFactory<T>(Constructor: Newable<T>, key: string, idkey: string = "id") {
    return function (context: any) {
        if (!dataset.has(key)) dataset.set(key, new Map())
        var variant = dataset.get(key).get(context.json[idkey])
        if (!variant) {
            variant = new Constructor();
            dataset.get(key).set(context.json[idkey], variant);
        }
        return variant;
    }
}



;getDefaultModelSchema(VariantType)     !.factory = createFactory(VariantType, "VariantType")
;getDefaultModelSchema(Variant)         !.factory = createFactory(Variant, "Variant")
;getDefaultModelSchema(Gene)            !.factory = createFactory(Gene, "Gene")
;getDefaultModelSchema(Drug)            !.factory = createFactory(Drug, "Drug")
;getDefaultModelSchema(Source)          !.factory = createFactory(Source, "Source")
;getDefaultModelSchema(Phenotype)       !.factory = createFactory(Phenotype, "Phenotype")
;getDefaultModelSchema(Evidence)        !.factory = createFactory(Evidence, "Evidence")
;getDefaultModelSchema(VariantGroup)    !.factory = createFactory(VariantGroup, "VariantGroup")
;getDefaultModelSchema(Disease)         !.factory = createFactory(Disease, "Disease")
;getDefaultModelSchema(Assertions)      !.factory = createFactory(Assertions, "Assertions")
;getDefaultModelSchema(ClinicalTrials)  !.factory = createFactory(ClinicalTrials, "ClinicalTrials", "nct_id")


function unwrap<T=any>({ records = [] }: { records: T[] }): T[] {
    return records
}

deserialize(Gene        , unwrap(readFileSync(`./datafiles/CIVIC-2020-10-08T01:37:14.295Z-DUMP/genes.json`)));
deserialize(Evidence    , unwrap(readFileSync(`./datafiles/CIVIC-2020-10-08T01:37:14.295Z-DUMP/evidence_items.json`)));
deserialize(Assertions  , unwrap(readFileSync(`./datafiles/CIVIC-2020-10-08T01:37:14.295Z-DUMP/assertions.json`)));
deserialize(VariantGroup, unwrap(readFileSync(`./datafiles/CIVIC-2020-10-08T01:37:14.295Z-DUMP/variant_groups.json`)));
deserialize(Variant     , unwrap(readFileSync(`./datafiles/CIVIC-2020-10-08T01:37:14.295Z-DUMP/variants.json`)));

//console.log(dataset.get(Variant).size)

const byName = new Map

dataset.get("Variant").forEach(function (variant: Variant) {
    const types: VariantType[] = variant.variant_types

    if (!byName.has(variant.name)) {
        byName.set(variant.name, new Set)
    }
    byName.get(variant.name).add(variant)
    types.forEach(t => t.variants.add(variant))
})

byName.forEach(function (V, N) {
    //if (V.size > 1) 
    //console.log(N, V.size)
})


const variants: any[] = []
const okvariants: any[] = []

dataset.get("Variant").forEach(function (variant: Variant) {
    if (!variant.coordinates?.variant_bases && !variant.coordinates?.reference_bases) {
        if (variant.coordinates?.stop && variant.coordinates?.start) {
            okvariants.push(variant)
            //console.log(variant.coordinates?.stop, variant.coordinates?.start, variant.coordinates?.stop- variant.coordinates?.start, variant.entrez_name, variant.name, variant.civic_actionability_score, variant.id);
        } else if (variant.name.match(/^[A-Z][0-9]+[A-Z]$/)) {
            //console.log(`${variant.entrez_name}:p.${variant.name}`)
        } else if (variant.name.match(/^[A-Z][^-]*(DEL|del|DUP)$/) || variant.name.match(/delins[A-Z]$/)) {
            console.log(`${variant.entrez_name}:p.${variant.name}`)
        } else if (variant.civic_actionability_score && !variant.variant_types.some(x => x.id == 120)) {
            variants.push(variant)
            //console.log(variant)
        }
    } else {
        okvariants.push(variant)
    }
})

console.log(okvariants.length)
console.log(variants.length)


writeFileSync("output.json", {variants})


//dataset.get("Evidence").forEach((element: Evidence) => {
//    element.variant = dataset.get("Variant").get(element.variant_id)
//    console.log(element)
//});

const counts: any = {}
const countsAll: any = {}

dataset.get("VariantType").forEach(function (variantType: VariantType) {
    //console.dir(variantType, { depth: 1 })

    // counts[variantType.name] = variantType.variants.size
    const variants = [ ...variantType.variants ]

    if (variantType.name == 'N/A') {
        variants.map(v => {
            counts[v.name] = counts[v.name] || 0;
            counts[v.name]++;
        })
    } else {
        variants.map(v => {
            countsAll[v.name] = countsAll[v.name] || 0;
            countsAll[v.name]++;
        })
    }


})