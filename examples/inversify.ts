export const DUPLICATED_INJECTABLE_DECORATOR = "Cannot apply @injectable decorator multiple times.";
export const DUPLICATED_METADATA = "Metadata key was used more than once in a parameter:";
export const NULL_ARGUMENT = "NULL argument";
export const KEY_NOT_FOUND = "Key Not Found";
export const AMBIGUOUS_MATCH = "Ambiguous match found for serviceIdentifier:";
export const CANNOT_UNBIND = "Could not unbind serviceIdentifier:";
export const NOT_REGISTERED = "No matching bindings found for serviceIdentifier:";
export const MISSING_INJECTABLE_ANNOTATION = "Missing required @injectable annotation in:";
export const MISSING_INJECT_ANNOTATION = "Missing required @inject or @multiInject annotation in:";
export const UNDEFINED_INJECT_ANNOTATION = (name: string) => `@inject called with undefined this could mean that the class ${name} has a circular dependency problem. You can use a LazyServiceIdentifer to overcome this limitation.`;
export const CIRCULAR_DEPENDENCY = "Circular dependency found:";
export const INVALID_BINDING_TYPE = "Invalid binding type:";
export const NO_MORE_SNAPSHOTS_AVAILABLE = "No snapshot available to restore.";
export const INVALID_MIDDLEWARE_RETURN = "Invalid return type in middleware. Middleware must return!";
export const INVALID_FUNCTION_BINDING = "Value provided to function binding must be a function!";
export const INVALID_TO_SELF_VALUE = "The toSelf function can only be applied when a constructor is used as service identifier";
export const INVALID_DECORATOR_OPERATION = "The @inject @multiInject @tagged and @named decorators must be applied to the parameters of a class constructor or a class property.";
export const ARGUMENTS_LENGTH_MISMATCH = (...values: any[]) => `The number of constructor arguments in the derived class ${values[0]} must be >= than the number of constructor arguments of its base class.`;
export const MULTIPLE_POST_CONSTRUCT_METHODS = "Cannot apply @postConstruct decorator multiple times in the same class";
export const POST_CONSTRUCT_ERROR = (...values: any[]) => `@postConstruct error in class ${values[0]}: ${values[1]}`;
export const CIRCULAR_DEPENDENCY_IN_FACTORY = (...values: any[]) => `It looks like there is a circular dependency in one of the '${values[0]}' bindings. Please investigate bindings with service identifier '${values[1]}'.`;
export const STACK_OVERFLOW = "Maximum call stack size exceeded";
export const CONTAINER_OPTIONS_MUST_BE_AN_OBJECT            = "Invalid Container constructor argument. Container options must be an object.";
export const CONTAINER_OPTIONS_INVALID_DEFAULT_SCOPE        = "Invalid Container option. Default scope must be a string ('singleton' or 'transient').";
export const CONTAINER_OPTIONS_INVALID_AUTO_BIND_INJECTABLE = "Invalid Container option. Auto bind injectable must be a boolean";
export const CONTAINER_OPTIONS_INVALID_SKIP_BASE_CHECK      = "Invalid Container option. Skip base check must be a boolean";

const NAMED_TAG         = Symbol("named");                    // Used for named bindings
const NAME_TAG          = Symbol("name");                     // The name of the target at design time
const UNMANAGED_TAG     = Symbol("unmanaged");                // The for unmanaged injections (in base classes when using inheritance)
const OPTIONAL_TAG      = Symbol("optional");                 // The for optional injections
const INJECT_TAG        = Symbol("inject");                   // The type of the binding at design time
const MULTI_INJECT_TAG  = Symbol("multi_inject");             // The type of the binding at design type for multi-injections
const TAGGED            = Symbol("inversify:tagged");         // used to store constructor arguments tags
const TAGGED_PROP       = Symbol("inversify:tagged_props");   // used to store class properties tags
const PARAM_TYPES       = Symbol("inversify:paramtypes");     // used to store types to be injected
const POST_CONSTRUCT    = Symbol("post_construct");           // used to identify postConstruct functions

export const METADATA_KEY = {
    NAMED_TAG, NAME_TAG, UNMANAGED_TAG, OPTIONAL_TAG, INJECT_TAG,
    MULTI_INJECT_TAG, TAGGED, TAGGED_PROP, PARAM_TYPES, POST_CONSTRUCT
};

export type TargetType = "ConstructorArgument" | "ClassProperty" | "Variable";

export interface Abstract<T> { prototype: T; }
export interface Clonable<T> { clone(): T; }
export interface Newable<T> { new (...args: any[]): T; }

export type ServiceIdentifier<T=any> = (string|symbol|Newable<T>|Abstract<T>);
export type Factory<T>         = (...args: any[]) => (((...args: any[]) => T) | T);
export type Provider<T>        = (...args: any[]) => (((...args: any[]) => Promise<T>) | Promise<T>);
export type ContextInterceptor = (context: Context) => Context;

export interface Next {
    (args: {
        avoidConstraints: boolean;
        contextInterceptor: ((contexts: Context) => Context);
        isMultiInject: boolean;
        targetType: TargetType;
        serviceIdentifier: ServiceIdentifier;
        key?: string | number | symbol;
        value?: any;
    }): any | any[];
}

export interface ConstructorMetadata {
    compilerGeneratedMetadata: Function[] | undefined;
    userGeneratedMetadata: { [key: string]: Metadata[]; };
}
export interface ConstraintFunction extends Function {
    metaData?: Metadata;
    (request: Request | null): boolean;
}

export type Bind    = <T>(id:ServiceIdentifier<T>) => BindingSyntax.BindingToSyntax<T>;
export type Rebind  = <T>(id:ServiceIdentifier<T>) => BindingSyntax.BindingToSyntax<T>;
export type Unbind  = <T>(id:ServiceIdentifier<T>) => void;
export type IsBound = <T>(id:ServiceIdentifier<T>) => boolean;
export interface ReflectResult { [key: string]: Metadata[]; }
export type ContainerModuleCallBack      = (bind: Bind, unbind: Unbind, isBound: IsBound, rebind: Rebind) => void;
export type AsyncContainerModuleCallBack = (bind: Bind, unbind: Unbind, isBound: IsBound, rebind: Rebind) => Promise<void>;
export type RequestScope = Map<any, any>;
export interface ContainerOptions { autoBindInjectable: boolean; defaultScope: BindingScope; skipBaseClassChecks: boolean; }
export type ResolveRequestHandler = (request: Request) => any;


let idCounter = 0;
export function id(): number {
  return idCounter++;
}


export type BindingScope = "Singleton" | "Transient" | "Request";
export type BindingType = "ConstantValue" | "Constructor" | "DynamicValue" | "Factory" | "Function" | "Instance" | "Invalid" | "Provider";


export function tagParameter(annotationTarget: any, propertyName: string, parameterIndex: number, metadata: Metadata) {
    if (typeof parameterIndex === "number" && propertyName !== undefined) throw new Error(INVALID_DECORATOR_OPERATION);
    var array = getDeepMetadataArray(TAGGED, annotationTarget, parameterIndex.toString())
    for (const m of array) if (m.key === metadata.key) 
        throw new Error(`${DUPLICATED_METADATA} ${m.key.toString()}`);
    array.push(metadata);
}

export function tagProperty(annotationTarget: any, propertyName: string, metadata: Metadata) {
    var array = getDeepMetadataArray(TAGGED_PROP, annotationTarget.constructor, propertyName)
    for (const m of array) if (m.key === metadata.key) 
        throw new Error(`${DUPLICATED_METADATA} ${m.key.toString()}`);
    array.push(metadata);
}

function getDeepMetadataArray(metadataKey: string|symbol, annotationTarget: any, key: string) {
    if (Reflect.hasOwnMetadata(metadataKey, annotationTarget)) {
        var paramsOrPropertiesMetadata: any = Reflect.getMetadata(metadataKey, annotationTarget)
    } else {
        var paramsOrPropertiesMetadata: any = {}
        Reflect.defineMetadata(metadataKey, paramsOrPropertiesMetadata, annotationTarget);
    }
    if (paramsOrPropertiesMetadata[key]) {
        return paramsOrPropertiesMetadata[key]
    } else {
        return paramsOrPropertiesMetadata[key] = [] as any
    }
}

// Allows VanillaJS developers to use decorators:
// decorate(injectable("Foo", "Bar"), FooBar);
// decorate(targetName("foo", "bar"), FooBar);
// decorate(named("foo"), FooBar, 0);
// decorate(tagged("bar"), FooBar, 1);
export function decorate(decorator: ClassDecorator|ParameterDecorator|MethodDecorator, target: any, parameterIndex?: number | string): void {

    if (typeof parameterIndex === "number") {
        Reflect.decorate([
            function (target: any, key: string) { 
                (decorator as ParameterDecorator)(target, key, parameterIndex); 
            } as any], target);
    } else if (typeof parameterIndex === "string") {
        Reflect.decorate([decorator as MethodDecorator], target, parameterIndex);
    } else {
        Reflect.decorate([decorator as ClassDecorator], target);
    }
}

function unifiedDecorator(target: any, targetKey: string, index: number|undefined, metadata: Metadata) {
    if (typeof index === "number") {
        tagParameter(target, targetKey, index, metadata);
    } else {
        tagProperty(target, targetKey, metadata);
    }
}

export function injectable {
    return function(target: any) {
        if (Reflect.hasOwnMetadata(PARAM_TYPES, target)) throw new Error(DUPLICATED_INJECTABLE_DECORATOR);
        Reflect.defineMetadata(PARAM_TYPES, Reflect.getMetadata("design:paramtypes", target) || [], target);
        return target;
    };
}
  
export function inject(serviceIdentifier: ServiceIdentifier | LazyServiceIdentifer) {
  return function(target: any, targetKey: string, index?: number): void {
    if (serviceIdentifier === undefined) throw new Error(UNDEFINED_INJECT_ANNOTATION(target.name));
    const metadata = new Metadata(INJECT_TAG, serviceIdentifier)
    unifiedDecorator(target, targetKey, index, metadata);
  }
}

// Used to add named metadata which is used to resolve name-based contextual bindings.
export function named(name: string | number | symbol) {
    return function(target: any, targetKey: string, index?: number) {
        unifiedDecorator(target, targetKey, index, new Metadata(NAMED_TAG, name));
    };
}

export function multiInject(serviceIdentifier: ServiceIdentifier) {
    return function(target: any, targetKey: string, index?: number) {
        unifiedDecorator(target, targetKey, index, new Metadata(MULTI_INJECT_TAG, serviceIdentifier));
    };
}

export function optional() {
    return function(target: any, targetKey: string, index?: number) {
        unifiedDecorator(target, targetKey, index, new Metadata(OPTIONAL_TAG, true));
    };
}

export function postConstruct() {
    return function (target: any, propertyKey: string, descriptor: PropertyDescriptor) {
        if (Reflect.hasOwnMetadata(POST_CONSTRUCT, target.constructor)) 
            throw new Error(MULTIPLE_POST_CONSTRUCT_METHODS);
        Reflect.defineMetadata(POST_CONSTRUCT, new Metadata(POST_CONSTRUCT, propertyKey), target.constructor);
    };
}

// Used to add custom metadata which is used to resolve metadata-based contextual bindings.
export function tagged(metadataKey: string | number | symbol, metadataValue: any) {
    return function(target: any, targetKey: string, index?: number) {
        unifiedDecorator(target, targetKey, index, new Metadata(metadataKey, metadataValue));
    };
}

export function targetName(name: string) {
    return function(target: any, targetKey: string, index: number) {
        tagParameter(target, targetKey, index, new Metadata(NAME_TAG, name));
    };
}

export function unmanaged() {
    return function(target: any, targetKey: string, index: number) {
        tagParameter(target, targetKey, index, new Metadata(UNMANAGED_TAG, true));
    };
}


export class LazyServiceIdentifer<T = any> {
    constructor(readonly unwrap: () => ServiceIdentifier<T>) {}
}



export function traverseAncerstors(request: Request, constraint: ConstraintFunction): boolean {
    while (request.parent !== null) {
        if (constraint(request = request.parent)) return true;
    }
    return false
};

export function taggedConstraint(key: string | number | symbol) {
    return function (value: any) {
        const constraint: ConstraintFunction = (request: Request | null) =>
            request !== null && request.target !== null && request.target.matchesTag(key, value);
        constraint.metaData = new Metadata(key, value);
        return constraint;
    }
}

export const namedConstraint = taggedConstraint(NAMED_TAG);
export function typeConstraint(type: (Function | string)) {
    return function (request: Request | null) {
        // Using index 0 because constraints are applied
        // to one binding at a time (see Planner class)
        if (request === null) return false;
        let binding = request.bindings[0];
        if (typeof type === "string") {
            return type === binding.serviceIdentifier;
        } else {
            return type === binding.implementationType;
        }
    };
}


export class Metadata {

    constructor(
        readonly key:string|number|symbol, 
        readonly value: any
    ) {}
    
    public toString() {
        if (this.key === NAMED_TAG) {
            return `named: ${this.value.toString()} `;
        }
        return `tagged: { key:${this.key.toString()}, value: ${this.value} }`;
    }

    static Named(metadata: string) {
        return new Metadata(NAMED_TAG, metadata)
    }
}

export class Binding<T=any> implements Clonable<Binding<T>> {

    public id: number = id();
    public moduleId: string;
    public activated: boolean = false;

    public implementationType: Newable<T> | null = null;
    public type: BindingType = "Invalid";
    public cache: T | null = null;
    public dynamicValue: ((context: Context) => T) | null = null;
    public factory     : ((context: Context) => Factory<T>)| null = null;
    public provider    : ((context: Context) => Provider<T>)| null = null;
    public constraint  : ConstraintFunction = () => true;
    public onActivation: ((context: Context, injectable: T) => T) | null = null;

    constructor(public serviceIdentifier: ServiceIdentifier<T>, public scope: BindingScope) {}

    public clone() {
        const clone = new Binding<T>(this.serviceIdentifier, this.scope);
        copyBindingParameters(clone, this);
        return clone;
    }
}

export class Request {

    readonly id: number = id();
    readonly children: Request[] = [];
    readonly context: Context;
    readonly parent: Request | null; 

    get avoidConstraints() {
        return false;
    }
    
    constructor(
        parent: Request | Context, 
        readonly bindings: Binding<any>[], 
        readonly target: Target
    ) {
        if (parent instanceof Request) {
            this.parent = parent
            this.context = parent.context
        } 
        if (parent instanceof Context) {
            this.parent = null
            this.context = parent;
        }
    }
}


export class Target {

    public id: number = id();
    public metadata:Metadata[] = []

    constructor(
        public type: TargetType,  
        public name: string, 
        public serviceIdentifier: ServiceIdentifier, 
        metadata: (Metadata|null)[] = []
    ) {
        metadata.forEach(item => {
            if (item === null) return;
            this.metadata.push(item);
        });
    }

    public hasTag(key: string|symbol): boolean {
        return this.metadata.some(m => m.key === key)
    }
    public matchesTag(key: string | number | symbol, value: any) {
        return this.metadata.some(m => m.key === key && m.value === value);
    }
    public isArray(): boolean {
        return this.hasTag(MULTI_INJECT_TAG);
    }
    public isNamed(): boolean {
        return this.hasTag(NAMED_TAG);
    }
    public matchesArray(name: ServiceIdentifier): boolean {
        return this.matchesTag(MULTI_INJECT_TAG, name);
    }
    public isTagged(): boolean {
        return this.metadata.some(isTagMetadata);
    }
    public isOptional(): boolean {
        return this.matchesTag(OPTIONAL_TAG, true);
    }
    public getNamedTag(): Metadata | null {
        return this.metadata.find((m) => m.key === NAMED_TAG) || null;
    }
    public getCustomTags(): Metadata[] | null {
        if (!this.isTagged()) return null;
        return this.metadata.filter(isTagMetadata);
    }
    public matchesNamedTag(name: string): boolean {
        return this.matchesTag(NAMED_TAG, name);
    }

    get serviceIdentifierString() {
        switch (typeof this.serviceIdentifier) {    
            case "function": return this.serviceIdentifier.name;
            case "symbol"  : return this.serviceIdentifier.toString();
            case "string"  : return this.serviceIdentifier
            default: return `${this.serviceIdentifier}`
        }
    }

    listMetadataForTarget(): string {
        if (!this.isTagged() && !this.isNamed()) return ` ${this.serviceIdentifierString}`;
    
        const tags = []
        const namedTag  = this.getNamedTag();
        const otherTags = this.getCustomTags();
    
        if (namedTag  !== null) tags.push(namedTag.toString())
        if (otherTags !== null) tags.push(...otherTags.map(it => it.toString()))
    
        return ` ${this.serviceIdentifierString}\n ${this.serviceIdentifierString} - ${tags.join("\n")}\n`;
    }
}

export class Context {
    public id: number = id();
    public currentRequest: Request;
    public rootRequest: Request;
    public requestScope: RequestScope = new Map<any, any>();

    public constructor(
        public container: Container,
        public avoidConstraints: boolean = false
    ) { }
}

export class Lookup<T extends Clonable<T>> {

    public map = new Map<ServiceIdentifier, T[]>();

    public add(id: ServiceIdentifier, value: T): void {
        if (id === null || id === undefined) throw new Error(NULL_ARGUMENT);
        if (value === null || value === undefined) throw new Error(NULL_ARGUMENT);
        const entry = this.map.get(id) || [];
        entry.push(value);
        this.map.set(id, entry);
    }

    public get(id: ServiceIdentifier): T[] {
        if (id === null || id === undefined) 
            throw new Error(NULL_ARGUMENT);
        const entry = this.map.get(id);
        if (entry) return entry;
        throw new Error(KEY_NOT_FOUND);
    }

    public remove(id: ServiceIdentifier): void {
        if (id === null || id === undefined) 
            throw new Error(NULL_ARGUMENT);
        if (!this.map.delete(id))
            throw new Error(KEY_NOT_FOUND);
    }

    public removeByCondition(condition: (item: T) => boolean): void {
        for (let [ key, array ] of this.map.entries()) {
            const updated = array.filter((entry) => !condition(entry));
            if (updated.length) {
                this.map.set(key, updated);
            } else {
                this.map.delete(key);
            }
        }
    }

    public hasKey(id: ServiceIdentifier): boolean {
        if (id === null || id === undefined) 
            throw new Error(NULL_ARGUMENT);
        return this.map.has(id);
    }

    public clone(): Lookup<T> {
        const copy = new Lookup<T>();

        for (let [ key, array ] of this.map.entries()) {
            for (let value of array) {
                copy.add(key, value.clone());
            }
        }

        return copy;
    }

    public clear(): void {
        this.map.clear()
    }

    public forEach(func: (key: ServiceIdentifier, value: T[]) => void): void {
        for (let [ key, array ] of this.map.entries()) {
            func(key, array)
        }
    }

}

export class MetadataReader {

    public getConstructorMetadata(constructorFunc: Function): ConstructorMetadata {
        return { 
            compilerGeneratedMetadata: Reflect.getMetadata(PARAM_TYPES, constructorFunc),
            userGeneratedMetadata    : Reflect.getMetadata(TAGGED     , constructorFunc) || {}
        };
    }
    public getPropertiesMetadata(constructorFunc: Function): { [key: string]: Metadata[]; } {
        return Reflect.getMetadata(TAGGED_PROP, constructorFunc) || [];;
    }

    public isAutoInjectable(serviceIdentifier: ServiceIdentifier) {
        if (typeof serviceIdentifier === "function") {
            const metadata = this.getConstructorMetadata(serviceIdentifier)
            return !!metadata.compilerGeneratedMetadata
        }
        return false;
    }
}

export class ContainerModule {
    public id: number = id();
    constructor(public registry: ContainerModuleCallBack) {}
}

export class AsyncContainerModule {
    public id: number = id();
    constructor(public registry: AsyncContainerModuleCallBack) {}
}

export class ContainerSnapshot {
    constructor (public bindings: Lookup<Binding<any>>, public middleware: Next) {}
    static of(container: ContainerSnapshotMixin) {
        return new ContainerSnapshot(container.bindings.clone(), container.middleware);
    }
}


function validateAndResolveOptions(options: Partial<ContainerOptions>): ContainerOptions {
    if (typeof options !== "object") throw new Error(CONTAINER_OPTIONS_MUST_BE_AN_OBJECT);
    const { autoBindInjectable  = false, skipBaseClassChecks = false, defaultScope = "Transient" } = options;
    const BindingScopeEnum = { Request: true, Singleton: true, Transient: true };
    if (!(defaultScope in BindingScopeEnum))      throw new Error(CONTAINER_OPTIONS_INVALID_DEFAULT_SCOPE);
    if (typeof autoBindInjectable  !== "boolean") throw new Error(CONTAINER_OPTIONS_INVALID_AUTO_BIND_INJECTABLE);
    if (typeof skipBaseClassChecks !== "boolean") throw new Error(CONTAINER_OPTIONS_INVALID_SKIP_BASE_CHECK);

    return { autoBindInjectable, defaultScope, skipBaseClassChecks };
}


function copyBindingParameters<T>(target: Binding<T>, source: Binding<T>) {
    target.implementationType = source.implementationType;
    target.scope              = source.scope;
    target.type               = source.type;
    target.cache              = source.cache;
    target.dynamicValue       = source.dynamicValue;
    target.factory            = source.factory;
    target.provider           = source.provider;
    target.constraint         = source.constraint;
    target.onActivation       = source.onActivation;
}



function copyDictionary(origin: Lookup<Binding>, destination: Lookup<Binding>) {
    origin.forEach((key, value) => {
        value.forEach((binding) => {
            destination.add(key, binding.clone());
        });
    });
}

/**-----------------------------------------------------------------------------------------------*/
/**------------------------------------- DANGER ZONE ---------------------------------------------*/
/**-----------------------------------------------------------------------------------------------*/

export function plan(container: Container, isMultiInject: boolean,  targetType: TargetType, 
                     serviceIdentifier: ServiceIdentifier, key?: string|number|symbol,  value?: any, avoidConstraints = false): Context {

    const target = new Target(targetType, "", serviceIdentifier, [
        isMultiInject ? new Metadata(MULTI_INJECT_TAG, serviceIdentifier) : 
                        new Metadata(INJECT_TAG, serviceIdentifier),
        key !== undefined ? new Metadata(key, value) : null
    ]);

    var context = new Context(container, avoidConstraints);

    try {
        context.rootRequest = resolveTarget(context, target);
        getChildRequest(context.rootRequest);
      
    } catch (error) {
        if (error instanceof RangeError || error.message === STACK_OVERFLOW) {
            circularDependencyToException(context.rootRequest);
        } else {
            throw error;
        }
    }

    return context;
}




export class Container {

    public id: number = id();
    public parent: Container|null = null;
    public readonly options: ContainerOptions;

    public middleware: Next = ({ isMultiInject, targetType, serviceIdentifier, key, value, avoidConstraints, contextInterceptor }) => {
        var context = plan(this, isMultiInject, targetType, serviceIdentifier, key, value, avoidConstraints)
        context = contextInterceptor(context);
        return resolve.context(context);
    };

    public bindings = new Lookup<Binding<any>>();

    public metadataReader = new MetadataReader();

    public static merge(container1: Container, container2: Container): Container {
        const container = new Container();
        copyDictionary(container1.bindings,  container.bindings);
        copyDictionary(container2.bindings,  container.bindings);
        return container;
    }

    constructor(options: Partial<ContainerOptions> = {}) {
        this.options = validateAndResolveOptions(options);
    }

    public load(...modules: ContainerModule[]) {
        for (const currentModule of modules) registryContainerModule(this, currentModule);
    }

    public async loadAsync(...modules: AsyncContainerModule[]) {
        for (const currentModule of modules) await registryContainerModule(this, currentModule);
    }

    public unload(...modules: ContainerModule[]): void {
        for (const currentModule of modules) 
        deregistryContainerModule(this, currentModule);
    }

    // Registers a type binding
    public bind<T>(serviceIdentifier: ServiceIdentifier<T>){
        const scope = this.options.defaultScope;
        const binding = new Binding<T>(serviceIdentifier, scope);
        this.bindings.add(serviceIdentifier, binding);
        return new BindingSyntax.BindingToSyntax<T>(binding);
    }

    public rebind<T>(serviceIdentifier: ServiceIdentifier<T>) {
        this.unbind(serviceIdentifier);
        return this.bind(serviceIdentifier);
    }

    // Removes a type binding from the registry by its key
    public unbind(serviceIdentifier: ServiceIdentifier): void {
        try {
            this.bindings.remove(serviceIdentifier);
        } catch (e) {
            throw new Error(`${CANNOT_UNBIND} ${getServiceIdentifierAsString(serviceIdentifier)}`);
        }
    }

    // Removes all the type bindings from the registry
    public unbindAll(): void {
        this.bindings.clear();
    }

    public createChild(containerOptions?: Partial<ContainerOptions>): Container {
        const child = new Container(containerOptions || this.options);
        child.parent = this;
        return child;
    }

    public applyMiddleware(...middlewares: ((next: Next) => Next)[]): void {
        this.middleware = middlewares.reduce((prev, curr) => curr(prev), this.middleware);
    }
    public _get<T>(avoidConstraints: boolean, isMultiInject: boolean, targetType: TargetType, 
        serviceIdentifier:ServiceIdentifier, key?: string | number | symbol, value?: any): (T | T[]) {
        const contextInterceptor = (context: Context) => context;
        let result: (T | T[]) | null = this.middleware({ avoidConstraints, contextInterceptor, isMultiInject, key, serviceIdentifier, targetType, value });
        if (result === undefined || result === null) throw new Error(INVALID_MIDDLEWARE_RETURN);
        return result;
    }


    public resolve<T>(constructorFunction: Newable<T>) {
        const tempContainer = new Container(this.options);
        tempContainer.parent = this;
        tempContainer.bind<T>(constructorFunction).toSelf();
        return tempContainer.get<T>(constructorFunction);
    }

    /**
     * TODO Check if it's intenrional
     */
    listRegisteredBindingsForServiceIdentifier(serviceIdentifier: string): string {
        let container = this;
        let registeredBindings: Binding<this>[] = [];
        do {
            if (container.bindings.hasKey(serviceIdentifier)) {
                registeredBindings.unshift(...container.bindings.get(serviceIdentifier));
            }
        } while (!registeredBindings.length && (container = container.parent as any))


        if (registeredBindings.length === 0) return '';
        let str = "\nRegistered bindings:";
    
        for (let binding of registeredBindings) {
            let name = binding.implementationType === null ? "Object"
                : getFunctionName(binding.implementationType);
    
            if (binding.constraint.metaData) {
                str += `\n ${name} - ${binding.constraint.metaData}`;
            } else {
                str += `\n ${name}`;
            }
        }
    
        return str;
    }
}


class ContainerSnapshotMixin {
    middleware: Next;
    bindings: Lookup<Binding<any>>;
    snapshots: ContainerSnapshot[];

    snapshot(): void {
        if (!this.snapshots) this.snapshots = []
        this.snapshots.push(ContainerSnapshot.of(this));
    }

    restore(): void {
        if (!this.snapshots) this.snapshots = []
        const snapshot = this.snapshots.pop();
        if (!snapshot) throw new Error(NO_MORE_SNAPSHOTS_AVAILABLE);
        this.bindings = snapshot.bindings as any;
        this.middleware = snapshot.middleware;
    }
}
export interface Container extends ContainerSnapshotMixin {}
copyPrototype(ContainerSnapshotMixin, Container); // Mixin


class ContainerGetters {
    bindings: Lookup<Binding<any>>;
    parent: Container|null;
    _get:<T>(avoidConstraints: boolean, isMultiInject: boolean, t: TargetType, id:ServiceIdentifier, key?: string|number|symbol, value?: any) => (T | T[]);

    // Allows to check if there are bindings available for serviceIdentifier
    public isBound(serviceIdentifier: ServiceIdentifier): boolean {
        return this.bindings.hasKey(serviceIdentifier) || 
                         this.parent?.isBound(serviceIdentifier) || false;
    }
    public isBoundNamed(serviceIdentifier: ServiceIdentifier, named: string | number | symbol): boolean {
        return isBoundTagged(this as any, serviceIdentifier, NAMED_TAG, named);
    }
    // Check if a binding with a complex constraint is available without throwing a error. Ancestors are also verified.
    public isBoundTagged(serviceIdentifier: ServiceIdentifier, key: string | number | symbol, value: any): boolean {
        return isBoundTagged(this as any, serviceIdentifier, key, value)
    }


    public get<T>(serviceIdentifier: ServiceIdentifier<T>): T {
        return this._get<T>(false, false, "Variable", serviceIdentifier) as T;
    }
    public getTagged<T>(serviceIdentifier: ServiceIdentifier<T>, key: string | number | symbol, value: any): T {
        return this._get<T>(false, false, "Variable", serviceIdentifier, key, value) as T;
    }
    public getNamed<T>(serviceIdentifier: ServiceIdentifier<T>, named: string | number | symbol): T {
        return this._get<T>(false, false, "Variable", serviceIdentifier, NAMED_TAG, named) as T;
    }
    public getAll<T>(serviceIdentifier: ServiceIdentifier<T>): T[] {
        return this._get<T>(true, true, "Variable", serviceIdentifier) as T[];
    }
    public getAllTagged<T>(serviceIdentifier: ServiceIdentifier<T>, key: string | number | symbol, value: any): T[] {
        return this._get<T>(false, true, "Variable", serviceIdentifier, key, value) as T[];
    }
    public getAllNamed<T>(serviceIdentifier: ServiceIdentifier<T>, named: string | number | symbol): T[] {
        return this._get<T>(false, true, "Variable", serviceIdentifier, NAMED_TAG, named) as T[];
    }
}
export interface Container extends ContainerGetters {}
copyPrototype(ContainerGetters, Container); // Mixin



function getBindings<C extends { bindings: Lookup<Binding<any>>, parent: Container|null  }, T>(container: C, target: Target): Binding<T>[] {
    const isMultiInject = target.isArray()
    const serviceId = target.serviceIdentifier;

    let bindings: Binding<T>[] = [];
    do {
        if (container.bindings.hasKey(serviceId)) {
            bindings.unshift(...container.bindings.get(serviceId));
        }
        if (bindings.length && !isMultiInject) {
            return bindings;
        }
    } while (container = container.parent as any)
    return bindings;    
}

function isBoundTagged(container: Container|null, serviceIdentifier: ServiceIdentifier, key: string | number | symbol, value: any): boolean {
    while (container) {
        // verify if there are bindings available for serviceIdentifier on current binding dictionary
        if (container.bindings.hasKey(serviceIdentifier)) {
            const bindings = container.bindings.get(serviceIdentifier);
            const context  = new Context(container)
            const target   = new Target("Variable", "", serviceIdentifier, [new Metadata(key, value)])
            const request  = new Request(context, [], target );
            if (bindings.some((b) => b.constraint(request))) return true
        }

        container = container.parent;
    }
    return false;
}




function registryContainerModule(C: Container, currentModule: ContainerModule|AsyncContainerModule) {
    function o(bindingToSyntax: any) {
        (bindingToSyntax as any)._binding.moduleId = currentModule.id;
        return bindingToSyntax;
    }
    return currentModule.registry(id => o(C.bind(id)), id => C.unbind(id), id => C.isBound(id), id => o(C.rebind(id)));
}

export function deregistryContainerModule(container: Container, currentModule: ContainerModule|AsyncContainerModule) {
    container.bindings.removeByCondition(item => (item.moduleId as any) === currentModule.id);
}

export function getServiceIdentifierAsString(serviceIdentifier: ServiceIdentifier): string {
    switch (typeof serviceIdentifier) {    
        case "function": return serviceIdentifier.name;
        case "symbol"  : return serviceIdentifier.toString();
        case "string"  : return serviceIdentifier
        default: return `${serviceIdentifier}`
    }
}

export function circularDependencyToException(request: Request) {
    const order = [ request ];
    let current2: Request | null = null;

    while (order.length) {
        request = order.shift() as Request;
        order.unshift(...request.children);

        let current: Request | null = request;
        while (!current2 && (current = current.parent))
            if (current.target.serviceIdentifier === request.target.serviceIdentifier) 
                current2 = request;
    }

    const parts: string[] = []
    while (current2) {
        const id = current2.target.serviceIdentifier;
        switch (typeof id) {    
            case "function": parts.unshift(id.name); break;
            case "symbol"  : parts.unshift(id.toString()); break;
            case "string"  : parts.unshift(id); break;
            default:  parts.unshift(`${id}`)
        }
        current2 = current2.parent
    }

    throw new Error(`${CIRCULAR_DEPENDENCY} ${parts.join(" --> ")}`);
}



export function getFunctionName(v: any): string {
    if (v.name) return v.name;

    const name = v.toString();
    const match = name.match(/^function\s*([^\s(]+)/);
    return match ? match[1] : `Anonymous function: ${name}`;
}

export function getClassDependencies(metadataReader: MetadataReader, currentConstructor: Function, isBaseClass: boolean): Target[] {
    const constructorName = getFunctionName(currentConstructor)
    const metadata = metadataReader.getConstructorMetadata(currentConstructor);

    if (!metadata.compilerGeneratedMetadata) // All types resolved must be annotated with @injectable
        throw new Error(`${MISSING_INJECTABLE_ANNOTATION} ${constructorName}.`);

    const keys = Object.keys(metadata.userGeneratedMetadata);
    const hasUserDeclaredUnknownInjections = (currentConstructor.length === 0 && keys.length > 0);
    const iterations = (hasUserDeclaredUnknownInjections) ? keys.length : currentConstructor.length;

    // Target instances that represent constructor arguments to be injected
    const targets: Target[] = [];
    for (let index = 0; index < iterations; index++) {
        const serviceIdentifiers = metadata.compilerGeneratedMetadata;
        const constructorArgsMetadata = metadata.userGeneratedMetadata;
        const targetMetadata = constructorArgsMetadata[index.toString()] || [];

        let targetName: any;
        let unmanaged: any;
        let id: any = serviceIdentifiers[index];

        targetMetadata.forEach((m: Metadata) => {
            switch (true) {
                case m.key == INJECT_TAG: id = m.value; break;
                case m.key == MULTI_INJECT_TAG: id = m.value; break;
                case m.key == NAME_TAG: targetName = m.value; break;
                case m.key == UNMANAGED_TAG: unmanaged = m.value; break;
            }
        });
         // we unwrap LazyServiceIdentifer wrappers to allow circular dependencies on symbols
        if (id instanceof LazyServiceIdentifer) id = id.unwrap();
        if (unmanaged === true) continue;

        if (!isBaseClass && (id === Object || id === Function || id === undefined)) throw new Error(
            `${MISSING_INJECT_ANNOTATION} argument ${index} in class ${constructorName}.`)

        targets.push(new Target("ConstructorArgument", targetName, id, targetMetadata));
    }
    // Target instances that represent properties to be injected

    while (currentConstructor !== Object) {
        const classPropsMetadata = metadataReader.getPropertiesMetadata(currentConstructor);
        for (const key in classPropsMetadata) {
            const targetMetadata = classPropsMetadata[key]

            let id: string = key
            let targetName: string = key;

            targetMetadata.forEach((m: Metadata) => {
                switch (true) {
                    case m.key == INJECT_TAG      : id = m.value; break;
                    case m.key == MULTI_INJECT_TAG: id = m.value; break;
                    case m.key == NAME_TAG        : targetName = m.value; break;
                }
            });
            targets.push(new Target("ClassProperty", targetName, id, targetMetadata));
        }
        currentConstructor = Object.getPrototypeOf(currentConstructor.prototype).constructor;
    }

    return targets;
}


function isTagMetadata(m: Metadata) {
    switch (m.key) {
        case INJECT_TAG      : return false;
        case MULTI_INJECT_TAG: return false;
        case NAME_TAG        : return false;
        case UNMANAGED_TAG   : return false;
        case NAMED_TAG       : return false;
    }
    return true;
}



function resolveTarget(parent: Request|Context, target: Target) {
    const isMultiInject = target.isArray()
    const context = parent instanceof Context ? parent : parent.context;
    const { container } = context;

    let bindings = getBindings(container, target);

    if (bindings.length === 0 && container.options.autoBindInjectable && container.metadataReader.isAutoInjectable(target.serviceIdentifier)) {
        container.bind(target.serviceIdentifier).toSelf();
        bindings = getBindings(container, target);
    }

    if (!parent.avoidConstraints) {
        bindings = bindings.filter((B) => B.constraint(new Request(parent, [B], target)));
    } 

    if (bindings.length === 0 && !target.isOptional()) throw new Error([ NOT_REGISTERED,
        target.listMetadataForTarget(),
        container.listRegisteredBindingsForServiceIdentifier(target.serviceIdentifierString)
    ].join(""));
    

    if (bindings.length > 1 && !isMultiInject) throw new Error([
        `${AMBIGUOUS_MATCH} ${target.serviceIdentifierString}`,
        container.listRegisteredBindingsForServiceIdentifier(target.serviceIdentifierString)
    ].join(""));
    

    return new Request(parent, bindings, target);;
}

// Throw if a derived class does not implement its constructor explicitly
// We do this to prevent errors when a base class (parent) has dependencies
// and one of the derived classes (children) has no dependencies
function baseClassDependencyCountCheck(metadataReader: MetadataReader, implementation: Newable<any>, dependencies: Target[]) {
    let currentConstructor = Object.getPrototypeOf(implementation.prototype).constructor;
    let baseClassDependencyCount = 0;

    while (currentConstructor !== Object && baseClassDependencyCount <= 0) {
        const targets = getClassDependencies(metadataReader, currentConstructor, true);
        baseClassDependencyCount = targets.length;
        for (let t of targets) for (let m of t.metadata) if (m.key === UNMANAGED_TAG) baseClassDependencyCount--;
        currentConstructor =  Object.getPrototypeOf(currentConstructor.prototype).constructor;
    }

    if (dependencies.length < baseClassDependencyCount) {
        throw new Error(ARGUMENTS_LENGTH_MISMATCH(getFunctionName(implementation)));
    }
}


function getChildRequest(request: Request) {
    const isMultiInject = request.target.isArray()

    if (isMultiInject) {
        request.bindings.forEach(binding => {
            let childRequest = new Request(request, [binding], request.target);
            request.children.push(childRequest);
            processInstanceDependencies(childRequest);
        })
    } else {
        const binding = request.bindings[0]
        if (!binding || binding.cache) return;
        processInstanceDependencies(request);
    }
}

function processInstanceDependencies(request: Request) {
    const binding = request.bindings[0];

    if (binding.type === "Instance" && binding.implementationType !== null) {
        const dependencies = getClassDependencies(request.context.container.metadataReader, binding.implementationType, false);
        if (!request.context.container.options.skipBaseClassChecks) {
            baseClassDependencyCountCheck(request.context.container.metadataReader, binding.implementationType, dependencies);
        }
    
        dependencies.forEach((dependency: Target) => {
            const child = resolveTarget(request, dependency);
            request.children.push(child);
            getChildRequest(child);
        });
    }
}



/**-----------------------------------------------------------------------------------------------*/
/**---------------------------------- DANGER ZONE END --------------------------------------------*/
/**-----------------------------------------------------------------------------------------------*/



export namespace resolve {

    export function context<T>(context: Context): T {
        return resolve.request(context.rootRequest)
    }

    export function request(r: Request): any {
        r.context.currentRequest = r;

        let shouldCreateArray = true;

        if (!r.target.isArray()) {
            shouldCreateArray = false;
        } else if (r.parent && r.parent.target.matchesArray(r.target.serviceIdentifier)) {
            shouldCreateArray = false;
        }

        if (shouldCreateArray) return r.children.map(request);

        const isOptional = r.target.isOptional()
        if (isOptional && r.bindings.length === 0) return undefined;

        const binding = r.bindings[0];
        switch (binding.scope) {
            case "Singleton": return Singleton(r, binding);
            case "Request"  : return Request  (r, binding);
            case "Transient": return Transient(r, binding);
        }
    }



    function Transient(request: Request, binding: Binding) {
        return Common(request, binding);
    }

    function Singleton(request: Request, binding: Binding) {
        if (binding.activated) return binding.cache;    
        const result = Common(request, binding);
        binding.activated = true;
        return binding.cache = result;
    }

    function Request(request: Request, binding: Binding) {
        const o = request.context.requestScope;
        if (o.has(binding.id)) return o.get(binding.id)
        const result = Common(request, binding);
        if (!o.has(binding.id)) o.set(binding.id, result)
        return result;
    }

    function Common(request: Request, binding: Binding) {
        let result = createResolvedInstance(request, binding);
        if (!binding.onActivation) return result;
        return binding.onActivation(request.context, result);
    }
    function createResolvedInstance(request: Request, binding: Binding<any>) {
        switch (binding.type) {
            case "ConstantValue": return binding.cache;
            case "Function"     : return binding.cache;
            case "Constructor"  : return binding.implementationType;
            case "DynamicValue" : return DynamicValue(request, binding);
            case "Factory"      : return Factory     (request, binding);
            case "Provider"     : return Provider    (request, binding);
            case "Instance"     : return Instance    (request, binding);
        }
        invokeError(request);
    }

    function DynamicValue(request: Request, binding: Binding<any>) {
        if (!binding.dynamicValue) return invokeError(request);
        const fn = () => binding.dynamicValue!(request.context); 
        return invokeFactory("toDynamicValue", binding.serviceIdentifier, fn);
    }

    function Factory(request: Request, binding: Binding<any>) {
        if (!binding.factory) return invokeError(request);
        const fn = () => binding.factory!(request.context);
        return invokeFactory("toFactory", binding.serviceIdentifier, fn);
    }

    function Provider(request: Request, binding: Binding<any>) {
        if (!binding.provider) return invokeError(request);
        const fn = () => binding.provider!(request.context);
        return invokeFactory("toProvider", binding.serviceIdentifier, fn);
    }

    function Instance(request: Request, binding: Binding<any>) {
        if (binding.implementationType === null) return invokeError(request);
        return invokeInstance(binding.implementationType, request.children)
    }

    function invokeError(request: Request) {
        // The user probably created a binding but didn't finish it
        // e.g. container.bind<T>("Something"); missing BindingToSyntax
        const id = request.target.serviceIdentifier;
        switch (typeof id) {    
            case "function": throw new Error(`${INVALID_BINDING_TYPE} ${id.name}`);
            default: throw new Error(`${INVALID_BINDING_TYPE} ${id.toString()}`)
        }
    }

    function invokeFactory(factoryType: "toDynamicValue"|"toFactory"|"toAutoFactory"|"toProvider", serviceIdentifier: ServiceIdentifier, fn: () => any) {
        try { 
            return fn();
        } catch (error) {
            if (error instanceof RangeError || error.message === STACK_OVERFLOW) {
                throw new Error(CIRCULAR_DEPENDENCY_IN_FACTORY(factoryType, serviceIdentifier.toString()));
            } else {
                throw error;
            }
        }
    };


    export function invokeInstance(constr: Newable<any>, children: Request[]): any {
        const propInjectionsFilter = (req: Request) => req.target?.type === "ClassProperty";
        const argsInjectionsFilter = (req: Request) => req.target?.type === "ConstructorArgument";

        const argumentInjections = children.filter(argsInjectionsFilter);
        const propertyInjections = children.filter(propInjectionsFilter);
        const result = Reflect.construct(constr, argumentInjections.map(resolve.request));
        const postcMetadata: Metadata = Reflect.getMetadata(POST_CONSTRUCT, constr);
        
        propertyInjections.forEach(function(r: Request) {
            result[r.target.name] = resolve.request(r)
        });

        try {
            if (postcMetadata) result[postcMetadata.value]();
        } catch (e) {
            throw new Error(POST_CONSTRUCT_ERROR(constr.name, e.message));
        }

        return result;
    }

}

export namespace BindingSyntax {
    class BindingSyntax<T> {
        public constructor(protected _binding: Binding<T>) {}
    }

    export class BindingToSyntax<T> extends BindingSyntax<T> {
        to(constructor: new (...args: any[]) => T): BindingInWhenOnSyntax<T> {
            this._binding.type = "Instance";
            this._binding.implementationType = constructor;
            return new BindingInWhenOnSyntax<T>(this._binding);
        }
        toSelf(): BindingInWhenOnSyntax<T> {
            if (typeof this._binding.serviceIdentifier !== "function") throw new Error(INVALID_TO_SELF_VALUE);
            return this.to(this._binding.serviceIdentifier);
        }
        toConstantValue(value: T) {
            this._binding.type = "ConstantValue";
            this._binding.cache = value;
            this._binding.dynamicValue = null;
            this._binding.implementationType = null;
            return new BindingWhenOnSyntax<T>(this._binding);
        }
        toDynamicValue(func: (context: Context) => T) {
            this._binding.type = "DynamicValue";
            this._binding.cache = null;
            this._binding.dynamicValue = func;
            this._binding.implementationType = null;
            return new BindingInWhenOnSyntax<T>(this._binding);
        }
        toConstructor<T2>(constructor: Newable<T2>): BindingWhenOnSyntax<T> {
            this._binding.type = "Constructor";
            this._binding.implementationType = constructor as any;
            return new BindingWhenOnSyntax<T>(this._binding);
        }
        toFactory<T2>(factory: ((context: Context) => Factory<T2>)): BindingWhenOnSyntax<T> {
            this._binding.type = "Factory";
            this._binding.factory = factory as any;
            return new BindingWhenOnSyntax<T>(this._binding);
        }
        toFunction(func: T): BindingWhenOnSyntax<T> {
            if (typeof func !== "function") throw new Error(INVALID_FUNCTION_BINDING);
            this._binding.type = "Function";
            this._binding.cache = func;
            this._binding.dynamicValue = null;
            this._binding.implementationType = null;
            return new BindingWhenOnSyntax<T>(this._binding);
        }
        toAutoFactory<T2>(serviceIdentifier: ServiceIdentifier<T2>): BindingWhenOnSyntax<T> {
            this._binding.type = "Factory";
            this._binding.factory = (context) => () => context.container.get<T2>(serviceIdentifier) as any;
            return new BindingWhenOnSyntax<T>(this._binding);
        }
        toProvider<T2>(provider: ((context: Context) => Provider<T2>)): BindingWhenOnSyntax<T> {
            this._binding.type = "Provider";
            this._binding.provider = provider as any;
            return new BindingWhenOnSyntax<T>(this._binding);
        }
        toService(service: string | symbol | Newable<T> | Abstract<T>): void {
            this._binding.type = "DynamicValue";
            this._binding.cache = null;
            this._binding.dynamicValue = (context) => context.container.get<T>(service);
            this._binding.implementationType = null;
        }
    }

    export class BindingWhenSyntax<T> extends BindingSyntax<T> {
        when(constraint: (request: Request) => boolean) {
            this._binding.constraint = constraint;
            return new BindingOnSyntax<T>(this._binding);
        }
        whenTargetNamed(name: string | number | symbol) {
            this._binding.constraint = namedConstraint(name);
            return new BindingOnSyntax<T>(this._binding);
        }
        whenTargetIsDefault(): BindingOnSyntax<T> {
            this._binding.constraint = (request: Request) => (request.target !== null) && (!request.target.isNamed()) && (!request.target.isTagged());
            return new BindingOnSyntax<T>(this._binding);
        }
        whenTargetTagged(tag: string | number | symbol, value: any): BindingOnSyntax<T> {
            this._binding.constraint = taggedConstraint(tag)(value);
            return new BindingOnSyntax<T>(this._binding);
        }
        whenInjectedInto(parent: (Function | string)) {
            this._binding.constraint = (request: Request) => typeConstraint(parent)(request.parent);
            return new BindingOnSyntax<T>(this._binding);
        }
        whenParentNamed(name: string | number | symbol) {
            this._binding.constraint = (request: Request) => namedConstraint(name)(request.parent);
            return new BindingOnSyntax<T>(this._binding);
        }
        whenParentTagged(tag: string | number | symbol, value: any): BindingOnSyntax<T> {
            this._binding.constraint = (request: Request) => taggedConstraint(tag)(value)(request.parent);
            return new BindingOnSyntax<T>(this._binding);
        }
        whenAnyAncestorIs(ancestor: (Function | string)){
            this._binding.constraint = (request: Request) => traverseAncerstors(request, typeConstraint(ancestor));
            return new BindingOnSyntax<T>(this._binding);
        }
        whenNoAncestorIs(ancestor: (Function | string)){
            this._binding.constraint = (request: Request) => !traverseAncerstors(request, typeConstraint(ancestor));
            return new BindingOnSyntax<T>(this._binding);
        }
        whenAnyAncestorNamed(name: string | number | symbol) {
            this._binding.constraint = (request: Request) => traverseAncerstors(request, namedConstraint(name));
            return new BindingOnSyntax<T>(this._binding);
        }
        whenNoAncestorNamed(name: string | number | symbol) {
            this._binding.constraint = (request: Request) => !traverseAncerstors(request, namedConstraint(name));
            return new BindingOnSyntax<T>(this._binding);
        }
        whenAnyAncestorTagged(tag: string | number | symbol, value: any){
            this._binding.constraint = (request: Request) => traverseAncerstors(request, taggedConstraint(tag)(value));
            return new BindingOnSyntax<T>(this._binding);
        }
        whenNoAncestorTagged(tag: string | number | symbol, value: any){
            this._binding.constraint = (request: Request) => !traverseAncerstors(request, taggedConstraint(tag)(value));
            return new BindingOnSyntax<T>(this._binding);
        }
        whenAnyAncestorMatches(constraint: (request: Request) => boolean) {
            this._binding.constraint = (request: Request) => traverseAncerstors(request, constraint);
            return new BindingOnSyntax<T>(this._binding);
        }
        whenNoAncestorMatches(constraint: (request: Request) => boolean) {
            this._binding.constraint = (request: Request) => !traverseAncerstors(request, constraint);
            return new BindingOnSyntax<T>(this._binding);
        }
    }

    export class BindingOnSyntax<T> extends BindingSyntax<T>  {
        onActivation(handler: (context: Context, injectable: T) => T): BindingWhenSyntax<T> {
            this._binding.onActivation = handler;
            return new BindingWhenSyntax<T>(this._binding);
        }
    }

    export class BindingInSyntax<T> extends BindingSyntax<T>  {
        inRequestScope() {
            this._binding.scope = "Request";
            return new BindingWhenOnSyntax<T>(this._binding);
        }
        inSingletonScope() {
            this._binding.scope = "Singleton";
            return new BindingWhenOnSyntax<T>(this._binding);
        }
        inTransientScope() {
            this._binding.scope = "Transient";
            return new BindingWhenOnSyntax<T>(this._binding);
        }
    }

    export interface BindingWhenOnSyntax<T> extends BindingSyntax<T>, BindingWhenSyntax<T>, BindingOnSyntax<T> { }
    export class BindingWhenOnSyntax<T> extends BindingSyntax<T>  { }
    copyPrototype(BindingOnSyntax, BindingWhenOnSyntax)
    copyPrototype(BindingWhenSyntax, BindingWhenOnSyntax)

    export interface BindingInWhenOnSyntax<T> extends BindingSyntax<T>, BindingWhenSyntax<T>, BindingOnSyntax<T>, BindingInSyntax<T> { }
    export class BindingInWhenOnSyntax<T> extends BindingSyntax<T>  { }
    copyPrototype(BindingOnSyntax, BindingInWhenOnSyntax)
    copyPrototype(BindingInSyntax, BindingInWhenOnSyntax)
    copyPrototype(BindingWhenSyntax, BindingInWhenOnSyntax)
}

function copyPrototype(source: Newable<any>, target: Newable<any>) {
    const t_keys = new Set(Object.getOwnPropertyNames(target.prototype));
    Object.getOwnPropertyNames(source.prototype).forEach(function (key) {
        const descr = Object.getOwnPropertyDescriptor(source.prototype, key);
        if (t_keys.has(key) || !descr) return;
        Object.defineProperty(target.prototype, key, descr)
    })
}


export const multiBindToService = (container: Container) =>
    (service: ServiceIdentifier) =>
        (...types: ServiceIdentifier[]) =>
            types.forEach((t) => container.bind(t).toService(service));
