import "reflect-metadata"
import { statSync, createReadStream } from 'fs';
import { createGunzip } from 'zlib';
import { injectable, Container, Context, inject, tagged } from './inversify';
import File from "vinyl";
import split from "split2"
import { once } from 'events';

function bindToVinylFile(container: Container, path: string) {
    const stat = statSync(path)
    const file = new File({ path, stat }) as File.StreamFile;
    const cmpr = file.extname.endsWith(".gz");
    
    container.bind(File)
            .toDynamicValue(function (ctx: Context) {
                let value = file.clone()
                let readable: any = createReadStream(path)
                if (cmpr) readable = readable.pipe(createGunzip())               
                value.contents = readable.pipe(split());
                return value;
            })
            .whenTargetTagged("byline", file)

    container.bind(File)
            .toDynamicValue(function (ctx: Context) {
                let value = file.clone()
                value.contents = createReadStream(path)
                return value;
            })
            .whenTargetTagged("raw", file)

    return file;
}
const container = new Container()
function open(path: string) {
    return bindToVinylFile(container, path)
}








/**
 * @returns {InputFile}
 */
export function InputFile(file: File) {
    return function ParameterDecoratorName(target: any, methodKey: string, parameterIndex: number) {
        inject(File)(target, methodKey, parameterIndex)
        tagged("byline", file)(target, methodKey, parameterIndex)
    }
}



//const f1 = open("./datafiles/tumor_sample.BRCA.cna.tsv")
//const f2 = open("./datafiles/tumor_sample.BRCA.vcf.gz")


const f1 = open("./datafiles/ZD610103_2.hg37d5.excavator.cnv.txt")
const f2 = open("./datafiles/ZD610103_2.hg37d5.somatic.vcf.gz")
const CV = open("./datafiles/01-Oct-2020-VariantSummaries.tsv")

@injectable
class MyPipeline {

    constructor (
        @InputFile(f1) source: File.StreamFile, 
        @InputFile(f2) source2: File.StreamFile, 
        @InputFile(CV) civicdb: File.StreamFile, 
    ) {
        //civicdb.contents.once("data", VCFParser)

        //consumeUntil(civicdb.contents, t => !t.startsWith("##"))
        //    .then(data => console.log(data))
        civicdb.contents.once("data", CVNParser)
        //source2.contents.once("data", VCFParser)
        //source.contents.once("data", CVNParser)
    }

}


async function peekFromStream(stream: any) {
    if (!stream.readable) return null;
    let buffer = stream._readableState.buffer
    if (buffer.length === 0) await once(stream, "readable")
    return buffer.head.data;
}



async function readFromStream(stream: any) {
    if (!stream.readable) return null;
    let item = stream.read();
    if (item !== null) return item;
    await once(stream, "readable")
    return stream.read();
}



async function consumeUntil<T=any>(stream: any, callback: (data:T) => boolean) {
    let collected = []
    while (1) {
        const item1 = await peekFromStream(stream);
        const item2 = await readFromStream(stream);
        collected.push(item1, item2)
        if (callback(item2)) return collected;
    }
}





console.log(container.resolve(MyPipeline))

function CVNParser(header: string) {
    this.on("data", parseBody)

    function parseBody(data: string) {
        const names = header.split("\t")
        const values = data.split("\t")

        const V: any = {}
        for (let i in values) {
            if (!values[i]) continue
            V[names[i]] = values[i];
        }
        console.log(V)
    }
}


function VCFParser(line: string) {
    let meta: any[] = [ line ]
    let head: any[] = []
    let body: any[] = []


    this.on("data", parseHeader)

    this.once("end", function () {
        console.dir({ meta, head, body }, { depth: 20 })
    })



    function parseHeader(line: string) {
        if (line.startsWith("##")) {
            meta.push(line)
        } else {
            head = line.split("\t");
            this.off("data", parseHeader)
            this.on("data", parseBody)
        }
    }

    function parseBody(line: string) {
        let [ CHROM, POS, ID, REF, ALT, QUAL, FILTER, INFO, FORMAT, ...DATA ] = line.split("\t") as any[]

        if (ID === '.') ID = null;
        if (QUAL === '.') QUAL = null;
        POS = parseInt(POS);

        const BLOCKS = INFO.split(";").map((pair:string) => pair.split("="))


        //FORMAT = FORMAT.split(":")
        //DATA   = DATA  .split(":")

        //const SAMPLE: any = {}
        //for (let i in FORMAT) {
        //    SAMPLE[FORMAT[i]] = DATA[i]
        // }

        body.push({ CHROM, POS, ID, REF, ALT, QUAL, FILTER, INFO: new Map(BLOCKS), FORMAT, DATA })
    }

}