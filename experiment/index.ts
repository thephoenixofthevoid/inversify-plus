import "reflect-metadata"
import { injectable, Container } from '../src/inversify';
import { Resolver } from "../src/API/Resolver";

@injectable()
class Example {

}

@injectable()
class Example1 {

}




const c = new Container();
console.dir(c, { depth: 10 })


c.on("target", function (context, target) {
    console.log("-----created-----")
    console.log(context)
    console.log("\n")
    console.log(target)
    console.log("\n")
})
c.on("planned", function (context, target) {
    console.log("-----planned-----")
    console.log(context)
    console.log("\n")
    console.log(target)
    console.log("\n")
})

c.bind(Example).toSelf()
const instance = Resolver(c).get(Example)


c.bind(Example).to(Example1)
const instances = Resolver(c).getAll(Example)

