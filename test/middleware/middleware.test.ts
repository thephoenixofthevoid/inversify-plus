// import { expect } from "chai";
// import * as sinon from "sinon";
// import { injectable } from "../../src/inversify";
// import { Container } from "../../src/inversify";
// import { Context } from "../../src/inversify";

// const INVALID_BINDING_TYPE = "Invalid binding type:";
// const AMBIGUOUS_MATCH = "Ambiguous match found for serviceIdentifier:";
// const INVALID_MIDDLEWARE_RETURN = "Invalid return type in middleware. Middleware must return!";
// const NOT_REGISTERED = "No matching bindings found for serviceIdentifier:";

// describe.skip("Middleware", () => {

//     let sandbox: sinon.SinonSandbox;

//     beforeEach(() => {
//         sandbox = sinon.createSandbox();
//     });

//     afterEach(() => {
//         sandbox.restore();
//     });

//     it("Should be able to use middleware as Container configuration", () => {

//         const container = new Container();

//         const log: string[] = [];

//         container.applyMiddleware(function middleware1(planAndResolve) {
//             return (args) => {
//                 log.push(`Middleware1: ${args.serviceIdentifier.toString()}`);
//                 return planAndResolve(args);
//             };
//         });
//         const _container: any = container;
//         expect(_container._middleware).not.to.eql(null);

//     });

//     it("Should support middleware", () => {

//         interface Ninja {}

//         @injectable
//         class Ninja implements Ninja {}

//         const container = new Container();

//         const log: string[] = [];

//         // two middlewares applied at one single point in time
//         container.applyMiddleware(function middleware1(planAndResolve) {
//             return (args) => {
//                 log.push(`Middleware1: ${args.serviceIdentifier.toString()}`);
//                 return planAndResolve(args);
//             };
//         }, function middleware2(planAndResolve) {
//             return (args) => {
//                 log.push(`Middleware2: ${args.serviceIdentifier.toString()}`);
//                 return planAndResolve(args);
//             };
//         });

//         container.bind<Ninja>("Ninja").to(Ninja);

//         const ninja = container.get<Ninja>("Ninja");

//         expect(ninja instanceof Ninja).eql(true);
//         expect(log.length).eql(2);
//         expect(log[0]).eql("Middleware2: Ninja");
//         expect(log[1]).eql("Middleware1: Ninja");

//     });

//     it("Should allow applyMiddleware at multiple points in time", () => {

//         interface Ninja {}

//         @injectable
//         class Ninja implements Ninja {}

//         const container = new Container();

//         const log: string[] = [];

//         container.applyMiddleware(function middleware1(planAndResolve) {
//             return (args) => {
//                 log.push(`Middleware1: ${args.serviceIdentifier.toString()}`);
//                 return planAndResolve(args);
//             };
//         }); // one point in time
//         container.applyMiddleware(function middleware2(planAndResolve) {
//             return (args) => {
//                 log.push(`Middleware2: ${args.serviceIdentifier.toString()}`);
//                 return planAndResolve(args);
//             };
//         });  // another point in time
//         container.bind<Ninja>("Ninja").to(Ninja);

//         const ninja = container.get<Ninja>("Ninja");

//         expect(ninja instanceof Ninja).eql(true);
//         expect(log.length).eql(2);
//         expect(log[0]).eql("Middleware2: Ninja");
//         expect(log[1]).eql("Middleware1: Ninja");

//     });

//     it("Should use middleware", () => {

//         interface Ninja {}

//         @injectable
//         class Ninja implements Ninja {}

//         const container = new Container();

//         const log: string[] = [];

        

       

//         container.applyMiddleware(function middleware1(planAndResolve) {
//             return (args) => {
//                 log.push(`Middleware1: ${args.serviceIdentifier.toString()}`);
//                 return planAndResolve(args);
//             };
//         },  function middleware2(planAndResolve) {
//             return (args) => {
//                 log.push(`Middleware2: ${args.serviceIdentifier.toString()}`);
//                 return planAndResolve(args);
//             };
//         });
//         container.bind<Ninja>("Ninja").to(Ninja);

//         const ninja = container.get<Ninja>("Ninja");

//         expect(ninja instanceof Ninja).eql(true);
//         expect(log.length).eql(2);
//         expect(log[0]).eql("Middleware2: Ninja");
//         expect(log[1]).eql("Middleware1: Ninja");

//     });

//     it("Should be able to use middleware to catch errors during pre-planning phase", () => {

//         interface Ninja {}

//         @injectable
//         class Ninja implements Ninja {}

//         const container = new Container();

//         const log: string[] = [];

        

//         container.applyMiddleware(function middleware(planAndResolve) {
//             return (args) => {
//                 try {
//                     return planAndResolve(args);
//                 } catch (e) {
//                     log.push(e.message);
//                     return [];
//                 }
//             };
//         });
//         container.bind<Ninja>("Ninja").to(Ninja);
//         container.get<any>("SOME_NOT_REGISTERED_ID");
//         expect(log.length).eql(1);
//         expect(log[0]).eql(`${NOT_REGISTERED} SOME_NOT_REGISTERED_ID`);

//     });

//     it("Should be able to use middleware to catch errors during planning phase", () => {

//         interface Warrior {}

//         @injectable
//         class Ninja implements Warrior {}

//         @injectable
//         class Samurai implements Warrior {}

//         const container = new Container();

//         const log: string[] = [];

        

//         container.applyMiddleware(function middleware(planAndResolve) {
//             return (args) => {
//                 try {
//                     return planAndResolve(args);
//                 } catch (e) {
//                     log.push(e.message);
//                     return [];
//                 }
//             };
//         });
//         container.bind<Warrior>("Warrior").to(Ninja);
//         container.bind<Warrior>("Warrior").to(Samurai);

//         container.get<any>("Warrior");
//         expect(log.length).eql(1);
//         expect(log[0]).to.contain(`${AMBIGUOUS_MATCH} Warrior`);

//     });

//     it("Should be able to use middleware to catch errors during resolution phase", () => {

//         interface Warrior {}

//         const container = new Container();

//         const log: string[] = [];

        

//         container.applyMiddleware(function middleware(planAndResolve) {
//             return (args) => {
//                 try {
//                     return planAndResolve(args);
//                 } catch (e) {
//                     log.push(e.message);
//                     return [];
//                 }
//             };
//         });
//         container.bind<Warrior>("Warrior"); // Invalid binding missing BindingToSyntax

//         container.get<any>("Warrior");
//         expect(log.length).eql(1);
//         expect(log[0]).eql(`${INVALID_BINDING_TYPE} Warrior`);

//     });

//     it("Should help users to identify problems with middleware", () => {

//         const container = new Container();

        

//         container.applyMiddleware(function middleware(planAndResolve) {
//             return (args) => {
//                 try {
//                     return planAndResolve(args);
//                 } catch (e) {
//                     // missing return!
//                 }
//             };
//         });
//         const throws = () => { container.get<any>("SOME_NOT_REGISTERED_ID"); };
//         expect(throws).to.throw(INVALID_MIDDLEWARE_RETURN);

//     });

//     it("Should allow users to intercept a resolution context", () => {

//         interface Ninja {}

//         @injectable
//         class Ninja implements Ninja {}

//         const container = new Container();

//         const log: string[] = [];

        

//         container.applyMiddleware(
//             function middleware1(planAndResolve) {
//                 return (args) => {
//                     const nextContextInterceptor = args.contextInterceptor;
//                     args.contextInterceptor = (context: Context) => {
//                         log.push(`contextInterceptor1: ${args.serviceIdentifier.toString()}`);
//                         return nextContextInterceptor !== null ? nextContextInterceptor(context) : context;
//                     };
//                     return planAndResolve(args);
//                 };
//             },    
//             function middleware2(planAndResolve) {
//                 return (args) => {
//                     const nextContextInterceptor = args.contextInterceptor;
//                     args.contextInterceptor = (context: Context) => {
//                         log.push(`contextInterceptor2: ${args.serviceIdentifier.toString()}`);
//                         return nextContextInterceptor !== null ? nextContextInterceptor(context) : context;
//                     };
//                     return planAndResolve(args);
//                 };
//             }
//         );
//         container.bind<Ninja>("Ninja").to(Ninja);

//         const ninja = container.get<Ninja>("Ninja");

//         expect(ninja instanceof Ninja).eql(true);
//         expect(log.length).eql(2);
//         expect(log[0]).eql("contextInterceptor1: Ninja");
//         expect(log[1]).eql("contextInterceptor2: Ninja");

//     });

// });
