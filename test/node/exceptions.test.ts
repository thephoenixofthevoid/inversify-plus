import { expect } from "chai";
import { Resolver } from "../../src/API/Resolver";
import { Container, inject, injectable, multiInject } from "../../src/inversify";

const CIRCULAR_DEPENDENCY = "Circular dependency found:";


describe("Node", () => {

    it("Should throw if circular dependencies found", () => {

        interface A {}
        interface B {}
        interface C {}
        interface D {}

        @injectable()
        class A implements A {
            public b: typeof B;
            public c: typeof C;
            public constructor(
                @inject("B")  b: typeof B,
                @inject("C")  c: typeof C
            ) {
                this.b = b;
                this.c = c;
            }
        }

        @injectable()
        class B implements B {}

        @injectable()
        class C implements C {
            public d: typeof D;
            public constructor(@inject("D") d: typeof D) {
                this.d = d;
            }
        }

        @injectable()
        class D implements D {
            public a: typeof A;
            public constructor(@inject("A") a: typeof A) {
                this.a = a;
            }
        }

        const container = new Container();
        container.bind<A>("A").to(A);
        container.bind<B>("B").to(B);
        container.bind<C>("C").to(C);
        container.bind<D>("D").to(D);

        function willThrow() {
            const a = Resolver(container).get<A>("A");
            return a;
        }

        expect(willThrow).to.throw(
            `${CIRCULAR_DEPENDENCY} A --> C --> D --> A`
        );

    });

    it("Should work with multiInject", () => {

        interface A {}
        interface B {}
        interface C {}
        interface D {}

        @injectable()
        class A implements A {
            public b: typeof B;
            public c: typeof C;
            public constructor(
                @multiInject("B")  b: typeof B,
                @multiInject("C")  c: typeof C
            ) {
                this.b = b;
                this.c = c;
            }
        }

        @injectable()
        class B implements B {}

        @injectable()
        class C implements C {
            public d: typeof D;
            public constructor(@inject("D") d: typeof D) {
                this.d = d;
            }
        }

        @injectable()
        class D implements D {
            public a: typeof A;
            public constructor(@inject("A") a: typeof A) {
                this.a = a;
            }
        }

        const container = new Container();
        container.bind<A>("A").to(A);
        container.bind<B>("B").to(B);
        container.bind<C>("C").to(C);
        container.bind<D>("D").to(D);

        function willThrow() {
            const a = Resolver(container).get<A>("A");
            return a;
        }

        expect(willThrow).to.throw(
            `${CIRCULAR_DEPENDENCY} A --> C --> D --> A`
        );

    });

});
