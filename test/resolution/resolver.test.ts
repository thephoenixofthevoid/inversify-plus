import { expect } from "chai";
import Proxy from "harmony-proxy";
import * as sinon from "sinon";
import { Resolver } from "../../src/API/Resolver";
import { Id, inject, injectable, multiInject, named, postConstruct, tagged, targetName, Container} from "../../src/inversify";
import { Context, EntityRequest } from "../../src/Base/PlanElements";
import { Target } from "../../src/Base/Target";
import { INJECT_TAG, MULTI_INJECT_TAG, NAMED_TAG, MODE_TAG } from "../../src/Constants";
import { isCallStackOverflowError, tryFindingCircularDependency } from "../../src/Errors";
import { Any } from "ts-toolbelt"
import { Factory } from "../../src/API/BindingOnSyntax";
import { createTreeNode } from "../../src/configureTree";
import { InvalidResolvingStrategy } from "../../src/Strategies/ResolvingStrategy";

function circularDependencyToException(error: Error, context: Context) {
    if (isCallStackOverflowError(error)) {
        const richError = tryFindingCircularDependency(context.root);
        if (richError) return richError;
    }
    return error;
}

function plan(context: Context, isMultiInject: boolean,  targetType: any,  serviceIdentifier: Id, key?: Any.Key,  value?: any) {
    const target = Target.for(serviceIdentifier)
    target.set(MODE_TAG, targetType)
    if (isMultiInject) {
        target.set(MULTI_INJECT_TAG, serviceIdentifier);
    } else {
        target.set(INJECT_TAG, serviceIdentifier);
    }

    if (key !== undefined) target.set(key, value);

    try { 
        context.root = createTreeNode(target, null, context);
        context.root.initNode()
        context.root.validateResults();
        context.root.initDeps();
    } 
    catch (error) { 
        throw circularDependencyToException(error, context); 
    }
}

const INVALID_BINDING_TYPE = "Invalid binding type:";

function getBindingDictionary(cntnr: any) {
    return cntnr.bindings;
}

function resolveContext<T>(context: Context): T {
    return context.root.getResults()
}

describe("Resolve", () => {

  let sandbox: sinon.SinonSandbox;

  beforeEach(() => {
    sandbox = sinon.createSandbox();
  });

  afterEach(() => {
    sandbox.restore();
  });

  it("Should be able to resolve BindingType.Instance bindings", () => {

      const ninjaId = "Ninja";
      const shurikenId = "Shuriken";
      const katanaId = "Katana";
      const katanaHandlerId = "KatanaHandler";
      const katanaBladeId = "KatanaBlade";

      interface Blade {}

      @injectable()
      class KatanaBlade implements Blade {}

      interface Handler {}

      @injectable()
      class KatanaHandler implements Handler {}

      interface Sword {
          handler: KatanaHandler;
          blade: KatanaBlade;
      }

      @injectable()
      class Katana implements Sword {
          public handler: Handler;
          public blade: Blade;
          public constructor(
              @inject(katanaHandlerId) @targetName("handler") handler: Handler,
              @inject(katanaBladeId) @targetName("blade") blade: Blade
          ) {
              this.handler = handler;
              this.blade = blade;
          }
      }

      interface Shuriken {}

      @injectable()
      class Shuriken implements Shuriken {}

      interface Warrior {
          katana: Katana;
          shuriken: Shuriken;
      }

      @injectable()
      class Ninja implements Warrior {
          public katana: Katana;
          public shuriken: Shuriken;
          public constructor(
              @inject(katanaId) @targetName("katana") katana: Katana,
              @inject(shurikenId) @targetName("shuriken") shuriken: Shuriken
          ) {
              this.katana = katana;
              this.shuriken = shuriken;
          }
      }

      const container = new Container();
      container.bind<Ninja>(ninjaId).to(Ninja);
      container.bind<Shuriken>(shurikenId).to(Shuriken);
      container.bind<Katana>(katanaId).to(Katana);
      container.bind<KatanaBlade>(katanaBladeId).to(KatanaBlade);
      container.bind<KatanaHandler>(katanaHandlerId).to(KatanaHandler);
      const context = new Context(container)
      plan(context, false, "Variable", ninjaId);
      const ninja = resolveContext<Ninja>(context);

      expect(ninja instanceof Ninja).eql(true);
      expect(ninja.katana instanceof Katana).eql(true);
      expect(ninja.katana.handler instanceof KatanaHandler).eql(true);
      expect(ninja.katana.blade instanceof KatanaBlade).eql(true);
      expect(ninja.shuriken instanceof Shuriken).eql(true);

  });

  it("Should store singleton type bindings in cache", () => {

      const ninjaId = "Ninja";
      const shurikenId = "Shuriken";
      const katanaId = "Katana";
      const katanaHandlerId = "KatanaHandler";
      const katanaBladeId = "KatanaBlade";

      interface Blade {}

      @injectable()
      class KatanaBlade implements Blade {}

      interface Handler {}

      @injectable()
      class KatanaHandler implements Handler {}

      interface Sword {
          handler: KatanaHandler;
          blade: KatanaBlade;
      }

      @injectable()
      class Katana implements Sword {
          public handler: Handler;
          public blade: Blade;
          public constructor(
              @inject(katanaHandlerId) @targetName("handler") handler: Handler,
              @inject(katanaBladeId) @targetName("blade") blade: Blade
          ) {
              this.handler = handler;
              this.blade = blade;
          }
      }

      interface Shuriken {}

      @injectable()
      class Shuriken implements Shuriken {}

      interface Warrior {
          katana: Katana;
          shuriken: Shuriken;
      }

      @injectable()
      class Ninja implements Warrior {
          public katana: Katana;
          public shuriken: Shuriken;
          public constructor(
              @inject(katanaId) @targetName("katana") katana: Katana,
              @inject(shurikenId) @targetName("shuriken") shuriken: Shuriken
          ) {
              this.katana = katana;
              this.shuriken = shuriken;
          }
      }

      const container = new Container();
      container.bind<Ninja>(ninjaId).to(Ninja);
      container.bind<Shuriken>(shurikenId).to(Shuriken);
      container.bind<Katana>(katanaId).to(Katana).inSingletonScope(); // SINGLETON!
      container.bind<KatanaBlade>(katanaBladeId).to(KatanaBlade);
      container.bind<KatanaHandler>(katanaHandlerId).to(KatanaHandler).inSingletonScope(); // SINGLETON!

      const bindingDictionary = getBindingDictionary(container);
      const context = new Context(container)
      plan(context, false, "Variable", ninjaId);

      //expect(bindingDictionary.get(katanaId)[0].cache === null).eql(true);
      const ninja = resolveContext<Ninja>(context);
      expect(ninja instanceof Ninja).eql(true);

      const ninja2 = resolveContext<Ninja>(context);
      expect(ninja2 instanceof Ninja).eql(true);

      //expect(bindingDictionary.get(katanaId)[0].cache instanceof Katana).eql(true);

  });

  it("Should throw when an invalid BindingType is detected", () => {

      interface Katana {}

      @injectable()
      class Katana implements Katana {}

      interface Shuriken {}

      @injectable()
      class Shuriken implements Shuriken {}

      interface Warrior {
          katana: Katana;
          shuriken: Shuriken;
      }

      @injectable()
      class Ninja implements Warrior {
          public katana: Katana;
          public shuriken: Shuriken;
          public constructor(
              @inject("Katana") @targetName("katana") katana: Katana,
              @inject("Shuriken") @targetName("shuriken") shuriken: Shuriken
          ) {
              this.katana = katana;
              this.shuriken = shuriken;
          }
      }

      // container and bindings
      const ninjaId = "Ninja";
      const container = new Container();
      container.bind<Ninja>(ninjaId); // IMPORTANT! (Invalid binding)

      // context and plan

      const context = new Context(container)
      plan(context, false, "Variable", ninjaId);

      const throwFunction = () => {
          resolveContext(context);
      };

      expect((context.root[0] as EntityRequest).binding.type).eql(InvalidResolvingStrategy);
      expect(throwFunction).to.throw(`${INVALID_BINDING_TYPE} ${ninjaId}`);

  });

  it("Should be able to resolve BindingType.ConstantValue bindings", () => {

      interface KatanaBlade {}

      @injectable()
      class KatanaBlade implements KatanaBlade {}

      interface KatanaHandler {}

      @injectable()
      class KatanaHandler implements KatanaHandler {}

      interface Sword {
          handler: KatanaHandler;
          blade: KatanaBlade;
      }

      @injectable()
      class Katana implements Sword {
          public handler: KatanaHandler;
          public blade: KatanaBlade;
          public constructor(handler: KatanaHandler, blade: KatanaBlade) {
              this.handler = handler;
              this.blade = blade;
          }
      }

      interface Shuriken {}

      @injectable()
      class Shuriken implements Shuriken {}

      interface Warrior {
          katana: Katana;
          shuriken: Shuriken;
      }

      @injectable()
      class Ninja implements Warrior {
          public katana: Katana;
          public shuriken: Shuriken;
          public constructor(
              @inject("Katana") @targetName("katana") katana: Katana,
              @inject("Shuriken") @targetName("shuriken") shuriken: Shuriken
          ) {
              this.katana = katana;
              this.shuriken = shuriken;
          }
      }

      const ninjaId = "Ninja";
      const shurikenId = "Shuriken";
      const katanaId = "Katana";

      const container = new Container();
      container.bind<Ninja>(ninjaId).to(Ninja);
      container.bind<Shuriken>(shurikenId).to(Shuriken);
      container.bind<Katana>(katanaId).toConstantValue(new Katana(new KatanaHandler(), new KatanaBlade())); // IMPORTANT!

      const context = new Context(container)
      plan(context, false, "Variable", ninjaId);

      const ninja = resolveContext<Ninja>(context);

      expect(ninja instanceof Ninja).eql(true);
      expect(ninja.katana instanceof Katana).eql(true);
      expect(ninja.katana.handler instanceof KatanaHandler).eql(true);
      expect(ninja.katana.blade instanceof KatanaBlade).eql(true);
      expect(ninja.shuriken instanceof Shuriken).eql(true);

  });

  it("Should be able to resolve BindingType.DynamicValue bindings", () => {

    interface UseDate {
        doSomething(): Date;
    }

    @injectable()
    class UseDate implements UseDate {
        public currentDate: Date;
        public constructor(@inject("Date") currentDate: Date) {
            this.currentDate = currentDate;
        }
        public doSomething() {
            return this.currentDate;
        }
    }

    const container = new Container();
    container.bind<UseDate>("UseDate").to(UseDate);
    container.bind<Date>("Date").toDynamicValue((context: Context) => new Date());

    const subject1 = Resolver(container).get<UseDate>("UseDate");
    const subject2 = Resolver(container).get<UseDate>("UseDate");
    expect(subject1.doSomething() === subject2.doSomething()).eql(false);

    container.unbind("Date");
    container.bind<Date>("Date").toConstantValue(new Date());

    const subject3 = Resolver(container).get<UseDate>("UseDate");
    const subject4 = Resolver(container).get<UseDate>("UseDate");
    expect(subject3.doSomething() === subject4.doSomething()).eql(true);

  });

//   it("Should be able to resolve BindingType.Constructor bindings", () => {

//       const ninjaId = "Ninja";
//       const shurikenId = "Shuriken";
//       const katanaId = "Katana";
//       const newableKatanaId = "Newable<Katana>";
//       const katanaHandlerId = "KatanaHandler";
//       const katanaBladeId = "KatanaBlade";

//       interface KatanaBlade {}

//       @injectable
//       class KatanaBlade implements KatanaBlade {}

//       interface KatanaHandler {}

//       @injectable
//       class KatanaHandler implements KatanaHandler {}

//       interface Sword {
//           handler: KatanaHandler;
//           blade: KatanaBlade;
//       }

//       @injectable
//       class Katana implements Sword {
//           public handler: KatanaHandler;
//           public blade: KatanaBlade;
//           public constructor(
//               @inject(katanaHandlerId) @targetName("handler") handler: KatanaHandler,
//               @inject(katanaBladeId) @targetName("blade") blade: KatanaBlade
//           ) {
//               this.handler = handler;
//               this.blade = blade;
//           }
//       }

//       interface Shuriken {}

//       @injectable
//       class Shuriken implements Shuriken {}

//       interface Warrior {
//           katana: Katana;
//           shuriken: Shuriken;
//       }

//       @injectable
//       class Ninja implements Warrior {
//           public katana: Katana;
//           public shuriken: Shuriken;
//           public constructor(
//               @inject(newableKatanaId) @targetName("katana") katana: Katana,
//               @inject(shurikenId) @targetName("shuriken") shuriken: Shuriken
//           ) {
//               this.katana = new Katana(new KatanaHandler(), new KatanaBlade());  // IMPORTANT!
//               this.shuriken = shuriken;
//           }
//       }

//       const container = new Container();
//       container.bind<Ninja>(ninjaId).to(Ninja);
//       container.bind<Shuriken>(shurikenId).to(Shuriken);
//       container.bind<Katana>(katanaId).to(Katana);
//       container.bind<Newable<Katana>, Katana>(newableKatanaId).toConstructor(Katana);  // IMPORTANT!

//       const context = new Context(container); 
//       plan(context, false, "Variable", ninjaId);
//       const ninja = resolveContext<Ninja>(context);

//       expect(ninja instanceof Ninja).eql(true);
//       expect(ninja.katana instanceof Katana).eql(true);
//       expect(ninja.katana.handler instanceof KatanaHandler).eql(true);
//       expect(ninja.katana.blade instanceof KatanaBlade).eql(true);
//       expect(ninja.shuriken instanceof Shuriken).eql(true);

//   });

  it("Should be able to resolve BindingType.Factory bindings", () => {

      const ninjaId = "Ninja";
      const shurikenId = "Shuriken";
      const swordFactoryId = "Factory<Sword>";
      const katanaId = "Katana";
      const handlerId = "Handler";
      const bladeId = "Blade";

      interface Blade {}

      @injectable()
      class KatanaBlade implements Blade {}

      interface Handler {}

      @injectable()
      class KatanaHandler implements Handler {}

      interface Sword {
          handler: Handler;
          blade: Blade;
      }

      type SwordFactory = () => Sword;

      @injectable()
      class Katana implements Sword {
          public handler: Handler;
          public blade: Blade;
          public constructor(
              @inject(handlerId) @targetName("handler") handler: Handler,
              @inject(bladeId) @targetName("blade") blade: Blade
          ) {
              this.handler = handler;
              this.blade = blade;
          }
      }

      interface Shuriken {}

      @injectable()
      class Shuriken implements Shuriken {}

      interface Warrior {
          katana: Katana;
          shuriken: Shuriken;
      }

      @injectable()
      class Ninja implements Warrior {
          public katana: Katana;
          public shuriken: Shuriken;
          public constructor(
              @inject(swordFactoryId) @targetName("makeKatana") makeKatana: SwordFactory,
              @inject(shurikenId) @targetName("shuriken") shuriken: Shuriken
          ) {
              this.katana = makeKatana(); // IMPORTANT!
              this.shuriken = shuriken;
          }
      }

      const container = new Container();
      container.bind<Ninja>(ninjaId).to(Ninja);
      container.bind<Shuriken>(shurikenId).to(Shuriken);
      container.bind<Katana>(katanaId).to(Katana);
      container.bind<KatanaBlade>(bladeId).to(KatanaBlade);
      container.bind<KatanaHandler>(handlerId).to(KatanaHandler);

      container.bind<Factory<Katana>>(swordFactoryId).toFactory((theContext: Context) =>
          () =>
                Resolver(theContext.container).get<Katana>(katanaId));
      const context = new Context(container)
      plan(context, false, "Variable", ninjaId);

      const ninja = resolveContext<Ninja>(context);

      expect(ninja instanceof Ninja).eql(true);
      expect(ninja.katana instanceof Katana).eql(true);
      expect(ninja.katana.handler instanceof KatanaHandler).eql(true);
      expect(ninja.katana.blade instanceof KatanaBlade).eql(true);
      expect(ninja.shuriken instanceof Shuriken).eql(true);

  });

  it("Should be able to resolve bindings with auto factory", () => {

      const ninjaId = "Ninja";
      const shurikenId = "Shuriken";
      const katanaFactoryId = "Factory<Sword>";
      const katanaId = "Katana";
      const katanaHandlerId = "KatanaHandler";
      const katanaBladeId = "KatanaBlade";

      interface KatanaBlade {}

      @injectable()
      class KatanaBlade implements KatanaBlade {}

      interface KatanaHandler {}

      @injectable()
      class KatanaHandler implements KatanaHandler {}

      interface Sword {
          handler: KatanaHandler;
          blade: KatanaBlade;
      }

      type SwordFactory = () => Sword;

      @injectable()
      class Katana implements Sword {
          public handler: KatanaHandler;
          public blade: KatanaBlade;
          public constructor(
              @inject(katanaHandlerId) @targetName("handler") handler: KatanaHandler,
              @inject(katanaBladeId) @targetName("blade") blade: KatanaBlade
          ) {
              this.handler = handler;
              this.blade = blade;
          }
      }

      interface Shuriken {}

      @injectable()
      class Shuriken implements Shuriken {}

      interface Warrior {
          katana: Katana;
          shuriken: Shuriken;
      }

      @injectable()
      class Ninja implements Warrior {
          public katana: Katana;
          public shuriken: Shuriken;
          public constructor(
              @inject(katanaFactoryId) @targetName("makeKatana") makeKatana: SwordFactory,
              @inject(shurikenId) @targetName("shuriken") shuriken: Shuriken
          ) {
              this.katana = makeKatana(); // IMPORTANT!
              this.shuriken = shuriken;
          }
      }

      const container = new Container();
      container.bind<Ninja>(ninjaId).to(Ninja);
      container.bind<Shuriken>(shurikenId).to(Shuriken);
      container.bind<Katana>(katanaId).to(Katana);
      container.bind<KatanaBlade>(katanaBladeId).to(KatanaBlade);
      container.bind<KatanaHandler>(katanaHandlerId).to(KatanaHandler);
      container.bind<Factory<Katana>>(katanaFactoryId)
        .toFactory(context => () => Resolver(context.container).get(katanaId));

      const context = new Context(container); 
      plan(context, false, "Variable", ninjaId);
      const ninja = resolveContext<Ninja>(context);

      expect(ninja instanceof Ninja).eql(true);
      expect(ninja.katana instanceof Katana).eql(true);
      expect(ninja.katana.handler instanceof KatanaHandler).eql(true);
      expect(ninja.katana.blade instanceof KatanaBlade).eql(true);
      expect(ninja.shuriken instanceof Shuriken).eql(true);

  });

  it("Should be able to resolve BindingType.Provider bindings", (done) => {

      type SwordProvider = () => Promise<Sword>;

      const ninjaId = "Ninja";
      const shurikenId = "Shuriken";
      const swordProviderId = "Provider<Sword>";
      const swordId = "Sword";
      const handlerId = "Handler";
      const bladeId = "Blade";

      interface Blade {}

      @injectable()
      class KatanaBlade implements Blade {}

      interface Handler {}

      @injectable()
      class KatanaHandler implements Handler {}

      interface Sword {
          handler: Handler;
          blade: Blade;
      }

      @injectable()
      class Katana implements Sword {
          public handler: Handler;
          public blade: Blade;
          public constructor(
              @inject(handlerId) @targetName("handler") handler: Handler,
              @inject(bladeId) @targetName("handler") blade: Blade
          ) {
              this.handler = handler;
              this.blade = blade;
          }
      }

      interface Shuriken {}

      @injectable()
      class Shuriken implements Shuriken {}

      interface Warrior {
          katana: Katana | null;
          katanaProvider: SwordProvider;
          shuriken: Shuriken;
      }

      @injectable()
      class Ninja implements Warrior {
          public katana: Katana | null;
          public katanaProvider: SwordProvider;
          public shuriken: Shuriken;
          public constructor(
              @inject(swordProviderId) @targetName("katanaProvider") katanaProvider: SwordProvider,
              @inject(shurikenId) @targetName("shuriken") shuriken: Shuriken
          ) {
              this.katana = null;
              this.katanaProvider = katanaProvider;
              this.shuriken = shuriken;
          }
      }

      const container = new Container();
      container.bind<Warrior>(ninjaId).to(Ninja);
      container.bind<Shuriken>(shurikenId).to(Shuriken);
      container.bind<Sword>(swordId).to(Katana);
      container.bind<Blade>(bladeId).to(KatanaBlade);
      container.bind<Handler>(handlerId).to(KatanaHandler);

      container.bind<SwordProvider>(swordProviderId).toProvider((theContext: Context) =>
          () =>
              new Promise<Sword>((resolveFunc) => {
                  // Using setTimeout to simulate complex initialization
                  setTimeout(() => { resolveFunc(Resolver(theContext.container).get<Sword>(swordId)); }, 100);
              }));

      const context = new Context(container); 
      plan(context, false, "Variable", ninjaId);

      const ninja = resolveContext<Warrior>(context);

      expect(ninja instanceof Ninja).eql(true);
      expect(ninja.shuriken instanceof Shuriken).eql(true);
      ninja.katanaProvider().then((katana) => {
          ninja.katana = katana;
          expect(ninja.katana instanceof Katana).eql(true);
          expect(ninja.katana.handler instanceof KatanaHandler).eql(true);
          expect(ninja.katana.blade instanceof KatanaBlade).eql(true);
          done();
      });

  });

  it("Should be able to resolve plans with constraints on tagged targets", () => {

      interface Weapon {}

      @injectable()
      class Katana implements Weapon { }

      @injectable()
      class Shuriken implements Weapon {}

      interface Warrior {
          katana: Weapon;
          shuriken: Weapon;
      }

      @injectable()
      class Ninja implements Warrior {
          public katana: Weapon;
          public shuriken: Weapon;
          public constructor(
              @inject("Weapon") @targetName("katana") @tagged("canThrow", false) katana: Weapon,
              @inject("Weapon") @targetName("shuriken") @tagged("canThrow", true) shuriken: Weapon
          ) {
              this.katana = katana;
              this.shuriken = shuriken;
          }
      }

      const ninjaId = "Ninja";
      const weaponId = "Weapon";

      const container = new Container();
      container.bind<Ninja>(ninjaId).to(Ninja);
      container.bind<Weapon>(weaponId).to(Katana).whenTargetTagged("canThrow", false);
      container.bind<Weapon>(weaponId).to(Shuriken).whenTargetTagged("canThrow", true);

      const context = new Context(container); 
      plan(context, false, "Variable", ninjaId);

      const ninja = resolveContext<Ninja>(context);

      expect(ninja instanceof Ninja).eql(true);
      expect(ninja.katana instanceof Katana).eql(true);
      expect(ninja.shuriken instanceof Shuriken).eql(true);

  });

  it("Should be able to resolve plans with constraints on named targets", () => {

      interface Weapon {}

      @injectable()
      class Katana implements Weapon {}

      @injectable()
      class Shuriken implements Weapon {}

      interface Warrior {
          katana: Weapon;
          shuriken: Weapon;
      }

      @injectable()
      class Ninja implements Warrior {
          public katana: Weapon;
          public shuriken: Weapon;
          public constructor(
              @inject("Weapon") @targetName("katana") @named("strong")katana: Weapon,
              @inject("Weapon") @targetName("shuriken") @named("weak") shuriken: Weapon
          ) {
              this.katana = katana;
              this.shuriken = shuriken;
          }
      }

      const ninjaId = "Ninja";
      const weaponId = "Weapon";

      const container = new Container();
      container.bind<Ninja>(ninjaId).to(Ninja);
      container.bind<Weapon>(weaponId).to(Katana).whenTargetTagged(NAMED_TAG, "strong");
      container.bind<Weapon>(weaponId).to(Shuriken).whenTargetTagged(NAMED_TAG, "weak");

      const context = new Context(container); 
      plan(context, false, "Variable", ninjaId);

      const ninja = resolveContext<Ninja>(context);

      expect(ninja instanceof Ninja).eql(true);
      expect(ninja.katana instanceof Katana).eql(true);
      expect(ninja.shuriken instanceof Shuriken).eql(true);

  });

  it("Should be able to resolve plans with custom contextual constraints", () => {

      interface Weapon {}

      @injectable()
      class Katana implements Weapon {}

      @injectable()
      class Shuriken implements Weapon {}

      interface Warrior {
          katana: Weapon;
          shuriken: Weapon;
      }

      @injectable()
      class Ninja implements Warrior {
          public katana: Weapon;
          public shuriken: Weapon;
          public constructor(
              @inject("Weapon") @targetName("katana") katana: Weapon,
              @inject("Weapon") @targetName("shuriken") shuriken: Weapon
          ) {
              this.katana = katana;
              this.shuriken = shuriken;
          }
      }

      const ninjaId = "Ninja";
      const weaponId = "Weapon";

      const container = new Container();
      container.bind<Ninja>(ninjaId).to(Ninja);

      container.bind<Weapon>(weaponId).to(Katana).when((request: EntityRequest) =>
          request.target.name === "katana");

      container.bind<Weapon>(weaponId).to(Shuriken).when((request: EntityRequest) =>
        request.target.name === "shuriken");

      const context = new Context(container); 
      plan(context, false, "Variable", ninjaId);

      const ninja = resolveContext<Ninja>(context);

      expect(ninja instanceof Ninja).eql(true);
      expect(ninja.katana instanceof Katana).eql(true);
      expect(ninja.shuriken instanceof Shuriken).eql(true);
  });

  it("Should be able to resolve plans with multi-injections", () => {

      interface Weapon {
          name: string;
      }

      @injectable()
      class Katana implements Weapon {
          public name = "Katana";
      }

      @injectable()
      class Shuriken implements Weapon {
          public name = "Shuriken";
      }

      interface Warrior {
          katana: Weapon;
          shuriken: Weapon;
      }

      @injectable()
      class Ninja implements Warrior {
          public katana: Weapon;
          public shuriken: Weapon;
          public constructor(
              @multiInject("Weapon") @targetName("weapons") weapons: Weapon[]
          ) {
              this.katana = weapons[0];
              this.shuriken = weapons[1];
          }
      }

      const ninjaId = "Ninja";
      const weaponId = "Weapon";

      const container = new Container();
      container.bind<Ninja>(ninjaId).to(Ninja);
      container.bind<Weapon>(weaponId).to(Katana);
      container.bind<Weapon>(weaponId).to(Shuriken);

      const context = new Context(container); 
      plan(context, false, "Variable", ninjaId);

      const ninja = resolveContext<Ninja>(context);

      expect(ninja instanceof Ninja).eql(true);
      expect(ninja.katana instanceof Katana).eql(true);
      expect(ninja.shuriken instanceof Shuriken).eql(true);

      // if only one value is bound to weaponId
      const container2 = new Container();
      container2.bind<Ninja>(ninjaId).to(Ninja);
      container2.bind<Weapon>(weaponId).to(Katana);

      const context2 = new Context(container2);
      plan(context2, false, "Variable", ninjaId);

      const ninja2 = resolveContext<Ninja>(context2);

      expect(ninja2 instanceof Ninja).eql(true);
      expect(ninja2.katana instanceof Katana).eql(true);

  });

  it("Should be able to resolve plans with activation handlers", () => {

        interface Sword {
            use(): void;
        }

        @injectable()
        class Katana implements Sword {
            public use() {
                return "Used Katana!";
            }
        }

        interface Warrior {
            katana: Katana;
        }

        @injectable()
        class Ninja implements Warrior {
            public katana: Katana;
            public constructor(
                @inject("Katana") katana: Katana
            ) {
                this.katana = katana;
            }
        }

        const ninjaId = "Ninja";
        const katanaId = "Katana";

        const container = new Container();
        container.bind<Ninja>(ninjaId).to(Ninja);

        // This is a global for unit testing but remember
        // that it is not a good idea to use globals
        const timeTracker: string[] = [];

        container.bind<Katana>(katanaId).to(Katana).onActivation((theContext: Context, katana: Katana) => {
            const handler = {
                apply(target: any, thisArgument: any, argumentsList: any[]) {
                    timeTracker.push(`Starting ${target.name} ${new Date().getTime()}`);
                    const result = target.apply(thisArgument, argumentsList);
                    timeTracker.push(`Finished ${target.name} ${new Date().getTime()}`);
                    return result;
                }
            };
            /// create a proxy for method use() own by katana instance about to be injected
            katana.use = new Proxy(katana.use, handler);
            return katana;
        });

        const context = new Context(container); 
        plan(context, false, "Variable", ninjaId);

        const ninja = resolveContext<Ninja>(context);

        expect(ninja.katana.use()).eql("Used Katana!");
        expect(Array.isArray(timeTracker)).eql(true);
        expect(timeTracker.length).eql(2);

  });

  it("Should be able to resolve BindingType.Function bindings", () => {

      const ninjaId = "Ninja";
      const shurikenId = "Shuriken";
      const katanaFactoryId = "KatanaFactory";

      type KatanaFactory = () => Katana;

      interface KatanaBlade {}

      @injectable()
      class KatanaBlade implements KatanaBlade {}

      interface KatanaHandler {}

      @injectable()
      class KatanaHandler implements KatanaHandler {}

      interface Sword {
          handler: KatanaHandler;
          blade: KatanaBlade;
      }

      @injectable()
      class Katana implements Sword {
          public handler: KatanaHandler;
          public blade: KatanaBlade;
          public constructor(handler: KatanaHandler, blade: KatanaBlade) {
              this.handler = handler;
              this.blade = blade;
          }
      }

      interface Shuriken {}

      @injectable()
      class Shuriken implements Shuriken {}

      interface Warrior {
          katanaFactory: KatanaFactory;
          shuriken: Shuriken;
      }

      @injectable()
      class Ninja implements Warrior {
          public constructor(
              @inject(katanaFactoryId) @targetName("katana") public katanaFactory: KatanaFactory,
              @inject(shurikenId) @targetName("shuriken") public shuriken: Shuriken
          ) {
          }
      }

      const container = new Container();
      container.bind<Ninja>(ninjaId).to(Ninja);
      container.bind<Shuriken>(shurikenId).to(Shuriken);

      const katanaFactoryInstance = function() {
          return new Katana(new KatanaHandler(), new KatanaBlade());
      };

      container.bind<KatanaFactory>(katanaFactoryId).toConstantValue(katanaFactoryInstance);

      const context = new Context(container); 
      plan(context, false, "Variable", ninjaId);

      const ninja = resolveContext<Ninja>(context);

      expect(ninja instanceof Ninja).eql(true);
      expect(typeof ninja.katanaFactory === "function").eql(true);
      expect(ninja.katanaFactory() instanceof Katana).eql(true);
      expect(ninja.katanaFactory().handler instanceof KatanaHandler).eql(true);
      expect(ninja.katanaFactory().blade instanceof KatanaBlade).eql(true);
      expect(ninja.shuriken instanceof Shuriken).eql(true);

    });

  it("Should run the @PostConstruct method", () => {

        interface Sword {
            use(): string;
        }

        @injectable()
        class Katana implements Sword {
            private useMessage: string;

            public use() {
                return this.useMessage;
            }

            @postConstruct
            public postConstruct () {
                this.useMessage = "Used Katana!";
            }
        }

        interface Warrior {
            katana: Katana;
        }

        @injectable()
        class Ninja implements Warrior {
            public katana: Katana;
            public constructor(@inject("Katana") katana: Katana) {
                this.katana = katana;
            }
        }
        const ninjaId = "Ninja";
        const katanaId = "Katana";

        const container = new Container();
        container.bind<Ninja>(ninjaId).to(Ninja);

        container.bind<Katana>(katanaId).to(Katana);

        const context = new Context(container); 
        plan(context, false, "Variable", ninjaId);

        const ninja = resolveContext<Ninja>(context);

        expect(ninja.katana.use()).eql("Used Katana!");

    });

  it("Should throw an error if the @postConstruct method throws an error", () => {

        @injectable()
        class Katana {

            @postConstruct
            public postConstruct() {
                throw new Error("Original Message");
            }
        }

        const container = new Container();
        expect(() => Resolver(container).resolve(Katana))
            .to.throw("@postConstruct error in class Katana: Original Message");
    });

  it("Should run the @PostConstruct method of parent class", () => {

        interface Weapon {
            use(): string;
        }

        @injectable()
        abstract class Sword implements Weapon {
            protected useMessage: string;

            @postConstruct
            public postConstruct () {
                this.useMessage = "Used Weapon!";
            }

            public abstract use(): string;
        }

        @injectable()
        class Katana extends Sword {
            public use() {
                return this.useMessage;
            }
        }

        interface Warrior {
            katana: Katana;
        }

        @injectable()
        class Ninja implements Warrior {
            public katana: Katana;
            public constructor(@inject("Katana") katana: Katana) {
                this.katana = katana;
            }
        }
        const ninjaId = "Ninja";
        const katanaId = "Katana";

        const container = new Container();
        container.bind<Ninja>(ninjaId).to(Ninja);

        container.bind<Katana>(katanaId).to(Katana);

        const context = new Context(container); 
        plan(context, false, "Variable", ninjaId);

        const ninja = resolveContext<Ninja>(context);

        expect(ninja.katana.use()).eql("Used Weapon!");

    });

  it("Should run the @PostConstruct method once in the singleton scope", () => {
        let timesCalled = 0;
        @injectable()
        class Katana {
            @postConstruct
            public postConstruct () {
                timesCalled ++;
            }
        }

        @injectable()
        class Ninja {
            public katana: Katana;
            public constructor(@inject("Katana") katana: Katana) {
                this.katana = katana;
            }
        }

        @injectable()
        class Samurai  {
            public katana: Katana;
            public constructor(@inject("Katana") katana: Katana) {
                this.katana = katana;
            }
        }
        const ninjaId = "Ninja";
        const samuraiId = "Samurai";
        const katanaId = "Katana";

        const container = new Container();
        container.bind<Ninja>(ninjaId).to(Ninja);
        container.bind<Samurai>(samuraiId).to(Samurai);
        container.bind<Katana>(katanaId).to(Katana).inSingletonScope();
        Resolver(container).get(ninjaId);
        Resolver(container).get(samuraiId);
        expect(timesCalled).to.be.equal(1);

  });

});
