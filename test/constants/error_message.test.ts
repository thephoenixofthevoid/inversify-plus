import { expect } from "chai";

const POST_CONSTRUCT_ERROR = (...values: any[]) => `@postConstruct error in class ${values[0]}: ${values[1]}`;

describe("ERROR_MSGS", () => {

  it("Should be able to customize POST_CONSTRUCT_ERROR", () => {
      const error = POST_CONSTRUCT_ERROR("a", "b");
      expect(error).eql("@postConstruct error in class a: b");
  });

});
