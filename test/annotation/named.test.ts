declare function __decorate(decorators: ClassDecorator[], target: any, key?: any, desc?: any): void;
declare function __param(paramIndex: number, decorator: ParameterDecorator): ClassDecorator;

import { decorate } from "../../src/inversify";
import { named } from "../../src/inversify";

const DUPLICATED_METADATA = "Metadata key was used more than once in a parameter:";
const INVALID_DECORATOR_OPERATION = "The @inject @multiInject @tagged and @named decorators must be applied to the parameters of a class constructor or a class property.";

interface Weapon {}

class NamedWarrior {

    private _primaryWeapon: Weapon;
    private _secondaryWeapon: Weapon;

    public constructor(
      @named("more_powerful") primary: Weapon,
      @named("less_powerful") secondary: Weapon) {

        this._primaryWeapon = primary;
        this._secondaryWeapon = secondary;
    }

    public debug() {
      return {
        primaryWeapon: this._primaryWeapon,
        secondaryWeapon: this._secondaryWeapon
      };
    }
}

class InvalidDecoratorUsageWarrior {

    private _primaryWeapon: Weapon;
    private _secondaryWeapon: Weapon;

    public constructor(
      primary: Weapon,
      secondary: Weapon
    ) {

          this._primaryWeapon = primary;
          this._secondaryWeapon = secondary;
    }

    public test(a: string) { /*...*/ }

    public debug() {
      return {
        primaryWeapon: this._primaryWeapon,
        secondaryWeapon: this._secondaryWeapon
      };
    }
}

describe("@named", () => {

  it.skip("Should generate metadata for named parameters", () => {

    // const metadataKey = METADATA_KEY.TAGGED;
    // const paramsMetadata = Reflect.getMetadata(metadataKey, NamedWarrior);
    // expect(paramsMetadata).to.be.an("object");

    // expect(paramsMetadata["0"].get(NAMED_TAG)).to.be.eql("more_powerful");
    // expect(paramsMetadata["1"].get(NAMED_TAG)).to.be.eql("less_powerful");
    // expect(paramsMetadata["2"]).to.eq(undefined);
  });

  // it("Should generate metadata for named properties", () => {

  //   class Warrior {
  //     @named("throwable")
  //     public weapon: Weapon;
  //   }

  //   const metadataKey = METADATA_KEY.TAGGED_PROP;
  //   const metadata: any = Reflect.getMetadata(metadataKey, Warrior);

  //   expect(metadata.weapon.get(NAMED_TAG)).to.be.eql("throwable");
  // });

  // it("Should throw when applied multiple times", () => {

  //   const useDecoratorMoreThanOnce = function() {
  //     __decorate([ __param(0, named("a")), __param(0, named("b")) ], InvalidDecoratorUsageWarrior);
  //   };

  //   const msg = `${DUPLICATED_METADATA} ${NAMED_TAG.toString()}`;
  //   expect(useDecoratorMoreThanOnce).to.throw(msg);

  // });

  // it("Should throw when not applied to a constructor", () => {

  //   const useDecoratorOnMethodThatIsNotAConstructor = function() {
  //     __decorate([ __param(0, named("a")) ],
  //                InvalidDecoratorUsageWarrior.prototype,
  //                "test", Object.getOwnPropertyDescriptor(InvalidDecoratorUsageWarrior.prototype, "test"));
  //   };

  //   const msg = `${INVALID_DECORATOR_OPERATION}`;
  //   expect(useDecoratorOnMethodThatIsNotAConstructor).to.throw(msg);

  // });

  it("Should be usable in VanillaJS applications", () => {

    interface Katana {}
    interface Shurien {}

    const VanillaJSWarrior = (function () {
        function NamedVanillaJSWarrior(primary: Katana, secondary: Shurien) {
            // ...
        }
        return NamedVanillaJSWarrior;
    })();

    decorate(named("more_powerful"), VanillaJSWarrior, 0);
    decorate(named("less_powerful"), VanillaJSWarrior, 1);

    // const metadataKey = METADATA_KEY.TAGGED;
    // const paramsMetadata = Reflect.getMetadata(metadataKey, VanillaJSWarrior);
    // expect(paramsMetadata).to.be.an("object");

    // expect(paramsMetadata["0"].get(NAMED_TAG)).to.be.eql("more_powerful");
    // expect(paramsMetadata["1"].get(NAMED_TAG)).to.be.eql("less_powerful");
    // expect(paramsMetadata["2"]).to.eq(undefined);
  });

});
