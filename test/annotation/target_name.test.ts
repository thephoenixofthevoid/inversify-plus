import { decorate } from "../../src/inversify";
import { injectable } from "../../src/inversify";
import { targetName } from "../../src/inversify";
import * as Stubs from "../utils/stubs";

describe("@targetName", () => {

    it("Should generate metadata if declared parameter names", () => {

        @injectable()
        class Warrior {

            public katana: Stubs.Katana;
            public shuriken: Stubs.Shuriken;

            public constructor(
                @targetName("katana") katana: Stubs.Katana,
                @targetName("shuriken") shuriken: Stubs.Shuriken
            ) {

                this.katana = katana;
                this.shuriken = shuriken;
            }
        }

        // const metadata = Reflect.getMetadata(METADATA_KEY.TAGGED, Warrior);

        // expect(metadata["0"].get(METADATA_KEY.NAME_TAG)).to.be.eql("katana");
        // expect(metadata["1"].get(METADATA_KEY.NAME_TAG)).to.be.eql("shuriken");
        // expect(metadata["2"]).to.eq(undefined);
    });

    it("Should be usable in VanillaJS applications", () => {

        interface Katana {}
        interface Shuriken {}

        const VanillaJSWarrior = function (primary: Katana, secondary: Shuriken) {
                // ...
        };

        decorate(targetName("primary"), VanillaJSWarrior, 0);
        decorate(targetName("secondary"), VanillaJSWarrior, 1);

        // const metadata = Reflect.getMetadata(METADATA_KEY.TAGGED, VanillaJSWarrior);

        // expect(metadata["0"].get(METADATA_KEY.NAME_TAG)).to.be.eql("primary");
        // expect(metadata["1"].get(METADATA_KEY.NAME_TAG)).to.be.eql("secondary");
        // expect(metadata["2"]).to.eq(undefined);
    });

});
