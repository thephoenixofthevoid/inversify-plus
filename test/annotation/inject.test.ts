declare function __decorate(decorators: ClassDecorator[], target: any, key?: any, desc?: any): void;
declare function __param(paramIndex: number, decorator: ParameterDecorator): ClassDecorator;

import { expect } from "chai";
import { decorate } from "../../src/inversify";
import { inject } from "../../src/inversify";
import { LazyServiceIdentifer } from "../../src/Base/ClassMetadata";

const DUPLICATED_METADATA = "Metadata key was used more than once in a parameter:";
const UNDEFINED_INJECT_ANNOTATION = `@inject called with undefined.`;
const INVALID_DECORATOR_OPERATION = "The @inject @multiInject @tagged and @named decorators must be applied to the parameters of a class constructor or a class property.";

interface Katana {}
interface Shuriken {}
interface Sword {}
class Katana implements Katana {}
class Shuriken implements Shuriken {}
class Sword implements Sword {}

const lazySwordId = new LazyServiceIdentifer(() => "Sword");

class DecoratedWarrior {

    private _primaryWeapon: Katana;
    private _secondaryWeapon: Shuriken;
    private _tertiaryWeapon: Sword;

    public constructor(
      @inject("Katana") primary: Katana,
      @inject("Shuriken") secondary: Shuriken,
      @inject(lazySwordId) tertiary: Shuriken
    ) {
        this._primaryWeapon = primary;
        this._secondaryWeapon = secondary;
        this._tertiaryWeapon = tertiary;
    }

    public debug() {
      return {
        primaryWeapon: this._primaryWeapon,
        secondaryWeapon: this._secondaryWeapon,
        tertiaryWeapon: this._tertiaryWeapon
      };
    }

}

class InvalidDecoratorUsageWarrior {

    private _primaryWeapon: Katana;
    private _secondaryWeapon: Shuriken;

    public constructor(
      primary: Katana,
      secondary: Shuriken
    ) {

          this._primaryWeapon = primary;
          this._secondaryWeapon = secondary;
    }

    public test(a: string) { /*...*/ }

    public debug() {
      return {
        primaryWeapon: this._primaryWeapon,
        secondaryWeapon: this._secondaryWeapon
      };
    }

}

describe("@inject", () => {

  it.skip("Should generate metadata for named parameters", () => {

    // const metadataKey = METADATA_KEY.TAGGED;
    // const paramsMetadata = Reflect.getMetadata(metadataKey, DecoratedWarrior);
    // expect(paramsMetadata).to.be.an("object");

    // expect(paramsMetadata["0"].get(METADATA_KEY.INJECT_TAG)).to.be.eql("Katana");
    // expect(paramsMetadata["1"].get(METADATA_KEY.INJECT_TAG)).to.be.eql("Shuriken");
    // expect(paramsMetadata["2"].get(METADATA_KEY.INJECT_TAG)).to.be.eql(lazySwordId);
    // expect(paramsMetadata["3"]).to.eq(undefined);
  });

  // it("Should throw when applied multiple times", () => {

  //   const useDecoratorMoreThanOnce = function() {
  //     __decorate([ __param(0, inject("Katana")), __param(0, inject("Shurien")) ], InvalidDecoratorUsageWarrior);
  //   };

  //   const msg = `${DUPLICATED_METADATA} ${METADATA_KEY.INJECT_TAG.toString()}`;
  //   expect(useDecoratorMoreThanOnce).to.throw(msg);

  // });

  // it("Should throw when not applayed to a constructor", () => {

  //   const useDecoratorOnMethodThatIsNotAConstructor = function() {
  //     __decorate([ __param(0, inject("Katana")) ],
  //                InvalidDecoratorUsageWarrior.prototype,
  //                "test", Object.getOwnPropertyDescriptor(InvalidDecoratorUsageWarrior.prototype, "test"));
  //   };

  //   const msg = `${INVALID_DECORATOR_OPERATION}`;
  //   expect(useDecoratorOnMethodThatIsNotAConstructor).to.throw(msg);

  // });

  it("Should throw when applied with undefined", () => {

    // this can happen when there is circular dependency between symbols
    const useDecoratorWithUndefined = function() {
      __decorate([ __param(0, inject(undefined as any)) ], InvalidDecoratorUsageWarrior);
    };

    expect(useDecoratorWithUndefined).to.throw(UNDEFINED_INJECT_ANNOTATION);

  });

  it("Should be usable in VanillaJS applications", () => {

    interface Shuriken {}

    const VanillaJSWarrior = (function () {
        function Warrior(primary: Katana, secondary: Shuriken) {
            // ...
        }
        return Warrior;
    })();

    decorate(inject("Katana"), VanillaJSWarrior, 0);
    decorate(inject("Shuriken"), VanillaJSWarrior, 1);

    // const paramsMetadata = Reflect.getMetadata(METADATA_KEY.TAGGED, VanillaJSWarrior);
    // expect(paramsMetadata).to.be.an("object");

    // expect(paramsMetadata["0"].get(METADATA_KEY.INJECT_TAG)).to.be.eql("Katana");
    // expect(paramsMetadata["1"].get(METADATA_KEY.INJECT_TAG)).to.be.eql("Shuriken");
    // expect(paramsMetadata["2"]).to.eq(undefined);
  });

});
