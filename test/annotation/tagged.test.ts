declare function __decorate(decorators: ClassDecorator[], target: any, key?: any, desc?: any): void;
declare function __param(paramIndex: number, decorator: ParameterDecorator): ClassDecorator;

import { decorate } from "../../src/inversify";
import { tagged } from "../../src/inversify";

const DUPLICATED_METADATA = "Metadata key was used more than once in a parameter:";
const INVALID_DECORATOR_OPERATION = "The @inject @multiInject @tagged and @named decorators must be applied to the parameters of a class constructor or a class property.";

interface Weapon {}

class TaggedWarrior {

    private _primaryWeapon: Weapon;
    private _secondaryWeapon: Weapon;

    public constructor(
      @tagged("power", 1) primary: Weapon,
      @tagged("power", 2) secondary: Weapon) {

          this._primaryWeapon = primary;
          this._secondaryWeapon = secondary;
    }
    public debug() {
      return {
        primaryWeapon: this._primaryWeapon,
        secondaryWeapon: this._secondaryWeapon
      };
    }
}

class DoubleTaggedWarrior {

    private _primaryWeapon: Weapon;
    private _secondaryWeapon: Weapon;

    public constructor(
      @tagged("power", 1) @tagged("distance", 1) primary: Weapon,
      @tagged("power", 2) @tagged("distance", 5) secondary: Weapon) {

          this._primaryWeapon = primary;
          this._secondaryWeapon = secondary;
    }
    public debug() {
      return {
        primaryWeapon: this._primaryWeapon,
        secondaryWeapon: this._secondaryWeapon
      };
    }
}

class InvalidDecoratorUsageWarrior {

    private _primaryWeapon: Weapon;
    private _secondaryWeapon: Weapon;

    public constructor(
      primary: Weapon,
      secondary: Weapon) {

        this._primaryWeapon = primary;
        this._secondaryWeapon = secondary;
    }

    public test(a: string) { /*...*/ }

    public debug() {
      return {
        primaryWeapon: this._primaryWeapon,
        secondaryWeapon: this._secondaryWeapon
      };
    }
}

describe("@Tagged", () => {

  // it("Should generate metadata for tagged parameters", () => {

  //   const metadataKey = METADATA_KEY.TAGGED;
  //   const paramsMetadata = Reflect.getMetadata(metadataKey, TaggedWarrior);
  //   expect(paramsMetadata).to.be.an("object");

  //   expect(paramsMetadata["0"].get("power")).to.be.eql(1);
  //   expect(paramsMetadata["1"].get("power")).to.be.eql(2);
  //   expect(paramsMetadata["2"]).to.eq(undefined);
  // });

  // it("Should generate metadata for tagged properties", () => {

  //   class Warrior {
  //     @tagged("throwable", false)
  //     public weapon: Weapon;
  //   }

  //   const metadataKey = METADATA_KEY.TAGGED_PROP;
  //   const metadata: any = Reflect.getMetadata(metadataKey, Warrior);

  //   expect(metadata.weapon.get("throwable")).to.be.eql(false);
  // });

  // it("Should generate metadata for parameters tagged multiple times", () => {
  //   const metadataKey = METADATA_KEY.TAGGED;
  //   const paramsMetadata = Reflect.getMetadata(metadataKey, DoubleTaggedWarrior);
  //   expect(paramsMetadata).to.be.an("object");

  //   expect(paramsMetadata["0"].get("distance")).to.be.eql(1);
  //   expect(paramsMetadata["0"].get("power")).to.be.eql(1);

  //   expect(paramsMetadata["1"].get("distance")).to.be.eql(5);
  //   expect(paramsMetadata["1"].get("power")).to.be.eql(2);

  //   expect(paramsMetadata["2"]).to.eq(undefined);
  // });

  // it("Should throw when applied multiple times", () => {

  //   const metadataKey = "a";

  //   const useDecoratorMoreThanOnce = function() {
  //     __decorate([ __param(0, tagged(metadataKey, 1)), __param(0, tagged(metadataKey, 2)) ], InvalidDecoratorUsageWarrior);
  //   };

  //   const msg = `${DUPLICATED_METADATA} ${metadataKey}`;
  //   expect(useDecoratorMoreThanOnce).to.throw(msg);

  // });

  // it("Should throw when not applied to a constructor", () => {

  //   const useDecoratorOnMethodThatIsNotAConstructor = function() {
  //     __decorate([ __param(0, tagged("a", 1)) ],
  //                InvalidDecoratorUsageWarrior.prototype,
  //                "test", Object.getOwnPropertyDescriptor(InvalidDecoratorUsageWarrior.prototype, "test"));
  //   };

  //   const msg = INVALID_DECORATOR_OPERATION;
  //   expect(useDecoratorOnMethodThatIsNotAConstructor).to.throw(msg);

  // });

  it("Should be usable in VanillaJS applications", () => {

    interface Katana {}
    interface Shuriken {}

    const VanillaJSWarrior = (function () {
        function TaggedVanillaJSWarrior(primary: Katana, secondary: Shuriken) {
            // ...
        }
        return TaggedVanillaJSWarrior;
    })();

    decorate(tagged("power", 1), VanillaJSWarrior, 0);
    decorate(tagged("power", 2), VanillaJSWarrior, 1);

    // const metadataKey = METADATA_KEY.TAGGED;
    // const paramsMetadata = Reflect.getMetadata(metadataKey, VanillaJSWarrior);
    // expect(paramsMetadata).to.be.an("object");

    // expect(paramsMetadata["0"].get("power")).to.be.eql(1);
    // expect(paramsMetadata["1"].get("power")).to.be.eql(2);
    // expect(paramsMetadata["2"]).to.eq(undefined);
  });

});
