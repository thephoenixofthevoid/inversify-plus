import { expect } from "chai";
import { postConstruct } from "../../src/inversify";
import { decorate } from "../../src/inversify";

const MULTIPLE_POST_CONSTRUCT_METHODS = "Cannot apply @postConstruct decorator multiple times in the same class";

describe("@postConstruct", () => {

    it("Should generate metadata for the decorated method", () => {
        class Katana {
            private useMessage: string;

            public use() {
                return "Used Katana!";
            }

            @postConstruct
            public testMethod() {
                this.useMessage = "Used Katana!";
            }
            public debug() {
                return this.useMessage;
            }
        }
        //const metadata = Reflect.getMetadata(METADATA_KEY.POST_CONSTRUCT, Katana);
        //expect(metadata).to.be.equal("testMethod");
    });

    it("Should throw when applied multiple times", () => {
        function setup() {
            class Katana {
                @postConstruct
                public testMethod1() {/* ... */ }

                @postConstruct
                public testMethod2() {/* ... */ }
            }
            Katana.toString();
        }
        expect(setup).to.throw(MULTIPLE_POST_CONSTRUCT_METHODS);
    });

    it("Should be usable in VanillaJS applications", () => {

        const VanillaJSWarrior = function () {
            // ...
        };
        VanillaJSWarrior.prototype.testMethod = function () {
            // ...
        };

        decorate(postConstruct, VanillaJSWarrior.prototype, "testMethod");

        //const metadata = Reflect.getMetadata(METADATA_KEY.POST_CONSTRUCT, VanillaJSWarrior);
        //expect(metadata).to.be.equal("testMethod");
    });

});
