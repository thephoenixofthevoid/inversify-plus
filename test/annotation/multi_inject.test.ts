declare function __decorate(decorators: ClassDecorator[], target: any, key?: any, desc?: any): void;
declare function __param(paramIndex: number, decorator: ParameterDecorator): ClassDecorator;

import { decorate } from "../../src/inversify";
import { multiInject } from "../../src/inversify";

const DUPLICATED_METADATA = "Metadata key was used more than once in a parameter:";
const INVALID_DECORATOR_OPERATION = "The @inject @multiInject @tagged and @named decorators must be applied to the parameters of a class constructor or a class property.";

interface Weapon {}

class DecoratedWarrior {

    private _primaryWeapon: Weapon;
    private _secondaryWeapon: Weapon;

    public constructor(
      @multiInject("Weapon") weapons: Weapon[]
    ) {

        this._primaryWeapon = weapons[0];
        this._secondaryWeapon = weapons[1];
    }

    public mock() {
      return `${JSON.stringify(this._primaryWeapon)} ${JSON.stringify(this._secondaryWeapon)}`;
    }

}

class InvalidDecoratorUsageWarrior {

    private _primaryWeapon: Weapon;
    private _secondaryWeapon: Weapon;

    public constructor(
      weapons: Weapon[]
    ) {
          this._primaryWeapon = weapons[0];
          this._secondaryWeapon = weapons[1];
    }

    public test(a: string) { /*...*/ }

    public debug() {
      return {
        primaryWeapon: this._primaryWeapon,
        secondaryWeapon: this._secondaryWeapon
      };
    }

}

describe("@multiInject", () => {

  it.skip("Should generate metadata for named parameters", () => {
    // const metadataKey = METADATA_KEY.TAGGED;
    // const paramsMetadata = Reflect.getMetadata(metadataKey, DecoratedWarrior);
    // expect(paramsMetadata).to.be.an("object");

    // expect(paramsMetadata["0"].get(METADATA_KEY.MULTI_INJECT_TAG)).to.be.eql("Weapon");
    // expect(paramsMetadata["1"]).to.eq(undefined);
  });

  // it("Should throw when applied multiple times", () => {

  //   const useDecoratorMoreThanOnce = function() {
  //     __decorate([ __param(0, multiInject("Weapon")), __param(0, multiInject("Weapon")) ], InvalidDecoratorUsageWarrior);
  //   };

  //   const msg = `${DUPLICATED_METADATA} ${METADATA_KEY.MULTI_INJECT_TAG.toString()}`;
  //   expect(useDecoratorMoreThanOnce).to.throw(msg);

  // });

  // it("Should throw when not applied to a constructor", () => {

  //   const useDecoratorOnMethodThatIsNotAConstructor = function() {
  //     __decorate([ __param(0, multiInject("Weapon")) ],
  //                InvalidDecoratorUsageWarrior.prototype,
  //                "test", Object.getOwnPropertyDescriptor(InvalidDecoratorUsageWarrior.prototype, "test"));
  //   };

  //   const msg = `${INVALID_DECORATOR_OPERATION}`;
  //   expect(useDecoratorOnMethodThatIsNotAConstructor).to.throw(msg);

  // });

  it("Should be usable in VanillaJS applications", () => {

    interface Katana {}
    interface Shurien {}

    const VanillaJSWarrior = (function () {
        function Warrior(primary: Katana, secondary: Shurien) {
            // ...
        }
        return Warrior;
    })();

    decorate(multiInject("Weapon"), VanillaJSWarrior, 0);

    // const metadataKey = METADATA_KEY.TAGGED;
    // const paramsMetadata = Reflect.getMetadata(metadataKey, VanillaJSWarrior);
    // expect(paramsMetadata).to.be.an("object");

    // expect(paramsMetadata["0"].get(METADATA_KEY.MULTI_INJECT_TAG)).to.be.eql("Weapon");
    // expect(paramsMetadata["1"]).to.eq(undefined);
  });

});
