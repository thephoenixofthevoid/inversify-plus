import { expect } from "chai";
import { Binding } from "../../src/Base/Binding";
import { TransientScoped } from "../../src/Strategies/CachingStrategy";
import * as Stubs from "../utils/stubs";

describe("Binding", () => {

  it("Should set its own properties correctly", () => {

    const fooIdentifier = "FooInterface";
    const fooBinding =  new Binding<Stubs.FooInterface>(fooIdentifier);
    expect(fooBinding.serviceIdentifier).eql(fooIdentifier);
    expect(fooBinding.value).eql(null);
    expect(fooBinding.scope).eql(TransientScoped);
    //expect(fooBinding.id).to.be.a("number");
  });

});
