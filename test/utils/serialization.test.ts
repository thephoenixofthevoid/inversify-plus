import { expect } from "chai";
import { MODE_TAG } from "../../src/Constants";
import { Target } from "../../src/Base/Target";
import { getFunctionName } from "../../src/Errors";

describe("Serialization", () => {

    it("Should return a good function name", () => {

        function testFunction() {
            return false;
        }

        expect(getFunctionName(testFunction)).eql("testFunction");

    });

    it("Should return a good function name by using the regex", () => {

        const testFunction: any = { name: null };
        testFunction.toString = () =>
            "function testFunction";

        expect(getFunctionName(testFunction)).eql("testFunction");

    });

    it.skip("Should not fail when target is not named or tagged", () => {
        const serviceIdentifier = "SomeTypeId";
        const target = Target.for(serviceIdentifier);
        target.set(MODE_TAG, "Variable")
        //expect(getListMetadataForTarget(target)).to.eql(` ${serviceIdentifier}`);
    });

});
