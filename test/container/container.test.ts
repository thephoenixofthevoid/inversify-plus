import { expect } from "chai";
import * as sinon from "sinon";
import { Id } from "../../src/inversify";
import { injectable } from "../../src/inversify";
import { Container } from "../../src/inversify";
import { ContainerModule, Loader } from "../../src/Addons/ContainerModule";
import { merge} from "../../src/Addons/merge";
import { Resolver } from "../../src/API/Resolver";
import { NAMED_TAG } from "../../src/Constants";
import { isBoundTagged } from "../../src/IocContainer";

function getServiceIdentifierAsString(id: Id): string {
    if (typeof id === "function") return id.name;
    if (typeof id === "symbol") return id.toString();
    return `${id}`
}

function createChild(container: Container, opt?: Partial<any>): Container {
    return new Container(container);;
}

const NOT_REGISTERED = "No matching bindings found for serviceIdentifier:";
const AMBIGUOUS_MATCH = "Ambiguous match found for serviceIdentifier:";

function getBindingDictionary(cntnr: any) {
    return cntnr.bindings;
}

type Dictionary = any

describe("Container", () => {

    let sandbox: sinon.SinonSandbox;

    beforeEach(() => {
        sandbox = sinon.createSandbox();
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("Should be able to use modules as configuration", () => {

      interface Ninja {}
      interface Katana {}
      interface Shuriken {}

      @injectable()
      class Katana implements Katana {}

      @injectable()
      class Shuriken implements Shuriken {}

      @injectable()
      class Ninja implements Ninja {}

      const warriors = new ContainerModule((bind) => {
          bind<Ninja>("Ninja").to(Ninja);
      });

      const weapons = new ContainerModule((bind) => {
          bind<Katana>("Katana").to(Katana);
          bind<Shuriken>("Shuriken").to(Shuriken);
      });

      const container = new Container();
      Loader(container).load(warriors);
      Loader(container).load(weapons);

      let map: Dictionary = getBindingDictionary(container);
      expect(map.has("Ninja")).equal(true);
      expect(map.has("Katana")).equal(true);
      expect(map.has("Shuriken")).equal(true);
      expect(map.size).equal(3);

      const tryGetNinja = () => { Resolver(container).get("Ninja"); };
      const tryGetKatana = () => { Resolver(container).get("Katana"); };
      const tryGetShuruken = () => { Resolver(container).get("Shuriken"); };

      Loader(container).unload(warriors);
      map = getBindingDictionary(container);
      expect(map.size).equal(2);
      expect(tryGetNinja).to.throw(NOT_REGISTERED);
      expect(tryGetKatana).not.to.throw();
      expect(tryGetShuruken).not.to.throw();

      Loader(container).unload(weapons);
      map = getBindingDictionary(container);
      expect(map.size).equal(0);
      expect(tryGetNinja).to.throw(NOT_REGISTERED);
      expect(tryGetKatana).to.throw(NOT_REGISTERED);
      expect(tryGetShuruken).to.throw(NOT_REGISTERED);

  });

    it("Should be able to store bindings", () => {

      interface Ninja {}

      @injectable()
      class Ninja implements Ninja {}
      const ninjaId = "Ninja";

      const container = new Container();
      container.bind<Ninja>(ninjaId).to(Ninja);

      const map: Dictionary = getBindingDictionary(container);
      expect(map.size).equal(1);
      expect(map.has(ninjaId)).equal(true);

  });

    //it("Should have an unique identifier", () => {

      //const container1 = new Container();
      //const container2 = new Container();
      //expect(container1.id).to.be.a("number");
      //expect(container2.id).to.be.a("number");
      //expect(container1.id).not.equal(container2.id);

  //});

    it("Should unbind a binding when requested", () => {

      interface Ninja {}

      @injectable()
      class Ninja implements Ninja {}
      const ninjaId = "Ninja";

      const container = new Container();
      container.bind<Ninja>(ninjaId).to(Ninja);

      const map: Dictionary = getBindingDictionary(container);
      expect(map.has(ninjaId)).equal(true);

      container.unbind(ninjaId);
      expect(map.has(ninjaId)).equal(false);
      expect(map.size).equal(0);

  });

//   BREAKING CHANGE
//   it("Should throw when cannot unbind", () => {
//       const serviceIdentifier = "Ninja";
//       const container = new Container();
//       const throwFunction = () => { container.unbind("Ninja"); };
//       expect(throwFunction).to.throw(`${CANNOT_UNBIND} ${getServiceIdentifierAsString(serviceIdentifier)}`);
//   });

    it("Should unbind a binding when requested", () => {

      interface Ninja {}

      @injectable()
      class Ninja implements Ninja {}
      interface Samurai {}

      @injectable()
      class Samurai implements Samurai {}

      const ninjaId = "Ninja";
      const samuraiId = "Samurai";

      const container = new Container();
      container.bind<Ninja>(ninjaId).to(Ninja);
      container.bind<Samurai>(samuraiId).to(Samurai);

      let map: Dictionary = getBindingDictionary(container);

      expect(map.size).equal(2);
      expect(map.has(ninjaId)).equal(true);
      expect(map.has(samuraiId)).equal(true);

      container.unbind(ninjaId);
      map = getBindingDictionary(container);
      expect(map.size).equal(1);

  });

    it("Should be able unbound all dependencies", () => {

      interface Ninja {}

      @injectable()
      class Ninja implements Ninja {}

      interface Samurai {}

      @injectable()
      class Samurai implements Samurai {}

      const ninjaId = "Ninja";
      const samuraiId = "Samurai";

      const container = new Container();
      container.bind<Ninja>(ninjaId).to(Ninja);
      container.bind<Samurai>(samuraiId).to(Samurai);

      let map: Dictionary = getBindingDictionary(container);

      expect(map.size).equal(2);
      expect(map.has(ninjaId)).equal(true);
      expect(map.has(samuraiId)).equal(true);

      container.unbindAll();
      map = getBindingDictionary(container);
      expect(map.size).equal(0);

  });

    it("Should NOT be able to get unregistered services", () => {

      interface Ninja {}

      @injectable()
      class Ninja implements Ninja {}
      const ninjaId = "Ninja";

      const container = new Container();
      const throwFunction = () => { Resolver(container).get<Ninja>(ninjaId); };

      expect(throwFunction).to.throw(`${NOT_REGISTERED} ${ninjaId}`);
  });

    it("Should NOT be able to get ambiguous match", () => {

      interface Warrior {}

      @injectable()
      class Ninja implements Warrior {}

      @injectable()
      class Samurai implements Warrior {}

      const warriorId = "Warrior";

      const container = new Container();
      container.bind<Warrior>(warriorId).to(Ninja);
      container.bind<Warrior>(warriorId).to(Samurai);

      const dictionary: Dictionary = getBindingDictionary(container);

      expect(dictionary.size).equal(1);

      const throwFunction = () => { Resolver(container).get<Warrior>(warriorId); };
      expect(throwFunction).to.throw(`${AMBIGUOUS_MATCH} ${warriorId}`);

  });

    it("Should NOT be able to getAll of an unregistered services", () => {

      interface Ninja {}

      @injectable()
      class Ninja implements Ninja {}
      const ninjaId = "Ninja";

      const container = new Container();
      const throwFunction = () => { Resolver(container).getAll<Ninja>(ninjaId); };

      expect(throwFunction).to.throw(`${NOT_REGISTERED} ${ninjaId}`);

  });

    it("Should be able to get a string literal identifier as a string", () => {
        const Katana = "Katana";
        const KatanaStr = getServiceIdentifierAsString(Katana);
        expect(KatanaStr).to.equal("Katana");
    });

    it("Should be able to get a symbol identifier as a string", () => {
        const KatanaSymbol = Symbol.for("Katana");
        const KatanaStr = getServiceIdentifierAsString(KatanaSymbol);
        expect(KatanaStr).to.equal("Symbol(Katana)");
    });

    it("Should be able to get a class identifier as a string", () => {
        class Katana {}
        const KatanaStr = getServiceIdentifierAsString(Katana);
        expect(KatanaStr).to.equal("Katana");
    });

    // it("Should be able to snapshot and restore container", () => {

    //     interface Warrior {
    //     }

    //     @injectable
    //     class Ninja implements Warrior {}

    //     @injectable
    //     class Samurai implements Warrior {}

    //     const container = new Container();
    //     container.bind<Warrior>(Ninja).to(Ninja);
    //     container.bind<Warrior>(Samurai).to(Samurai);

    //     expect(Resolver(container).get(Samurai)).to.be.instanceOf(Samurai);
    //     expect(Resolver(container).get(Ninja)).to.be.instanceOf(Ninja);

    //     container.snapshot(); // snapshot container = v1

    //     container.unbind(Ninja);
    //     expect(Resolver(container).get(Samurai)).to.be.instanceOf(Samurai);
    //     expect(() => Resolver(container).get(Ninja)).to.throw();

    //     container.snapshot(); // snapshot container = v2
    //     expect(() => Resolver(container).get(Ninja)).to.throw();

    //     container.bind<Warrior>(Ninja).to(Ninja);
    //     expect(Resolver(container).get(Samurai)).to.be.instanceOf(Samurai);
    //     expect(Resolver(container).get(Ninja)).to.be.instanceOf(Ninja);

    //     container.restore(); // restore container to v2
    //     expect(Resolver(container).get(Samurai)).to.be.instanceOf(Samurai);
    //     expect(() => Resolver(container).get(Ninja)).to.throw();

    //     container.restore(); // restore container to v1
    //     expect(Resolver(container).get(Samurai)).to.be.instanceOf(Samurai);
    //     expect(Resolver(container).get(Ninja)).to.be.instanceOf(Ninja);

    //     expect(() => container.restore()).to.throw(NO_MORE_SNAPSHOTS_AVAILABLE);
    // });

    it("Should be able to check is there are bindings available for a given identifier", () => {

        interface Warrior {}
        const warriorId = "Warrior";
        const warriorSymbol = Symbol.for("Warrior");

        @injectable()
        class Ninja implements Warrior {}

        const container = new Container();
        container.bind<Warrior>(Ninja).to(Ninja);
        container.bind<Warrior>(warriorId).to(Ninja);
        container.bind<Warrior>(warriorSymbol).to(Ninja);

        expect(container.isBound(Ninja)).equal(true);
        expect(container.isBound(warriorId)).equal(true);
        expect(container.isBound(warriorSymbol)).equal(true);

        interface Katana {}
        const katanaId = "Katana";
        const katanaSymbol = Symbol.for("Katana");

        @injectable()
        class Katana implements Katana {}

        expect(container.isBound(Katana)).equal(false);
        expect(container.isBound(katanaId)).equal(false);
        expect(container.isBound(katanaSymbol)).equal(false);

    });

    it("Should be able to get services from parent container", () => {
        const weaponIdentifier = "Weapon";

        @injectable()
        class Katana {}

        const container = new Container();
        container.bind(weaponIdentifier).to(Katana);

        const childContainer = new Container();
        childContainer.parent = container;

        const secondChildContainer = new Container();
        secondChildContainer.parent = childContainer;

        expect(Resolver(secondChildContainer).get(weaponIdentifier)).to.be.instanceOf(Katana);
    });

    it("Should be able to check if services are bound from parent container", () => {
        const weaponIdentifier = "Weapon";

        @injectable()
        class Katana {}

        const container = new Container();
        container.bind(weaponIdentifier).to(Katana);

        const childContainer = new Container();
        childContainer.parent = container;

        const secondChildContainer = new Container();
        secondChildContainer.parent = childContainer;

        expect(secondChildContainer.isBound(weaponIdentifier)).to.be.equal(true);
    });

    it("Should prioritize requested container to resolve a service identifier", () => {
        const weaponIdentifier = "Weapon";

        @injectable()
        class Katana {}

        @injectable()
        class DivineRapier {}

        const container = new Container();
        container.bind(weaponIdentifier).to(Katana);

        const childContainer = new Container();
        childContainer.parent = container;

        const secondChildContainer = new Container();
        secondChildContainer.parent = childContainer;
        secondChildContainer.bind(weaponIdentifier).to(DivineRapier);

        expect(Resolver(secondChildContainer).get(weaponIdentifier)).to.be.instanceOf(DivineRapier);
    });

    it("Should be able to resolve named multi-injection", () => {

        interface Intl {
            hello?: string;
            goodbye?: string;
        }

        const container = new Container();
        container.bind<Intl>("Intl").toConstantValue({ hello: "bonjour" }).whenTargetTagged(NAMED_TAG, "fr");
        container.bind<Intl>("Intl").toConstantValue({ goodbye: "au revoir" }).whenTargetTagged(NAMED_TAG, "fr");
        container.bind<Intl>("Intl").toConstantValue({ hello: "hola" }).whenTargetTagged(NAMED_TAG, "es");
        container.bind<Intl>("Intl").toConstantValue({ goodbye: "adios" }).whenTargetTagged(NAMED_TAG, "es");

        const fr = Resolver(container).getAllTagged<Intl>("Intl", NAMED_TAG, "fr");
        expect(fr.length).to.equal(2);
        expect(fr[0].hello).to.equal("bonjour");
        expect(fr[1].goodbye).to.equal("au revoir");

        const es = Resolver(container).getAllTagged<Intl>("Intl", NAMED_TAG, "es");
        expect(es.length).to.equal(2);
        expect(es[0].hello).to.equal("hola");
        expect(es[1].goodbye).to.equal("adios");

    });

    it("Should be able to resolve tagged multi-injection", () => {

        interface Intl {
            hello?: string;
            goodbye?: string;
        }

        const container = new Container();
        container.bind<Intl>("Intl").toConstantValue({ hello: "bonjour" }).whenTargetTagged("lang", "fr");
        container.bind<Intl>("Intl").toConstantValue({ goodbye: "au revoir" }).whenTargetTagged("lang", "fr");
        container.bind<Intl>("Intl").toConstantValue({ hello: "hola" }).whenTargetTagged("lang", "es");
        container.bind<Intl>("Intl").toConstantValue({ goodbye: "adios" }).whenTargetTagged("lang", "es");

        const fr = Resolver(container).getAllTagged<Intl>("Intl", "lang", "fr");
        expect(fr.length).to.equal(2);
        expect(fr[0].hello).to.equal("bonjour");
        expect(fr[1].goodbye).to.equal("au revoir");

        const es = Resolver(container).getAllTagged<Intl>("Intl", "lang", "es");
        expect(es.length).to.equal(2);
        expect(es[0].hello).to.equal("hola");
        expect(es[1].goodbye).to.equal("adios");

    });

    it.skip("Should be able configure the default scope at a global level", () => {

        interface Warrior {
            health: number;
            takeHit(damage: number): void;
        }

        @injectable()
        class Ninja implements Warrior {
            public health: number;
            public constructor() {
                this.health = 100;
            }
            public takeHit(damage: number) {
                this.health = this.health - damage;
            }
        }

        const TYPES = {
            Warrior: "Warrior"
        };

        const container1 = new Container();
        container1.bind<Warrior>(TYPES.Warrior).to(Ninja);

        const transientNinja1 = Resolver(container1).get<Warrior>(TYPES.Warrior);
        expect(transientNinja1.health).to.equal(100);
        transientNinja1.takeHit(10);
        expect(transientNinja1.health).to.equal(90);

        const transientNinja2 = Resolver(container1).get<Warrior>(TYPES.Warrior);
        expect(transientNinja2.health).to.equal(100);
        transientNinja2.takeHit(10);
        expect(transientNinja2.health).to.equal(90);

        const container2 = new Container();
        container2.bind<Warrior>(TYPES.Warrior).to(Ninja);

        const singletonNinja1 = Resolver(container2).get<Warrior>(TYPES.Warrior);
        expect(singletonNinja1.health).to.equal(100);
        singletonNinja1.takeHit(10);
        expect(singletonNinja1.health).to.equal(90);

        const singletonNinja2 = Resolver(container2).get<Warrior>(TYPES.Warrior);
        expect(singletonNinja2.health).to.equal(90);
        singletonNinja2.takeHit(10);
        expect(singletonNinja2.health).to.equal(80);

    });

    // it.skip("Should be able to configure automatic binding for @injectable decorated classes", () => {

    //     @injectable
    //     class Katana {}

    //     @injectable
    //     class Shuriken {}

    //     @injectable
    //     class Ninja {
    //         public constructor(public weapon: Katana) {}
    //     }

    //     class Samurai {}

    //     const container1 = new Container({autoBindInjectable: true});
    //     const katana1 = container1.get(Katana);
    //     const ninja1 = container1.get(Ninja);
    //     expect(katana1).to.be.an.instanceof(Katana);
    //     expect(katana1).to.not.equal(container1.get(Katana));
    //     expect(ninja1).to.be.an.instanceof(Ninja);
    //     expect(ninja1).to.not.equal(container1.get(Ninja));
    //     expect(ninja1.weapon).to.be.an.instanceof(Katana);
    //     expect(ninja1.weapon).to.not.equal(container1.get(Ninja).weapon);
    //     expect(ninja1.weapon).to.not.equal(katana1);

    //     const container2 = new Container({defaultScope: "Singleton", autoBindInjectable: true});
    //     const katana2 = container2.get(Katana);
    //     const ninja2 = container2.get(Ninja);
    //     expect(katana2).to.be.an.instanceof(Katana);
    //     expect(katana2).to.equal(container2.get(Katana));
    //     expect(ninja2).to.be.an.instanceof(Ninja);
    //     expect(ninja2).to.equal(container2.get(Ninja));
    //     expect(ninja2.weapon).to.be.an.instanceof(Katana);
    //     expect(ninja2.weapon).to.equal(container2.get(Ninja).weapon);
    //     expect(ninja2.weapon).to.equal(katana2);

    //     const container3 = new Container({autoBindInjectable: true});
    //     container3.bind(Katana).toSelf().inSingletonScope();
    //     const katana3 = container3.get(Katana);
    //     const ninja3 = container3.get(Ninja);
    //     expect(katana3).to.be.an.instanceof(Katana);
    //     expect(katana3).to.equal(container3.get(Katana));
    //     expect(ninja3).to.be.an.instanceof(Ninja);
    //     expect(ninja3).to.not.equal(container3.get(Ninja));
    //     expect(ninja3.weapon).to.be.an.instanceof(Katana);
    //     expect(ninja3.weapon).to.equal(container3.get(Ninja).weapon);
    //     expect(ninja3.weapon).to.equal(katana3);

    //     const container4 = new Container({autoBindInjectable: true});
    //     container4.bind(Katana).to(Shuriken);
    //     const katana4 = container4.get(Katana);
    //     const ninja4 = container4.get(Ninja);
    //     expect(katana4).to.be.an.instanceof(Shuriken);
    //     expect(katana4).to.not.equal(container4.get(Katana));
    //     expect(ninja4).to.be.an.instanceof(Ninja);
    //     expect(ninja4).to.not.equal(container4.get(Ninja));
    //     expect(ninja4.weapon).to.be.an.instanceof(Shuriken);
    //     expect(ninja4.weapon).to.not.equal(container4.get(Ninja).weapon);
    //     expect(ninja4.weapon).to.not.equal(katana4);

    //     const container5 = new Container({autoBindInjectable: true});
    //     expect(() => container5.get(Samurai)).to.throw(NOT_REGISTERED);

    // });

    it.skip("Should be throw an exception if incorrect options is provided", () => {

        const invalidOptions1: any = () => 0;
        const wrong1 = () => new Container(invalidOptions1);
        expect(wrong1).to.throw();

        const invalidOptions2: any = { autoBindInjectable: "wrongValue" };
        const wrong2 = () => new Container(invalidOptions2);
        expect(wrong2).to.throw();

        const invalidOptions3: any = { defaultScope: "wrongValue" };
        const wrong3 = () => new Container(invalidOptions3);
        expect(wrong3).to.throw();

    });

    it("Should be able to merge multiple containers", () => {

        @injectable()
        class Ninja {
            public name = "Ninja";
        }

        @injectable()
        class Shuriken {
            public name = "Shuriken";
        }

        const CHINA_EXPANSION_TYPES = {
            Ninja: "Ninja",
            Shuriken: "Shuriken"
        };

        const chinaExpansionContainer = new Container();
        chinaExpansionContainer.bind<Ninja>(CHINA_EXPANSION_TYPES.Ninja).to(Ninja);
        chinaExpansionContainer.bind<Shuriken>(CHINA_EXPANSION_TYPES.Shuriken).to(Shuriken);

        @injectable()
        class Samurai {
            public name = "Samurai";
        }

        @injectable()
        class Katana {
            public name = "Katana";
        }

        const JAPAN_EXPANSION_TYPES = {
            Katana: "Katana",
            Samurai: "Samurai"
        };

        const japanExpansionContainer = new Container();
        japanExpansionContainer.bind<Samurai>(JAPAN_EXPANSION_TYPES.Samurai).to(Samurai);
        japanExpansionContainer.bind<Katana>(JAPAN_EXPANSION_TYPES.Katana).to(Katana);

        const gameContainer = merge(chinaExpansionContainer, japanExpansionContainer);
        expect(Resolver(gameContainer).get<Ninja>(CHINA_EXPANSION_TYPES.Ninja).name).to.equal("Ninja");
        expect(Resolver(gameContainer).get<Shuriken>(CHINA_EXPANSION_TYPES.Shuriken).name).to.equal("Shuriken");
        expect(Resolver(gameContainer).get<Samurai>(JAPAN_EXPANSION_TYPES.Samurai).name).to.equal("Samurai");
        expect(Resolver(gameContainer).get<Katana>(JAPAN_EXPANSION_TYPES.Katana).name).to.equal("Katana");

    });

    it("Should be able create a child containers", () => {
        const parent = new Container();
        const child = createChild(parent);
        if (child.parent === null) {
            throw new Error("Parent should not be null");
        }
        expect(child.parent).to.equal(parent);
    });

    it.skip("Should inherit parent container options", () => {
        @injectable()
        class Warrior { }

        const parent = new Container()
        const child = createChild(parent);
        child.bind(Warrior).toSelf().inSingletonScope();

        const singletonWarrior1 = Resolver(child).get(Warrior);
        const singletonWarrior2 = Resolver(child).get(Warrior);
        expect(singletonWarrior1).to.equal(singletonWarrior2);
    });

    it("Should be able to override options to child containers", () => {
        @injectable()
        class Warrior { }

        const parent = new Container();
        const child = createChild(parent, {});
        child.bind(Warrior).toSelf().inSingletonScope();

        const singletonWarrior1 = Resolver(child).get(Warrior);
        const singletonWarrior2 = Resolver(child).get(Warrior);
        expect(singletonWarrior1).to.equal(singletonWarrior2);
    });

    it("Should be able check if a named binding is bound", () => {

        const zero = "Zero";
        const invalidDivisor = "InvalidDivisor";
        const validDivisor = "ValidDivisor";
        const container = new Container();

        expect(container.isBound(zero)).to.equal(false);
        container.bind<number>(zero).toConstantValue(0);
        expect(container.isBound(zero)).to.equal(true);

        container.unbindAll();
        expect(container.isBound(zero)).to.equal(false);
        container.bind<number>(zero).toConstantValue(0).whenTargetTagged(NAMED_TAG, invalidDivisor);
        expect(isBoundTagged(container, zero, NAMED_TAG, invalidDivisor)).to.equal(true);
        expect(isBoundTagged(container, zero, NAMED_TAG, validDivisor)).to.equal(false);

        container.bind<number>(zero).toConstantValue(1).whenTargetTagged(NAMED_TAG, validDivisor);
        expect(isBoundTagged(container, zero, NAMED_TAG, invalidDivisor)).to.equal(true);
        expect(isBoundTagged(container, zero, NAMED_TAG, validDivisor)).to.equal(true);

    });

    it("Should be able to check if a named binding is bound from parent container", () => {

        const zero = "Zero";
        const invalidDivisor = "InvalidDivisor";
        const validDivisor = "ValidDivisor";
        const container = new Container();
        const childContainer = createChild(container);
        const secondChildContainer = createChild(childContainer);

        container.bind<number>(zero).toConstantValue(0).whenTargetTagged(NAMED_TAG, invalidDivisor);
        expect(isBoundTagged(secondChildContainer, zero, NAMED_TAG, invalidDivisor)).to.equal(true);
        expect(isBoundTagged(secondChildContainer, zero, NAMED_TAG, validDivisor)).to.equal(false);

        container.bind<number>(zero).toConstantValue(1).whenTargetTagged(NAMED_TAG, validDivisor);
        expect(isBoundTagged(secondChildContainer, zero, NAMED_TAG, invalidDivisor)).to.equal(true);
        expect(isBoundTagged(secondChildContainer, zero, NAMED_TAG, validDivisor)).to.equal(true);

    });

    it("Should be able to get a tagged binding", () => {

        const zero = "Zero";
        const isValidDivisor = "IsValidDivisor";
        const container = new Container();

        container.bind<number>(zero).toConstantValue(0).whenTargetTagged(isValidDivisor, false);
        expect(Resolver(container).getTagged(zero, isValidDivisor, false)).to.equal(0);

        container.bind<number>(zero).toConstantValue(1).whenTargetTagged(isValidDivisor, true);
        expect(Resolver(container).getTagged(zero, isValidDivisor, false)).to.equal(0);
        expect(Resolver(container).getTagged(zero, isValidDivisor, true)).to.equal(1);

    });

    it("Should be able to get a tagged binding from parent container", () => {

        const zero = "Zero";
        const isValidDivisor = "IsValidDivisor";
        const container = new Container();
        const childContainer = createChild(container);
        const secondChildContainer = createChild(childContainer);

        container.bind<number>(zero).toConstantValue(0).whenTargetTagged(isValidDivisor, false);
        container.bind<number>(zero).toConstantValue(1).whenTargetTagged(isValidDivisor, true);
        expect(Resolver(secondChildContainer).getTagged(zero, isValidDivisor, false)).to.equal(0);
        expect(Resolver(secondChildContainer).getTagged(zero, isValidDivisor, true)).to.equal(1);

    });

    it("Should be able check if a tagged binding is bound", () => {

        const zero = "Zero";
        const isValidDivisor = "IsValidDivisor";
        const container = new Container();

        expect(container.isBound(zero)).to.equal(false);
        container.bind<number>(zero).toConstantValue(0);
        expect(container.isBound(zero)).to.equal(true);

        container.unbindAll();
        expect(container.isBound(zero)).to.equal(false);
        container.bind<number>(zero).toConstantValue(0).whenTargetTagged(isValidDivisor, false);
        expect(isBoundTagged(container, zero, isValidDivisor, false)).to.equal(true);
        expect(isBoundTagged(container, zero, isValidDivisor, true)).to.equal(false);

        container.bind<number>(zero).toConstantValue(1).whenTargetTagged(isValidDivisor, true);
        expect(isBoundTagged(container, zero, isValidDivisor, false)).to.equal(true);
        expect(isBoundTagged(container, zero, isValidDivisor, true)).to.equal(true);

    });

    it("Should be able to check if a tagged binding is bound from parent container", () => {

        const zero = "Zero";
        const isValidDivisor = "IsValidDivisor";
        const container = new Container();
        const childContainer = createChild(container);
        const secondChildContainer = createChild(childContainer);

        container.bind<number>(zero).toConstantValue(0).whenTargetTagged(isValidDivisor, false);
        expect(isBoundTagged(secondChildContainer, zero, isValidDivisor, false)).to.equal(true);
        expect(isBoundTagged(secondChildContainer, zero, isValidDivisor, true)).to.equal(false);

        container.bind<number>(zero).toConstantValue(1).whenTargetTagged(isValidDivisor, true);
        expect(isBoundTagged(secondChildContainer, zero, isValidDivisor, false)).to.equal(true);
        expect(isBoundTagged(secondChildContainer, zero, isValidDivisor, true)).to.equal(true);

    });

    it("Should be able to override a binding using rebind", () => {

        const TYPES = {
            someType: "someType"
        };

        const container = new Container();
        container.bind<number>(TYPES.someType).toConstantValue(1);
        container.bind<number>(TYPES.someType).toConstantValue(2);

        const values1 = Resolver(container).getAll(TYPES.someType);
        expect(values1[0]).to.eq(1);
        expect(values1[1]).to.eq(2);

        container.rebind<number>(TYPES.someType).toConstantValue(3);
        const values2 = Resolver(container).getAll(TYPES.someType);
        expect(values2[0]).to.eq(3);
        expect(values2[1]).to.eq(undefined);

    });

});
