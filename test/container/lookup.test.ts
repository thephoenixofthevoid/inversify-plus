import { expect } from "chai";
import { Clonable } from "../../dts/interfaces/interfaces";
import { MultiMap } from "../../src/Base/MultiMap";
import { Binding } from "../../src/Base/Binding";
import { Any } from "ts-toolbelt"

const NULL_ARGUMENT = "NULL argument";
const KEY_NOT_FOUND = "Key Not Found";

class ClonableValue<T> implements Clonable<ClonableValue<T>> {
  public readonly val: T;
  public constructor(val: T) {
    this.val = val;
  }
  public clone () {
    return new ClonableValue<T>(this.val);
  }
}

describe("Lookup", () => {

  const invalid: any = null;

  // BREAKING CHANGE
  // it("Should throw when invoking get, remove or hasKey with a null key", () => {
  //   const lookup = new Lookup<any>();
  //   expect(() => { lookup.get(invalid); }).to.throw(NULL_ARGUMENT);
  //   expect(() => { lookup.remove(invalid); }).to.throw(NULL_ARGUMENT);
  //   expect(() => { lookup.hasKey(invalid); }).to.throw(NULL_ARGUMENT);
  // });

  // it("Should throw when attempting to add a null key", () => {
  //   const lookup = new Lookup<any>();
  //   expect(() => { lookup.add(invalid, new ClonableValue<number>(1)); }).to.throw(NULL_ARGUMENT);
  // });

  // it("Should throw when attempting to add a null value", () => {
  //   const lookup = new Lookup<any>();
  //   expect(() => { lookup.add("TEST_KEY", null); }).to.throw(NULL_ARGUMENT);
  // });

  // it("Should be able to link multiple values to a string key", () => {
  //   const lookup = new Lookup<any>();
  //   const key = "TEST_KEY";
  //   lookup.add(key, new ClonableValue<number>(1));
  //   lookup.add(key, new ClonableValue<number>(2));
  //   const result = lookup.get(key);
  //   expect(result.length).to.eql(2);
  // });

  // it("Should be able to link multiple values a symbol key", () => {
  //   const lookup = new Lookup<any>();
  //   const key = Symbol.for("TEST_KEY");
  //   lookup.add(key, new ClonableValue<number>(1));
  //   lookup.add(key, new ClonableValue<number>(2));
  //   const result = lookup.get(key);
  //   expect(result.length).to.eql(2);
  // });

  // it("Should throws when key not found", () => {
  //   const lookup = new Lookup<any>();
  //   expect(() => { lookup.get("THIS_KEY_IS_NOT_AVAILABLE"); }).to.throw(KEY_NOT_FOUND);
  //   expect(() => { lookup.remove("THIS_KEY_IS_NOT_AVAILABLE"); }).to.throw(KEY_NOT_FOUND);
  // });

  // it("Should be clonable", () => {

  //   const lookup = new Lookup<Clonable<any>>();
  //   const key1 = Symbol.for("TEST_KEY");

  //   class Warrior {
  //     public kind: string;
  //     public constructor(kind: string) {
  //       this.kind = kind;
  //     }
  //     public clone() {
  //       return new Warrior(this.kind);
  //     }
  //   }

  //   lookup.add(key1, new Warrior("ninja"));
  //   lookup.add(key1, new Warrior("samurai"));

  //   const copy = lookup.clone();
  //   expect(copy.hasKey(key1)).to.eql(true);

  //   lookup.remove(key1);
  //   expect(copy.hasKey(key1)).to.eql(true);

  // });

  it("Should be able to remove a binding by a condition", () => {

    const moduleId1 = "moduleId1";
    const moduleId2 = "moduleId2";
    const warriorId = "Warrior";
    const weaponId = "Weapon";

    const getLookup = () => {

      interface Warrior {}

      class Ninja implements Warrior {}
      const ninjaBinding: any = new Binding(warriorId);
      ninjaBinding.value = Ninja;
      ninjaBinding.moduleId = moduleId1;

      class Samurai implements Warrior {}
      const samuraiBinding: any = new Binding(warriorId);
      samuraiBinding.value = Samurai;
      samuraiBinding.moduleId = moduleId2;

      interface Weapon {}

      class Shuriken implements Weapon {}
      const shurikenBinding: any = new Binding(weaponId);
      shurikenBinding.value = Shuriken;
      shurikenBinding.moduleId = moduleId1;

      class Katana implements Weapon {}
      const katanaBinding: any = new Binding(weaponId);
      katanaBinding.value = Katana;
      katanaBinding.moduleId = moduleId2;

      const lookup = new MultiMap<Any.Key, Binding<any>>();
      lookup.add(warriorId, ninjaBinding);
      lookup.add(warriorId, samuraiBinding);
      lookup.add(weaponId, shurikenBinding);
      lookup.add(weaponId, katanaBinding);

      return lookup;

    };

    const removeByModule = (expected: any) => (item: any): boolean =>
        item.moduleId === expected;

    const lookup1 = getLookup();
    expect(lookup1.has(warriorId)).to.eql(true);
    expect(lookup1.has(weaponId)).to.eql(true);
    expect(lookup1.get(warriorId).length).to.eql(2);
    expect(lookup1.get(weaponId).length).to.eql(2);

    const removeByModule1 = removeByModule(moduleId1);
    lookup1.removeByCondition(removeByModule1);
    expect(lookup1.has(warriorId)).to.eql(true);
    expect(lookup1.has(weaponId)).to.eql(true);
    expect(lookup1.get(warriorId).length).to.eql(1);
    expect(lookup1.get(weaponId).length).to.eql(1);

    const lookup2 = getLookup();
    expect(lookup2.has(warriorId)).to.eql(true);
    expect(lookup2.has(weaponId)).to.eql(true);
    expect(lookup2.get(warriorId).length).to.eql(2);
    expect(lookup2.get(weaponId).length).to.eql(2);

    const removeByModule2 = removeByModule(moduleId2);
    lookup2.removeByCondition(removeByModule1);
    lookup2.removeByCondition(removeByModule2);
    expect(lookup2.has(warriorId)).to.eql(false);
    expect(lookup2.has(weaponId)).to.eql(false);

  });

});
