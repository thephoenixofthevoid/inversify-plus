import { expect } from "chai";
import * as sinon from "sinon";
import { injectable } from "../../src/inversify";
import { Binding } from "../../src/Base/Binding";
import { NAMED_TAG } from "../../src/Constants";
import { Context, EntityRequest } from "../../src/Base/PlanElements";
import { getSyntaxes } from "../../src/API/BindingOnSyntax";

export const BindingInWhenOnSyntax = <T = any>(binding: Binding<T>) => {
    return getSyntaxes(binding).inWhenOnSyntax
}


describe("BindingInWhenOnSyntax", () => {

    let sandbox: sinon.SinonSandbox;

    beforeEach(() => {
        sandbox = sinon.createSandbox();
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("Should set its own properties correctly", () => {

        interface Ninja {}
        const ninjaIdentifier = "Ninja";

        const binding = new Binding<Ninja>(ninjaIdentifier);
        const bindingInWhenOnSyntax = BindingInWhenOnSyntax<Ninja>(binding);

        // cast to any to be able to access private props
        const _bindingInWhenOnSyntax: any = bindingInWhenOnSyntax;

        expect(_bindingInWhenOnSyntax._binding.serviceIdentifier).eql(ninjaIdentifier);

    });

    it("Should provide access to BindingInSyntax methods", () => {

        interface Ninja {}
        const ninjaIdentifier = "Ninja";

        const binding = new Binding<Ninja>(ninjaIdentifier);
        const bindingInWhenOnSyntax = BindingInWhenOnSyntax<Ninja>(binding);

        // cast to any to be able to access private props
        const _bindingInWhenOnSyntax: any = bindingInWhenOnSyntax;

        // stubs for BindingWhenSyntax methods
        const inSingletonScopeStub = sinon.stub(_bindingInWhenOnSyntax, "inSingletonScope").returns(null);
        const inTransientScopeStub = sinon.stub(_bindingInWhenOnSyntax, "inTransientScope").returns(null);

        // invoke BindingWhenOnSyntax methods
        bindingInWhenOnSyntax.inSingletonScope();
        bindingInWhenOnSyntax.inTransientScope();

        // assert invoked BindingWhenSyntax methods
        expect(inSingletonScopeStub.callCount).eql(1);
        expect(inTransientScopeStub.callCount).eql(1);

    });

    it("Should provide access to BindingWhenSyntax methods", () => {

        interface Army {}

        @injectable()
        class Army implements Army {}

        interface ZombieArmy {}

        @injectable()
        class ZombieArmy implements ZombieArmy {}

        interface Ninja {}
        const ninjaIdentifier = "Ninja";

        const binding = new Binding<Ninja>(ninjaIdentifier);
        const bindingInWhenOnSyntax = BindingInWhenOnSyntax<Ninja>(binding);

        // cast to any to be able to access private props
        const _bindingInWhenOnSyntax: any = bindingInWhenOnSyntax;

        // stubs for BindingWhenSyntax methods
        
         // DEPRECATED const whenTargetNamedStub = sinon.stub(_bindingInWhenOnSyntax, "whenTargetNamed").returns(null);
        // DEPRECATED const whenParentNamedStub = sinon.stub(_bindingInWhenOnSyntax, "whenParentNamed").returns(null);
        // DEPRECATED const whenNoAncestorNamedStub = sinon.stub(_bindingInWhenOnSyntax, "whenNoAncestorNamed").returns(null);
        // DEPRECATED const whenAnyAncestorNamedStub = sinon.stub(_bindingInWhenOnSyntax, "whenAnyAncestorNamed").returns(null);

        const whenStub = sinon.stub(_bindingInWhenOnSyntax, "when").returns(null);
        const whenTargetTaggedStub = sinon.stub(_bindingInWhenOnSyntax, "whenTargetTagged").returns(null);
        const whenInjectedIntoStub = sinon.stub(_bindingInWhenOnSyntax, "whenInjectedInto").returns(null);
        const whenParentTaggedStub = sinon.stub(_bindingInWhenOnSyntax, "whenParentTagged").returns(null);
        const whenAnyAncestorIsStub = sinon.stub(_bindingInWhenOnSyntax, "whenAnyAncestorIs").returns(null);
        const whenNoAncestorIsStub = sinon.stub(_bindingInWhenOnSyntax, "whenNoAncestorIs").returns(null);
        const whenNoAncestorTaggedStub = sinon.stub(_bindingInWhenOnSyntax, "whenNoAncestorTagged").returns(null);
        const whenAnyAncestorTaggedStub = sinon.stub(_bindingInWhenOnSyntax, "whenAnyAncestorTagged").returns(null);
        const whenAnyAncestorMatchesStub = sinon.stub(_bindingInWhenOnSyntax, "whenAnyAncestorMatches").returns(null);
        const whenNoAncestorMatchesStub = sinon.stub(_bindingInWhenOnSyntax, "whenNoAncestorMatches").returns(null);

        // invoke BindingWhenOnSyntax methods
        bindingInWhenOnSyntax.when((request: EntityRequest) => true);
        bindingInWhenOnSyntax.whenTargetTagged(NAMED_TAG, "test");
        bindingInWhenOnSyntax.whenTargetTagged("test", true);
        bindingInWhenOnSyntax.whenInjectedInto("army");
        bindingInWhenOnSyntax.whenInjectedInto(Army);
        bindingInWhenOnSyntax.whenParentTagged(NAMED_TAG, "test");
        bindingInWhenOnSyntax.whenParentTagged("test", true);
        bindingInWhenOnSyntax.whenAnyAncestorIs(Army);
        bindingInWhenOnSyntax.whenNoAncestorIs(ZombieArmy);
        bindingInWhenOnSyntax.whenAnyAncestorTagged(NAMED_TAG, "test");
        bindingInWhenOnSyntax.whenAnyAncestorTagged("test", true);
        bindingInWhenOnSyntax.whenNoAncestorTagged(NAMED_TAG, "test");
        bindingInWhenOnSyntax.whenNoAncestorTagged("test", true);
        bindingInWhenOnSyntax.whenAnyAncestorMatches((request: EntityRequest) => true);
        bindingInWhenOnSyntax.whenNoAncestorMatches((request: EntityRequest) => true);

        // assert invoked BindingWhenSyntax methods

        // expect(whenTargetNamedStub.callCount).eql(1);
        // expect(whenParentNamedStub.callCount).eql(1);
        // expect(whenAnyAncestorNamedStub.callCount).eql(1);
        // expect(whenNoAncestorNamedStub.callCount).eql(1);

        expect(whenStub.callCount).eql(1);
        expect(whenTargetTaggedStub.callCount).eql(2);
        expect(whenInjectedIntoStub.callCount).eql(2);
        expect(whenAnyAncestorIsStub.callCount).eql(1);
        expect(whenNoAncestorIsStub.callCount).eql(1);
        expect(whenParentTaggedStub.callCount).eql(2);
        expect(whenAnyAncestorTaggedStub.callCount).eql(2);
        expect(whenNoAncestorTaggedStub.callCount).eql(2);
        expect(whenAnyAncestorMatchesStub.callCount).eql(1);
        expect(whenNoAncestorMatchesStub.callCount).eql(1);

    });

    it("Should provide access to BindingOnSyntax methods", () => {

        interface Ninja {}
        const ninjaIdentifier = "Ninja";

        const binding = new Binding<Ninja>(ninjaIdentifier);
        const bindingInWhenOnSyntax = BindingInWhenOnSyntax<Ninja>(binding);

        // cast to any to be able to access private props
        const _bindingInWhenOnSyntax: any = bindingInWhenOnSyntax;

        // stubs for BindingWhenSyntax methods
        const onActivationStub = sinon.stub(_bindingInWhenOnSyntax, "onActivation").returns(null);

        // invoke BindingWhenOnSyntax methods
        bindingInWhenOnSyntax.onActivation((context: Context, ninja: Ninja) =>
            // DO NOTHING
            ninja);

        // assert invoked BindingWhenSyntax methods
        expect(onActivationStub.callCount).eql(1);

    });

});
