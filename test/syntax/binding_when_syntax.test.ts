import { expect } from "chai";
import { Container } from "../../src/inversify";
import { Binding } from "../../src/Base/Binding";
import { Context, EntityRequest } from "../../src/Base/PlanElements";
import { Target } from "../../src/Base/Target";
import { typeConstraint } from "../../src/inversify";
import { NAMED_TAG, MODE_TAG } from "../../src/Constants";
import { getSyntaxes } from "../../src/API/BindingOnSyntax";
import { InstanceResolvingStrategy } from "../../src/Strategies/ResolvingStrategy";

export const BindingInWhenOnSyntax = <T = any>(binding: Binding<T>) => {
    return getSyntaxes(binding).inWhenOnSyntax
}

describe("BindingWhenSyntax", () => {

    it("Should set its own properties correctly", () => {

        interface Ninja {}
        const ninjaIdentifier = "Ninja";

        const binding = new Binding<Ninja>(ninjaIdentifier);
        const bindingWhenSyntax = BindingInWhenOnSyntax<Ninja>(binding);

        // cast to any to be able to access private props
        const _bindingWhenSyntax: any = bindingWhenSyntax;

        expect(_bindingWhenSyntax._binding.serviceIdentifier).eql(ninjaIdentifier);

    });

    it.skip("Should be able to configure custom constraint of a binding", () => {

        interface Ninja {}
        const ninjaIdentifier = "Ninja";

        const binding = new Binding<Ninja>(ninjaIdentifier);
        const bindingWhenSyntax = BindingInWhenOnSyntax<Ninja>(binding);

        bindingWhenSyntax.when((theRequest: EntityRequest) =>
            theRequest.target.name === "ninja");

        const target = Target.for(ninjaIdentifier);
        target.set(MODE_TAG, "ConstructorArgument")
        const context = new Context(new Container());
        const request = new EntityRequest(null, context, binding, target);
        expect(binding.constraint && binding.constraint(request)).eql(true);

    });

    it("Should be able to constraint a binding to a named target", () => {

        interface Ninja {}
        const ninjaIdentifier = "Ninja";

        const binding = new Binding<Ninja>(ninjaIdentifier);
        const bindingWhenSyntax = BindingInWhenOnSyntax<Ninja>(binding);

        const named = "primary";

        bindingWhenSyntax.whenTargetTagged(NAMED_TAG, named);
        expect(binding.constraint).not.to.eql(null);

        const context = new Context(new Container());

        const target = Target.for(ninjaIdentifier);
        target.set(MODE_TAG, "ConstructorArgument")
        target.set(NAMED_TAG, named)
        const request = new EntityRequest(null, context, binding, target);
        expect(binding.constraint && binding.constraint(request)).eql(true);

        const target2 = Target.for(ninjaIdentifier);
        target.set(MODE_TAG, "ConstructorArgument")
        const request2 = new EntityRequest(null, context, binding, target2);
        expect(binding.constraint && binding.constraint(request2)).eql(false);

    });

    it("Should be able to constraint a binding to a tagged target", () => {

        interface Ninja {}
        const ninjaIdentifier = "Ninja";

        const binding = new Binding<Ninja>(ninjaIdentifier);
        const bindingWhenSyntax = BindingInWhenOnSyntax<Ninja>(binding);

        bindingWhenSyntax.whenTargetTagged("canSwim", true);
        expect(binding.constraint).not.to.eql(null);

        const context = new Context(new Container());

        const target = Target.for(ninjaIdentifier);
        target.set(MODE_TAG, "ConstructorArgument")
        target.set("canSwim", true)
        const request = new EntityRequest(null, context, binding, target);
        expect(binding.constraint && binding.constraint(request)).eql(true);

        const target2 = Target.for(ninjaIdentifier);
        target2.set(MODE_TAG, "ConstructorArgument")
        target2.set("canSwim", false)

        const request2 = new EntityRequest(null, context, binding, target2);
        expect(binding.constraint && binding.constraint(request2)).eql(false);

    });

    it("Should be able to constraint a binding to its parent", () => {

        interface Weapon {
            name: string;
        }

        interface JaponeseWarrior {
            katana: Weapon;
        }

        interface ChineseWarrior {
            shuriken: Weapon;
        }

        class Ninja implements ChineseWarrior {
            public shuriken: Weapon;
            public constructor(shuriken: Weapon) {
                this.shuriken = shuriken;
            }
        }

        class Samurai implements JaponeseWarrior {
            public katana: Weapon;
            public constructor(katana: Weapon) {
                this.katana = katana;
            }
        }

        const context = new Context(new Container());

        const samuraiBinding = new Binding<Samurai>("Samurai");
        samuraiBinding.type = InstanceResolvingStrategy;
        samuraiBinding.value = Samurai;
        const samuraiTarget = Target.for("Samurai");
        samuraiTarget.set(MODE_TAG, "Variable")
        const samuraiRequest = new EntityRequest(null, context, samuraiBinding, samuraiTarget);

        const ninjaBinding = new Binding<Ninja>("Ninja");
        ninjaBinding.type = InstanceResolvingStrategy;
        ninjaBinding.value = Ninja;
        const ninjaTarget = Target.for("Ninja");
        ninjaTarget.set(MODE_TAG, "Variable")
        const ninjaRequest = new EntityRequest(null, context, ninjaBinding, ninjaTarget);

        const katanaBinding = new Binding<Weapon>("Weapon");
        const katanaBindingWhenSyntax = BindingInWhenOnSyntax<Weapon>(katanaBinding);
        const katanaTarget = Target.for("Weapon");
        katanaTarget.set(MODE_TAG, "ConstructorArgument")
        const katanaRequest = new EntityRequest(samuraiRequest, samuraiRequest.context, katanaBinding, katanaTarget);

        const shurikenBinding = new Binding<Weapon>("Weapon");
        const shurikenBindingWhenSyntax = BindingInWhenOnSyntax<Weapon>(shurikenBinding);
        const shurikenTarget = Target.for("Weapon");
        shurikenTarget.set(MODE_TAG, "ConstructorArgument")
        const shurikenRequest = new EntityRequest(ninjaRequest, ninjaRequest.context, shurikenBinding, shurikenTarget);

        katanaBindingWhenSyntax.whenInjectedInto(Samurai);
        expect(katanaBinding.constraint && katanaBinding.constraint(katanaRequest)).eql(true);
        expect(katanaBinding.constraint && katanaBinding.constraint(shurikenRequest)).eql(false);

        katanaBindingWhenSyntax.whenInjectedInto(Ninja);
        expect(katanaBinding.constraint && katanaBinding.constraint(katanaRequest)).eql(false);
        expect(katanaBinding.constraint && katanaBinding.constraint(shurikenRequest)).eql(true);

        shurikenBindingWhenSyntax.whenInjectedInto(Samurai);
        expect(shurikenBinding.constraint && shurikenBinding.constraint(katanaRequest)).eql(true);
        expect(shurikenBinding.constraint && shurikenBinding.constraint(shurikenRequest)).eql(false);

        shurikenBindingWhenSyntax.whenInjectedInto(Ninja);
        expect(shurikenBinding.constraint && shurikenBinding.constraint(katanaRequest)).eql(false);
        expect(shurikenBinding.constraint && shurikenBinding.constraint(shurikenRequest)).eql(true);

        katanaBindingWhenSyntax.whenInjectedInto("Samurai");
        expect(katanaBinding.constraint && katanaBinding.constraint(katanaRequest)).eql(true);
        expect(katanaBinding.constraint && katanaBinding.constraint(shurikenRequest)).eql(false);

        katanaBindingWhenSyntax.whenInjectedInto("Ninja");
        expect(katanaBinding.constraint && katanaBinding.constraint(katanaRequest)).eql(false);
        expect(katanaBinding.constraint && katanaBinding.constraint(shurikenRequest)).eql(true);

        shurikenBindingWhenSyntax.whenInjectedInto("Samurai");
        expect(shurikenBinding.constraint && shurikenBinding.constraint(katanaRequest)).eql(true);
        expect(shurikenBinding.constraint && shurikenBinding.constraint(shurikenRequest)).eql(false);

        shurikenBindingWhenSyntax.whenInjectedInto("Ninja");
        expect(shurikenBinding.constraint && shurikenBinding.constraint(katanaRequest)).eql(false);
        expect(shurikenBinding.constraint && shurikenBinding.constraint(shurikenRequest)).eql(true);

    });

    it("Should be able to constraint a binding to a named parent", () => {

        interface Weapon {
            name: string;
        }

        interface JaponeseWarrior {
            katana: Weapon;
        }

        interface ChineseWarrior {
            shuriken: Weapon;
        }

        class Ninja implements ChineseWarrior {
            public shuriken: Weapon;
            public constructor(shuriken: Weapon) {
                this.shuriken = shuriken;
            }
        }

        class Samurai implements JaponeseWarrior {
            public katana: Weapon;
            public constructor(katana: Weapon) {
                this.katana = katana;
            }
        }

        const samuraiBinding = new Binding<Samurai>("Samurai");
        samuraiBinding.value = Samurai;
        samuraiBinding.type = InstanceResolvingStrategy;

        const context = new Context(new Container());

        const samuraiTarget = Target.for("Samurai");
        samuraiTarget.set(MODE_TAG, "ConstructorArgument")
        samuraiTarget.set(NAMED_TAG, "japonese")


        const samuraiRequest = new EntityRequest(null, context, samuraiBinding, samuraiTarget);
        const ninjaBinding = new Binding<Ninja>("Ninja");

        ninjaBinding.value = Ninja;
        ninjaBinding.type = InstanceResolvingStrategy;

        const ninjaTarget = Target.for("Ninja");
        ninjaTarget.set(MODE_TAG, "ConstructorArgument")
        ninjaTarget.set(NAMED_TAG, "chinese")
        const ninjaRequest = new EntityRequest(null, context, ninjaBinding, ninjaTarget);

        const katanaBinding = new Binding<Weapon>("Weapon");
        const katanaBindingWhenSyntax = BindingInWhenOnSyntax<Weapon>(katanaBinding);
        const katanaTarget = Target.for("Weapon");
        katanaTarget.set(MODE_TAG, "ConstructorArgument")
        const katanaRequest = new EntityRequest(samuraiRequest, samuraiRequest.context, katanaBinding, katanaTarget);

        const shurikenBinding = new Binding<Weapon>("Weapon");
        const shurikenBindingWhenSyntax = BindingInWhenOnSyntax<Weapon>(shurikenBinding);
        const shurikenTarget = Target.for("Weapon");
        shurikenTarget.set(MODE_TAG, "ConstructorArgument")
        const shurikenRequest = new EntityRequest(ninjaRequest, ninjaRequest.context, shurikenBinding, shurikenTarget);

        katanaBindingWhenSyntax.whenParentTagged(NAMED_TAG, "chinese");
        shurikenBindingWhenSyntax.whenParentTagged(NAMED_TAG, "chinese");
        expect(katanaBinding.constraint && katanaBinding.constraint(katanaRequest)).eql(false);
        expect(shurikenBinding.constraint && shurikenBinding.constraint(shurikenRequest)).eql(true);

        katanaBindingWhenSyntax.whenParentTagged(NAMED_TAG, "japonese");
        shurikenBindingWhenSyntax.whenParentTagged(NAMED_TAG, "japonese");
        expect(katanaBinding.constraint && katanaBinding.constraint(katanaRequest)).eql(true);
        expect(shurikenBinding.constraint && shurikenBinding.constraint(shurikenRequest)).eql(false);

    });

    it("Should be able to constraint a binding to a tagged parent", () => {

        interface Weapon {
            name: string;
        }

        interface JaponeseWarrior {
            katana: Weapon;
        }

        interface ChineseWarrior {
            shuriken: Weapon;
        }

        class Ninja implements ChineseWarrior {
            public shuriken: Weapon;
            public constructor(shuriken: Weapon) {
                this.shuriken = shuriken;
            }
        }

        class Samurai implements JaponeseWarrior {
            public katana: Weapon;
            public constructor(katana: Weapon) {
                this.katana = katana;
            }
        }

        const context = new Context(new Container());

        const samuraiBinding = new Binding<Samurai>("Samurai");
        samuraiBinding.value = Samurai;
        samuraiBinding.type = InstanceResolvingStrategy;


        const samuraiTarget = Target.for("Samurai");
        samuraiTarget.set(MODE_TAG, "ConstructorArgument")
        samuraiTarget.set("sneaky", false)
        const samuraiRequest = new EntityRequest(null, context, samuraiBinding, samuraiTarget);

        const ninjaBinding = new Binding<Ninja>("Ninja");
        ninjaBinding.value = Ninja;
        ninjaBinding.type = InstanceResolvingStrategy;

        const ninjaTarget = Target.for("Ninja");
        ninjaTarget.set(MODE_TAG, "ConstructorArgument")
        ninjaTarget.set("sneaky", true)

        const ninjaRequest = new EntityRequest(null, context, ninjaBinding, ninjaTarget);
        const katanaBinding = new Binding<Weapon>("Weapon");
        const katanaBindingWhenSyntax = BindingInWhenOnSyntax<Weapon>(katanaBinding);
        const katanaTarget = Target.for("Weapon");
        katanaTarget.set(MODE_TAG, "ConstructorArgument")
        const katanaRequest = new EntityRequest(samuraiRequest, samuraiRequest.context, katanaBinding, katanaTarget);

        const shurikenBinding = new Binding<Weapon>("Weapon");
        const shurikenBindingWhenSyntax = BindingInWhenOnSyntax<Weapon>(shurikenBinding);
        const shurikenTarget = Target.for("Weapon");
        shurikenTarget.set(MODE_TAG, "ConstructorArgument")
        const shurikenRequest = new EntityRequest(ninjaRequest, ninjaRequest.context, shurikenBinding, shurikenTarget);

        katanaBindingWhenSyntax.whenParentTagged("sneaky", true);
        shurikenBindingWhenSyntax.whenParentTagged("sneaky", true);
        expect(katanaBinding.constraint && katanaBinding.constraint(katanaRequest)).eql(false);
        expect(shurikenBinding.constraint && shurikenBinding.constraint(shurikenRequest)).eql(true);

        katanaBindingWhenSyntax.whenParentTagged("sneaky", false);
        shurikenBindingWhenSyntax.whenParentTagged("sneaky", false);
        expect(katanaBinding.constraint && katanaBinding.constraint(katanaRequest)).eql(true);
        expect(shurikenBinding.constraint && shurikenBinding.constraint(shurikenRequest)).eql(false);

    });

    describe("BindingWhenSyntax.when*Ancestor*()", () => {

        interface Material {
            name: string;
        }

        interface Weapon {
            name: string;
            material: Material;
        }

        class Katana implements Weapon {
            public name = "Katana";
            public material: Material;
            public constructor(material: Material) {
                this.material = material;
            }
        }

        class Shuriken implements Weapon {
            public name = "Shuriken";
            public material: Material;
            public constructor(material: Material) {
                this.material = material;
            }
        }

        interface Samurai {
            katana: Weapon;
        }

        interface Ninja {
            shuriken: Weapon;
        }

        class NinjaMaster implements Ninja {
            public shuriken: Weapon;
            public constructor(shuriken: Weapon) {
                this.shuriken = shuriken;
            }
        }

        class SamuraiMaster implements Samurai {
            public katana: Weapon;
            public constructor(katana: Weapon) {
                this.katana = katana;
            }
        }

        class NinjaStudent implements Ninja {
            public shuriken: Weapon;
            public constructor(shuriken: Weapon) {
                this.shuriken = shuriken;
            }
        }

        class SamuraiStudent implements Samurai {
            public katana: Weapon;
            public constructor(katana: Weapon) {
                this.katana = katana;
            }
        }

        const context = new Context(new Container());

        // Samurai
        const samuraiMasterBinding = new Binding<Samurai>("Samurai");
        samuraiMasterBinding.value = SamuraiMaster;
        samuraiMasterBinding.type = InstanceResolvingStrategy;

        const samuraiStudentBinding = new Binding<Samurai>("Samurai");
        samuraiStudentBinding.value = SamuraiStudent;
        samuraiStudentBinding.type = InstanceResolvingStrategy;

        const samuraiTarget = Target.for("Samurai");
        samuraiTarget.set(MODE_TAG, "ConstructorArgument")
        samuraiTarget.set("sneaky", false)

        const samuraiMasterRequest = new EntityRequest(null, context, samuraiMasterBinding, samuraiTarget);
        const samuraiStudentRequest = new EntityRequest(null, context, samuraiStudentBinding, samuraiTarget);

        // Ninja
        const ninjaMasterBinding = new Binding<Ninja>("Ninja");
        ninjaMasterBinding.value = NinjaMaster;
        ninjaMasterBinding.type = InstanceResolvingStrategy;

        const ninjaStudentBinding = new Binding<Ninja>("Ninja");
        ninjaStudentBinding.value = NinjaStudent;
        ninjaStudentBinding.type = InstanceResolvingStrategy;

        const ninjaTarget = Target.for("Ninja");
        ninjaTarget.set(MODE_TAG, "ConstructorArgument")
        ninjaTarget.set("sneaky", true)

        const ninjaMasterRequest = new EntityRequest(null, context, ninjaMasterBinding, ninjaTarget);
        const ninjaStudentRequest = new EntityRequest(null, context, ninjaStudentBinding, ninjaTarget);

        // Katana
        const katanaBinding = new Binding<Weapon>("Weapon");
        katanaBinding.type = InstanceResolvingStrategy;
        katanaBinding.value = Katana;
        const katanaBindingWhenSyntax = BindingInWhenOnSyntax<Weapon>(katanaBinding);
        const katanaTarget = Target.for("Weapon");
        katanaTarget.set(MODE_TAG, "ConstructorArgument")
        const ironKatanaRequest = new EntityRequest(samuraiMasterRequest, samuraiMasterRequest.context, katanaBinding, katanaTarget);
        const woodKatanaRequest = new EntityRequest(samuraiStudentRequest, samuraiStudentRequest.context, katanaBinding, katanaTarget);

        // Shuriken
        const shurikenBinding = new Binding<Weapon>("Weapon");
        shurikenBinding.value = Shuriken;
        shurikenBinding.type = InstanceResolvingStrategy;
        const shurikenBindingWhenSyntax = BindingInWhenOnSyntax<Weapon>(shurikenBinding);
        const shurikenTarget = Target.for("Weapon");
        shurikenTarget.set(MODE_TAG, "ConstructorArgument")
        const ironShurikenRequest = new EntityRequest(ninjaMasterRequest, ninjaMasterRequest.context, shurikenBinding, shurikenTarget);
        const woodShurikenRequest = new EntityRequest(ninjaStudentRequest, ninjaStudentRequest.context, shurikenBinding, shurikenTarget);

        it("Should be able to apply a type constraint to some of its ancestors", () => {

            shurikenBindingWhenSyntax.whenAnyAncestorIs(NinjaMaster);
            expect(shurikenBinding.constraint && shurikenBinding.constraint(woodShurikenRequest)).eql(false);
            expect(shurikenBinding.constraint && shurikenBinding.constraint(ironShurikenRequest)).eql(true);

            shurikenBindingWhenSyntax.whenAnyAncestorIs(NinjaStudent);
            expect(shurikenBinding.constraint && shurikenBinding.constraint(woodShurikenRequest)).eql(true);
            expect(shurikenBinding.constraint && shurikenBinding.constraint(ironShurikenRequest)).eql(false);

            katanaBindingWhenSyntax.whenAnyAncestorIs(SamuraiMaster);
            expect(katanaBinding.constraint && katanaBinding.constraint(woodKatanaRequest)).eql(false);
            expect(katanaBinding.constraint && katanaBinding.constraint(ironKatanaRequest)).eql(true);

            katanaBindingWhenSyntax.whenAnyAncestorIs(SamuraiStudent);
            expect(katanaBinding.constraint && katanaBinding.constraint(woodKatanaRequest)).eql(true);
            expect(katanaBinding.constraint && katanaBinding.constraint(ironKatanaRequest)).eql(false);

        });

        it("Should be able to apply a type constraint to none of its ancestors", () => {

            shurikenBindingWhenSyntax.whenNoAncestorIs(NinjaMaster);
            expect(shurikenBinding.constraint && shurikenBinding.constraint(woodShurikenRequest)).eql(true);
            expect(shurikenBinding.constraint && shurikenBinding.constraint(ironShurikenRequest)).eql(false);

            shurikenBindingWhenSyntax.whenNoAncestorIs(NinjaStudent);
            expect(shurikenBinding.constraint && shurikenBinding.constraint(woodShurikenRequest)).eql(false);
            expect(shurikenBinding.constraint && shurikenBinding.constraint(ironShurikenRequest)).eql(true);

            katanaBindingWhenSyntax.whenNoAncestorIs(SamuraiMaster);
            expect(katanaBinding.constraint && katanaBinding.constraint(woodKatanaRequest)).eql(true);
            expect(katanaBinding.constraint && katanaBinding.constraint(ironKatanaRequest)).eql(false);

            katanaBindingWhenSyntax.whenNoAncestorIs(SamuraiStudent);
            expect(katanaBinding.constraint && katanaBinding.constraint(woodKatanaRequest)).eql(false);
            expect(katanaBinding.constraint && katanaBinding.constraint(ironKatanaRequest)).eql(true);

        });

        it("Should be able to apply a named constraint to some of its ancestors", () => {

            shurikenBindingWhenSyntax.whenAnyAncestorTagged(NAMED_TAG, "chinese");
            expect(shurikenBinding.constraint && shurikenBinding.constraint(woodShurikenRequest)).eql(false);
            expect(shurikenBinding.constraint && shurikenBinding.constraint(ironShurikenRequest)).eql(false);

            shurikenBindingWhenSyntax.whenAnyAncestorTagged(NAMED_TAG, "chinese");
            expect(shurikenBinding.constraint && shurikenBinding.constraint(woodShurikenRequest)).eql(false);
            expect(shurikenBinding.constraint && shurikenBinding.constraint(ironShurikenRequest)).eql(false);

            katanaBindingWhenSyntax.whenAnyAncestorTagged(NAMED_TAG, "japonese");
            expect(katanaBinding.constraint && katanaBinding.constraint(woodKatanaRequest)).eql(false);
            expect(katanaBinding.constraint && katanaBinding.constraint(ironKatanaRequest)).eql(false);

            katanaBindingWhenSyntax.whenAnyAncestorTagged(NAMED_TAG, "japonese");
            expect(katanaBinding.constraint && katanaBinding.constraint(woodKatanaRequest)).eql(false);
            expect(katanaBinding.constraint && katanaBinding.constraint(ironKatanaRequest)).eql(false);

        });

        it("Should be able to apply a named constraint to none of its ancestors", () => {

            shurikenBindingWhenSyntax.whenNoAncestorTagged(NAMED_TAG, "chinese");
            expect(shurikenBinding.constraint && shurikenBinding.constraint(woodShurikenRequest)).eql(true);
            expect(shurikenBinding.constraint && shurikenBinding.constraint(ironShurikenRequest)).eql(true);

            shurikenBindingWhenSyntax.whenNoAncestorTagged(NAMED_TAG, "chinese");
            expect(shurikenBinding.constraint && shurikenBinding.constraint(woodShurikenRequest)).eql(true);
            expect(shurikenBinding.constraint && shurikenBinding.constraint(ironShurikenRequest)).eql(true);

            katanaBindingWhenSyntax.whenNoAncestorTagged(NAMED_TAG, "japonese");
            expect(katanaBinding.constraint && katanaBinding.constraint(woodKatanaRequest)).eql(true);
            expect(katanaBinding.constraint && katanaBinding.constraint(ironKatanaRequest)).eql(true);

            katanaBindingWhenSyntax.whenNoAncestorTagged(NAMED_TAG, "japonese");
            expect(katanaBinding.constraint && katanaBinding.constraint(woodKatanaRequest)).eql(true);
            expect(katanaBinding.constraint && katanaBinding.constraint(ironKatanaRequest)).eql(true);

        });

        it("Should be able to apply a tagged constraint to some of its ancestors", () => {

            shurikenBindingWhenSyntax.whenAnyAncestorTagged("sneaky", true);
            expect(shurikenBinding.constraint && shurikenBinding.constraint(woodShurikenRequest)).eql(true);
            expect(shurikenBinding.constraint && shurikenBinding.constraint(ironShurikenRequest)).eql(true);

            shurikenBindingWhenSyntax.whenAnyAncestorTagged("sneaky", false);
            expect(shurikenBinding.constraint && shurikenBinding.constraint(woodShurikenRequest)).eql(false);
            expect(shurikenBinding.constraint && shurikenBinding.constraint(ironShurikenRequest)).eql(false);

            katanaBindingWhenSyntax.whenAnyAncestorTagged("sneaky", true);
            expect(katanaBinding.constraint && katanaBinding.constraint(woodKatanaRequest)).eql(false);
            expect(katanaBinding.constraint && katanaBinding.constraint(ironKatanaRequest)).eql(false);

            katanaBindingWhenSyntax.whenAnyAncestorTagged("sneaky", false);
            expect(katanaBinding.constraint && katanaBinding.constraint(woodKatanaRequest)).eql(true);
            expect(katanaBinding.constraint && katanaBinding.constraint(ironKatanaRequest)).eql(true);

        });

        it("Should be able to apply a tagged constraint to none of its ancestors", () => {

            shurikenBindingWhenSyntax.whenNoAncestorTagged("sneaky", true);
            expect(shurikenBinding.constraint && shurikenBinding.constraint(woodShurikenRequest)).eql(false);
            expect(shurikenBinding.constraint && shurikenBinding.constraint(ironShurikenRequest)).eql(false);

            shurikenBindingWhenSyntax.whenNoAncestorTagged("sneaky", false);
            expect(shurikenBinding.constraint && shurikenBinding.constraint(woodShurikenRequest)).eql(true);
            expect(shurikenBinding.constraint && shurikenBinding.constraint(ironShurikenRequest)).eql(true);

            katanaBindingWhenSyntax.whenNoAncestorTagged("sneaky", true);
            expect(katanaBinding.constraint && katanaBinding.constraint(woodKatanaRequest)).eql(true);
            expect(katanaBinding.constraint && katanaBinding.constraint(ironKatanaRequest)).eql(true);

            katanaBindingWhenSyntax.whenNoAncestorTagged("sneaky", false);
            expect(katanaBinding.constraint && katanaBinding.constraint(woodKatanaRequest)).eql(false);
            expect(katanaBinding.constraint && katanaBinding.constraint(ironKatanaRequest)).eql(false);

        });

        it("Should be able to apply a custom constraint to some of its ancestors", () => {

            const anyAncestorIsNinjaMasterConstraint = typeConstraint(NinjaMaster);
            const anyAncestorIsNinjaStudentConstraint = typeConstraint(NinjaStudent);

            shurikenBindingWhenSyntax.whenAnyAncestorMatches(anyAncestorIsNinjaMasterConstraint);
            expect(shurikenBinding.constraint && shurikenBinding.constraint(woodShurikenRequest)).eql(false);
            expect(shurikenBinding.constraint && shurikenBinding.constraint(ironShurikenRequest)).eql(true);

            shurikenBindingWhenSyntax.whenAnyAncestorMatches(anyAncestorIsNinjaStudentConstraint);
            expect(shurikenBinding.constraint && shurikenBinding.constraint(woodShurikenRequest)).eql(true);
            expect(shurikenBinding.constraint && shurikenBinding.constraint(ironShurikenRequest)).eql(false);

            const anyAncestorIsSamuraiMasterConstraint = typeConstraint(SamuraiMaster);
            const anyAncestorIsSamuraiStudentConstraint = typeConstraint(SamuraiStudent);

            katanaBindingWhenSyntax.whenAnyAncestorMatches(anyAncestorIsSamuraiMasterConstraint);
            expect(katanaBinding.constraint && katanaBinding.constraint(woodKatanaRequest)).eql(false);
            expect(katanaBinding.constraint && katanaBinding.constraint(ironKatanaRequest)).eql(true);

            katanaBindingWhenSyntax.whenAnyAncestorMatches(anyAncestorIsSamuraiStudentConstraint);
            expect(katanaBinding.constraint && katanaBinding.constraint(woodKatanaRequest)).eql(true);
            expect(katanaBinding.constraint && katanaBinding.constraint(ironKatanaRequest)).eql(false);

        });

        it("Should be able to apply a custom constraint to none of its ancestors", () => {

            const anyAncestorIsNinjaMasterConstraint = typeConstraint(NinjaMaster);
            const anyAncestorIsNinjaStudentConstraint = typeConstraint(NinjaStudent);

            shurikenBindingWhenSyntax.whenNoAncestorMatches(anyAncestorIsNinjaMasterConstraint);
            expect(shurikenBinding.constraint && shurikenBinding.constraint(woodShurikenRequest)).eql(true);
            expect(shurikenBinding.constraint && shurikenBinding.constraint(ironShurikenRequest)).eql(false);

            shurikenBindingWhenSyntax.whenNoAncestorMatches(anyAncestorIsNinjaStudentConstraint);
            expect(shurikenBinding.constraint && shurikenBinding.constraint(woodShurikenRequest)).eql(false);
            expect(shurikenBinding.constraint && shurikenBinding.constraint(ironShurikenRequest)).eql(true);

            const anyAncestorIsSamuraiMasterConstraint = typeConstraint(SamuraiMaster);
            const anyAncestorIsSamuraiStudentConstraint = typeConstraint(SamuraiStudent);

            katanaBindingWhenSyntax.whenNoAncestorMatches(anyAncestorIsSamuraiMasterConstraint);
            expect(katanaBinding.constraint && katanaBinding.constraint(woodKatanaRequest)).eql(true);
            expect(katanaBinding.constraint && katanaBinding.constraint(ironKatanaRequest)).eql(false);

            katanaBindingWhenSyntax.whenNoAncestorMatches(anyAncestorIsSamuraiStudentConstraint);
            expect(katanaBinding.constraint && katanaBinding.constraint(woodKatanaRequest)).eql(false);
            expect(katanaBinding.constraint && katanaBinding.constraint(ironKatanaRequest)).eql(true);
        });

    });

});
