import { expect } from "chai";
import { injectable } from "../../src/inversify";
import { Binding } from "../../src/Base/Binding";
import { Context } from "../../src/Base/PlanElements";
import { Resolver } from "../../src/API/Resolver";
import { getSyntaxes } from "../../src/API/BindingOnSyntax";
import { ConstantValueResolvingStrategy, DynamicValueResolvingStrategy, InstanceResolvingStrategy, InvalidResolvingStrategy } from "../../src/Strategies/ResolvingStrategy";

const BindingToSyntax = <T = any>(B: Binding<T>) => {
    return getSyntaxes(B).toSyntax
};


const INVALID_FUNCTION_BINDING = "Value provided to function binding must be a function!";

describe("BindingToSyntax", () => {

    it("Should set its own properties correctly", () => {

        interface Ninja {}
        const ninjaIdentifier = "Ninja";

        const binding = new Binding<Ninja>(ninjaIdentifier);
        const bindingToSyntax = BindingToSyntax<Ninja>(binding);

        // cast to any to be able to access private props
        const _bindingToSyntax: any = bindingToSyntax;

        expect(_bindingToSyntax._binding.serviceIdentifier).eql(ninjaIdentifier);

    });

    it("Should be able to configure the type of a binding", () => {

        interface Ninja {}

        @injectable()
        class Ninja implements Ninja {}
        const ninjaIdentifier = "Ninja";

        const binding = new Binding<Ninja>(ninjaIdentifier);
        // let bindingWithClassAsId = new Binding<Ninja>(Ninja);
        const bindingToSyntax = BindingToSyntax<Ninja>(binding);

        expect(binding.type).eql(InvalidResolvingStrategy);

        bindingToSyntax.to(Ninja);
        expect(binding.type).eql(InstanceResolvingStrategy);
        expect(binding.value).not.to.eql(null);

//        (bindingToSyntax as any)._binding = bindingWithClassAsId;
//        bindingToSyntax.toSelf();
//        expect(binding.type).eql("Instance");
//        expect(binding.implementationType).not.to.eql(null);

        (bindingToSyntax as any)._binding = binding;
        bindingToSyntax.toConstantValue(new Ninja());
        expect(binding.type).eql(ConstantValueResolvingStrategy);
        expect(binding.value instanceof Ninja).eql(true);

        bindingToSyntax.toDynamicValue((context: Context) => new Ninja());
        expect(binding.type).eql(DynamicValueResolvingStrategy);
        expect(typeof binding.value).eql("function");

        const dynamicValueFactory: any = binding.value;
        expect(dynamicValueFactory(null) instanceof Ninja).eql(true);

        // bindingToSyntax.toConstructor(Ninja);
        // expect(binding.type).eql("Constructor");
        // expect(binding.value).not.to.eql(null);

        bindingToSyntax.toFactory((context: Context) =>
            () =>
                new Ninja());

        //expect(binding.type).eql("Factory");
        expect(binding.value).not.to.eql(null);

        const f = () => "test";
        bindingToSyntax.toConstantValue(f);
        expect(binding.type).eql(ConstantValueResolvingStrategy);
        expect(binding.value === f).eql(true);

        bindingToSyntax.toFactory(context => Resolver(context.container).get(ninjaIdentifier));

        //expect(binding.type).eql("Factory");
        expect(binding.value).not.to.eql(null);

        bindingToSyntax.toProvider((context: Context) =>
            () =>
                new Promise<Ninja>((resolve) => {
                    resolve(new Ninja());
                }));

        //expect(binding.type).eql("Provider");
        expect(binding.value).not.to.eql(null);

    });

});
