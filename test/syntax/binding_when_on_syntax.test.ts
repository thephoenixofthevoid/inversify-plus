import { expect } from "chai";
import * as sinon from "sinon";
import { injectable } from "../../src/inversify";
import { Binding } from "../../src/Base/Binding";
import { Context, EntityRequest } from "../../src/Base/PlanElements";
import { NAMED_TAG } from "../../src/Constants";
import { getSyntaxes } from "../../src/API/BindingOnSyntax";

export const BindingInWhenOnSyntax = <T = any>(binding: Binding<T>) => {
    return getSyntaxes(binding).inWhenOnSyntax
}


describe("BindingWhenOnSyntax", () => {

    let sandbox: sinon.SinonSandbox;

    beforeEach(() => {
        sandbox = sinon.createSandbox();
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("Should set its own properties correctly", () => {

        interface Ninja {}
        const ninjaIdentifier = "Ninja";

        const binding = new Binding<Ninja>(ninjaIdentifier);
        const bindingWhenOnSyntax = BindingInWhenOnSyntax<Ninja>(binding);

        // cast to any to be able to access private props
        const _bindingWhenOnSyntax: any = bindingWhenOnSyntax;

        expect(_bindingWhenOnSyntax._binding.serviceIdentifier).eql(ninjaIdentifier);

    });

    it("Should provide access to BindingWhenSyntax methods", () => {

        interface Army {}

        @injectable()
        class Army implements Army {}

        interface ZombieArmy {}

        @injectable()
        class ZombieArmy implements ZombieArmy {}

        interface Ninja {}
        const ninjaIdentifier = "Ninja";

        const binding = new Binding<Ninja>(ninjaIdentifier);
        const bindingWhenOnSyntax: any  = BindingInWhenOnSyntax<Ninja>(binding);

        // stubs for BindingWhenSyntax methods

        // const whenTargetNamedStub = sinon.stub(bindingWhenOnSyntax, "whenTargetNamed").returns(null);
        // const whenParentNamedStub = sinon.stub(bindingWhenOnSyntax, "whenParentNamed").returns(null);
        // const whenAnyAncestorNamedStub = sinon.stub(bindingWhenOnSyntax, "whenAnyAncestorNamed").returns(null);
        // const whenNoAncestorNamedStub = sinon.stub(bindingWhenOnSyntax, "whenNoAncestorNamed").returns(null);


        const whenStub = sinon.stub(bindingWhenOnSyntax, "when").returns(null);
        const whenTargetTaggedStub = sinon.stub(bindingWhenOnSyntax, "whenTargetTagged").returns(null);
        const whenInjectedIntoStub = sinon.stub(bindingWhenOnSyntax, "whenInjectedInto").returns(null);
        const whenParentTaggedStub = sinon.stub(bindingWhenOnSyntax, "whenParentTagged").returns(null);
        const whenAnyAncestorIsStub = sinon.stub(bindingWhenOnSyntax, "whenAnyAncestorIs").returns(null);
        const whenNoAncestorIsStub = sinon.stub(bindingWhenOnSyntax, "whenNoAncestorIs").returns(null);
        const whenNoAncestorTaggedStub = sinon.stub(bindingWhenOnSyntax, "whenNoAncestorTagged").returns(null);
        const whenAnyAncestorTaggedStub = sinon.stub(bindingWhenOnSyntax, "whenAnyAncestorTagged").returns(null);
        const whenAnyAncestorMatchesStub = sinon.stub(bindingWhenOnSyntax, "whenAnyAncestorMatches").returns(null);
        const whenNoAncestorMatchesStub = sinon.stub(bindingWhenOnSyntax, "whenNoAncestorMatches").returns(null);

        // invoke BindingWhenOnSyntax methods
        bindingWhenOnSyntax.when((request: EntityRequest) => true);
        bindingWhenOnSyntax.whenTargetTagged(NAMED_TAG, "test");
        bindingWhenOnSyntax.whenTargetTagged("test", true);
        bindingWhenOnSyntax.whenInjectedInto("army");
        bindingWhenOnSyntax.whenInjectedInto(Army);
        bindingWhenOnSyntax.whenParentTagged(NAMED_TAG, "test");
        bindingWhenOnSyntax.whenParentTagged("test", true);
        bindingWhenOnSyntax.whenAnyAncestorIs(Army);
        bindingWhenOnSyntax.whenNoAncestorIs(ZombieArmy);
        bindingWhenOnSyntax.whenAnyAncestorTagged(NAMED_TAG, "test");
        bindingWhenOnSyntax.whenAnyAncestorTagged("test", true);
        bindingWhenOnSyntax.whenNoAncestorTagged(NAMED_TAG, "test");
        bindingWhenOnSyntax.whenNoAncestorTagged("test", true);
        bindingWhenOnSyntax.whenAnyAncestorMatches((request: EntityRequest) => true);
        bindingWhenOnSyntax.whenNoAncestorMatches((request: EntityRequest) => true);

        // assert invoked BindingWhenSyntax methods
        // expect(whenTargetNamedStub.callCount).eql(1);
        // expect(whenParentNamedStub.callCount).eql(1);
        // expect(whenAnyAncestorNamedStub.callCount).eql(1);
        // expect(whenNoAncestorNamedStub.callCount).eql(1);

        expect(whenStub.callCount).eql(1);
        expect(whenTargetTaggedStub.callCount).eql(2);
        expect(whenInjectedIntoStub.callCount).eql(2);
        expect(whenParentTaggedStub.callCount).eql(2);
        expect(whenAnyAncestorIsStub.callCount).eql(1);
        expect(whenNoAncestorIsStub.callCount).eql(1);
        expect(whenAnyAncestorTaggedStub.callCount).eql(2);
        expect(whenNoAncestorTaggedStub.callCount).eql(2);
        expect(whenAnyAncestorMatchesStub.callCount).eql(1);
        expect(whenNoAncestorMatchesStub.callCount).eql(1);

    });

    it("Should provide access to BindingOnSyntax methods", () => {

        interface Ninja {}
        const ninjaIdentifier = "Ninja";

        const binding = new Binding<Ninja>(ninjaIdentifier);
        const bindingWhenOnSyntax = BindingInWhenOnSyntax<Ninja>(binding);

        // cast to any to be able to access private props
        const _bindingWhenOnSyntax: any = bindingWhenOnSyntax;

        // stubs for BindingWhenSyntax methods
        const onActivationStub = sinon.stub(_bindingWhenOnSyntax, "onActivation").returns(null);

        // invoke BindingWhenOnSyntax methods
        bindingWhenOnSyntax.onActivation((context: Context, ninja: Ninja) =>
            // DO NOTHING
            ninja);

        // assert invoked BindingWhenSyntax methods
        expect(onActivationStub.callCount).eql(1);

    });

});
