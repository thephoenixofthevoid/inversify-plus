import { expect } from "chai";
import { Binding } from "../../src/Base/Binding";
import { getSyntaxes } from "../../src/API/BindingOnSyntax";
import { SingletonScoped, TransientScoped } from "../../src/Strategies/CachingStrategy";

export const BindingInWhenOnSyntax = <T = any>(binding: Binding<T>) => {
    return getSyntaxes(binding).inWhenOnSyntax
}

describe("BindingInSyntax", () => {

    it("Should set its own properties correctly", () => {

        interface Ninja {}
        const ninjaIdentifier = "Ninja";

        const binding = new Binding<Ninja>(ninjaIdentifier);
        const bindingInSyntax = BindingInWhenOnSyntax<Ninja>(binding);

        // cast to any to be able to access private props
        const _bindingInSyntax: any = bindingInSyntax;

        expect(_bindingInSyntax._binding.serviceIdentifier).eql(ninjaIdentifier);

    });

    it("Should be able to configure the scope of a binding", () => {

        interface Ninja {}
        const ninjaIdentifier = "Ninja";

        const binding = new Binding<Ninja>(ninjaIdentifier);
        const bindingInSyntax = BindingInWhenOnSyntax<Ninja>(binding);

        // default scope is transient
        expect(binding.scope).eql(TransientScoped);

        // singleton scope
        bindingInSyntax.inSingletonScope();
        expect(binding.scope).eql(SingletonScoped);

        // set transient scope explicitly
        bindingInSyntax.inTransientScope();
        expect(binding.scope).eql(TransientScoped);

    });

});
