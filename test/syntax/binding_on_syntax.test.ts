import { expect } from "chai";
import Proxy from "harmony-proxy";
import { Binding } from "../../src/Base/Binding";
import { Context } from "../../src/Base/PlanElements";
import { getSyntaxes } from "../../src/API/BindingOnSyntax";

export const BindingInWhenOnSyntax = <T = any>(binding: Binding<T>) => {
    return getSyntaxes(binding).inWhenOnSyntax
}


describe("BindingOnSyntax", () => {

    it("Should set its own properties correctly", () => {

        interface Ninja {}
        const ninjaIdentifier = "Ninja";

        const binding = new Binding<Ninja>(ninjaIdentifier);
        const bindingOnSyntax = BindingInWhenOnSyntax<Ninja>(binding);

        // cast to any to be able to access private props
        const _bindingOnSyntax: any = bindingOnSyntax;

        expect(_bindingOnSyntax._binding.serviceIdentifier).eql(ninjaIdentifier);

    });

    it("Should be able to configure the activation handler of a binding", () => {

        interface Ninja {}
        const ninjaIdentifier = "Ninja";

        const binding = new Binding<Ninja>(ninjaIdentifier);
        const bindingOnSyntax = BindingInWhenOnSyntax<Ninja>(binding);

        bindingOnSyntax.onActivation((context: Context, ninja: Ninja) => {
            const handler = {};
            return new Proxy<Ninja>(ninja, handler);
        });

        expect(binding.onActivation).not.to.eql(undefined);

    });

});
