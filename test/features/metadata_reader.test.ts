import { Any } from "ts-toolbelt"

class Metadata{ constructor(readonly key:Any.Key, readonly value: any) {}  }

// describe("Custom Metadata Reader", () => {

//     interface FunctionWithMetadata extends Function {
//         constructorInjections: Id[];
//         propertyInjections: PropertyInjectionMetadata[];
//     }

//     interface PropertyInjectionMetadata {
//         propName: string;
//         injection: Id;
//     }

//     class StaticPropsMetadataReader extends MetadataReader {

//         public getConstructorMetadata(constructorFunc: FunctionWithMetadata) {

//             const formatMetadata = (injections: Id[]) => {
//                 const userGeneratedMetadata: { [key: string]: Metadata[]; } = {};
//                 injections.forEach((injection, index) => {
//                     const metadata = new Metadata(METADATA_KEY.INJECT_TAG, injection);
//                     if (Array.isArray(userGeneratedMetadata[index])) {
//                         userGeneratedMetadata[index].push(metadata);
//                     } else {
//                         userGeneratedMetadata[index] = [metadata];
//                     }
//                 });
//                 return userGeneratedMetadata;
//             };

//             const constructorInjections = constructorFunc.constructorInjections;

//             if (!Array.isArray(constructorInjections)) {
//                 throw new Error("Missing constructorInjections annotation!");
//             }

//             const userGeneratedConsturctorMetadata = formatMetadata(constructorInjections);
//             return userGeneratedConsturctorMetadata;

//         }


//         public getCompilerMetadata(constructorFunc: FunctionWithMetadata) {
//             const constructorInjections = constructorFunc.constructorInjections;
//             if (!Array.isArray(constructorInjections)) {
//                 throw new Error("Missing constructorInjections annotation!");
//             }
//             return new Array(constructorInjections.length);
//         }

//         public getPropertiesMetadata(constructorFunc: FunctionWithMetadata): { [key: string]: Metadata[]; } {

//             const formatMetadata = (injections: PropertyInjectionMetadata[]) => {
//                 const userGeneratedMetadata: { [key: string]: Metadata[]; } = {};
//                 injections.forEach((propInjection, index) => {
//                     const metadata = new Metadata(METADATA_KEY.INJECT_TAG, propInjection.injection);
//                     if (Array.isArray(userGeneratedMetadata[propInjection.propName])) {
//                         userGeneratedMetadata[propInjection.propName].push(metadata);
//                     } else {
//                         userGeneratedMetadata[propInjection.propName] = [metadata];
//                     }
//                 });
//                 return userGeneratedMetadata;
//             };

//             const propertyInjections = constructorFunc.propertyInjections;

//             if (!Array.isArray(propertyInjections)) {
//                 throw new Error("Missing propertyInjections annotation!");
//             }

//             const userGeneratedPropertyMetadata = formatMetadata(propertyInjections);
//             return userGeneratedPropertyMetadata;

//         }

//     }

//     it("Should be able to use custom constructor injection metadata", () => {

//         interface Ninja {
//             fight(): string;
//             sneak(): string;
//         }

//         interface Katana {
//             hit(): string;
//         }

//         interface Shuriken {
//             throw(): string;
//         }

//         class Katana implements Katana {
//             public static readonly constructorInjections = [];
//             public static readonly propertyInjections = [];
//             public hit() {
//                 return "cut!";
//             }
//         }

//         class Shuriken implements Shuriken {
//             public static readonly constructorInjections = [];
//             public static readonly propertyInjections = [];
//             public throw() {
//                 return "hit!";
//             }
//         }

//         class Ninja implements Ninja {

//             public static readonly constructorInjections = ["Katana", "Shuriken"];
//             public static readonly propertyInjections = [];

//             private _katana: Katana;
//             private _shuriken: Shuriken;

//             public constructor(
//                 katana: Katana,
//                 shuriken: Shuriken
//             ) {
//                 this._katana = katana;
//                 this._shuriken = shuriken;
//             }

//             public fight() { return this._katana.hit(); }
//             public sneak() { return this._shuriken.throw(); }

//         }

//         const container = new Container();
//         container.metadataReader = new StaticPropsMetadataReader()

//         container.bind<Ninja>("Ninja").to(Ninja);
//         container.bind<Katana>("Katana").to(Katana);
//         container.bind<Shuriken>("Shuriken").to(Shuriken);

//         const ninja = container.get<Ninja>("Ninja");

//         expect(ninja.fight()).eql("cut!");
//         expect(ninja.sneak()).eql("hit!");

//     });

//     it("Should be able to use custom prop injection metadata", () => {

//         interface Ninja {
//             fight(): string;
//             sneak(): string;
//         }

//         interface Katana {
//             hit(): string;
//         }

//         interface Shuriken {
//             throw(): string;
//         }

//         class Katana implements Katana {
//             public static readonly constructorInjections = [];
//             public static readonly propertyInjections = [];
//             public static readonly __brk = 1; // TEMP
//             public hit() {
//                 return "cut!";
//             }
//         }

//         class Shuriken implements Shuriken {
//             public static readonly constructorInjections = [];
//             public static readonly propertyInjections = [];
//             public static readonly __brk = 1; // TEMP
//             public throw() {
//                 return "hit!";
//             }
//         }

//         class Ninja implements Ninja {

//             public static readonly constructorInjections = [];

//             public static readonly propertyInjections = [
//                 { propName: "_katana", injection: "Katana" },
//                 { propName: "_shuriken", injection: "Shuriken" }
//             ];

//             public static readonly __brk = 1; // TEMP

//             private _katana: Katana;
//             private _shuriken: Shuriken;
//             public fight() { return this._katana.hit(); }
//             public sneak() { return this._shuriken.throw(); }

//         }

//         const container = new Container();
//         container.metadataReader = new StaticPropsMetadataReader()
//         container.bind<Ninja>("Ninja").to(Ninja);
//         container.bind<Katana>("Katana").to(Katana);
//         container.bind<Shuriken>("Shuriken").to(Shuriken);

//         const ninja = container.get<Ninja>("Ninja");

//         expect(ninja.fight()).eql("cut!");
//         expect(ninja.sneak()).eql("hit!");

//     });

//     it("Should be able to use extend the default metadata reader", () => {

//         const constructorMetadataLog: any[] = [];
//         const compilerMetadataLog: any[] = [];
//         const propertyMetadataLog: { [key: string]: Metadata[]; }[] = [];

//         class CustomMetadataReader extends MetadataReader {
//             public getCompilerMetadata(constructorFunc: Function): any {
//                 const compilerMetadata = super.getCompilerMetadata(constructorFunc);
//                 compilerMetadataLog.push(compilerMetadata);
//                 return compilerMetadata;
//             }
//             public getConstructorMetadata(constructorFunc: Function): any {
//                 const constructorMetadata = super.getConstructorMetadata(constructorFunc);
//                 constructorMetadataLog.push(constructorMetadata);
//                 return constructorMetadata;
//             }
//             public getPropertiesMetadata(constructorFunc: Function): { [key: string]: Metadata[]; } {
//                 const propertyMetadata = super.getPropertiesMetadata(constructorFunc);
//                 propertyMetadataLog.push(propertyMetadata);
//                 return propertyMetadata;
//             }
//         }

//         interface Ninja {
//             fight(): string;
//             sneak(): string;
//         }

//         interface Katana {
//             hit(): string;
//         }

//         interface Shuriken {
//             throw(): string;
//         }

//         @injectable
//         class Katana implements Katana {
//             public hit() {
//                 return "cut!";
//             }
//         }

//         @injectable
//         class Shuriken implements Shuriken {
//             public throw() {
//                 return "hit!";
//             }
//         }

//         @injectable
//         class Ninja implements Ninja {

//             private _katana: Katana;
//             private _shuriken: Shuriken;

//             public constructor(
//                 @inject("Katana") katana: Katana,
//                 @inject("Shuriken") shuriken: Shuriken
//             ) {
//                 this._katana = katana;
//                 this._shuriken = shuriken;
//             }

//             public fight() {return this._katana.hit(); }
//             public sneak() { return this._shuriken.throw(); }

//         }

//         const container = new Container();
//         container.metadataReader = new CustomMetadataReader()
//         container.bind<Ninja>("Ninja").to(Ninja);
//         container.bind<Katana>("Katana").to(Katana);
//         container.bind<Shuriken>("Shuriken").to(Shuriken);

//         const ninja = container.get<Ninja>("Ninja");

//         expect(ninja.fight()).eql("cut!");
//         expect(ninja.sneak()).eql("hit!");

//         expect(Array.isArray(constructorMetadataLog)).eq(true);
//         expect(constructorMetadataLog.length).eq(3);

//         const compilerGeneratedMetadata0 = compilerMetadataLog[0];

//         if (compilerGeneratedMetadata0) {
//             expect(compilerGeneratedMetadata0[0]).eq(Katana);
//             expect(compilerGeneratedMetadata0[1]).eq(Shuriken);
//         }

//         expect(constructorMetadataLog[0]["0"][0].key.toString()).eq("Symbol(inject)");
//         expect(constructorMetadataLog[0]["0"][0].value).eq("Katana");
//         expect(constructorMetadataLog[0]["1"][0].key.toString()).eq("Symbol(inject)");
//         expect(constructorMetadataLog[0]["1"][0].value).eq("Shuriken");

//         expect(JSON.stringify(compilerMetadataLog[1])).eq(JSON.stringify([]));
//         expect(JSON.stringify(compilerMetadataLog[2])).eq(JSON.stringify([]));
//         expect(JSON.stringify(constructorMetadataLog[1])).eq(JSON.stringify({}));
//         expect(JSON.stringify(constructorMetadataLog[2])).eq(JSON.stringify({}));

//         expect(propertyMetadataLog.length).eq(3);
//         expect(propertyMetadataLog[0].length).eq(0);
//         expect(propertyMetadataLog[1].length).eq(0);
//         expect(propertyMetadataLog[2].length).eq(0);

//     });

// });
