/// <reference path="../globals.d.ts" />

import { expect } from "chai";
import "es6-symbol/implement";
import { Resolver } from "../../src/API/Resolver";
import { Container, inject, injectable, named } from "../../src/inversify";
import { NAMED_TAG } from "../../src/Constants";

describe("Named default", () => {

    it("Should be able to inject a default to avoid ambiguous binding exceptions", () => {

        const TYPES = {
            Warrior: "Warrior",
            Weapon: "Weapon"
        };

        const TAG = {
            chinese: "chinese",
            japanese: "japanese",
            throwable: "throwable"
        };

        interface Weapon {
            name: string;
        }

        interface Warrior {
            name: string;
            weapon: Weapon;
        }

        @injectable()
        class Katana implements Weapon {
            public name: string;
            public constructor() {
                this.name = "Katana";
            }
        }

        @injectable()
        class Shuriken implements Weapon {
            public name: string;
            public constructor() {
                this.name = "Shuriken";
            }
        }

        @injectable()
        class Samurai implements Warrior {
            public name: string;
            public weapon: Weapon;
            public constructor(
                @inject(TYPES.Weapon) weapon: Weapon
            ) {
                this.name = "Samurai";
                this.weapon = weapon;
            }
        }

        @injectable()
        class Ninja implements Warrior {
            public name: string;
            public weapon: Weapon;
            public constructor(
                @inject(TYPES.Weapon) @named(TAG.throwable) weapon: Weapon
            ) {
                this.name = "Ninja";
                this.weapon = weapon;
            }
        }

        const container = new Container();
        container.bind<Warrior>(TYPES.Warrior).to(Ninja).whenTargetTagged(NAMED_TAG, TAG.chinese);
        container.bind<Warrior>(TYPES.Warrior).to(Samurai).whenTargetTagged(NAMED_TAG, TAG.japanese);
        container.bind<Weapon>(TYPES.Weapon).to(Shuriken).whenTargetTagged(NAMED_TAG, TAG.throwable);
        container.bind<Weapon>(TYPES.Weapon).to(Katana).whenTargetIsDefault();

        const ninja = Resolver(container).getTagged<Warrior>(TYPES.Warrior, NAMED_TAG, TAG.chinese);
        const samurai = Resolver(container).getTagged<Warrior>(TYPES.Warrior, NAMED_TAG, TAG.japanese);

        expect(ninja.name).to.eql("Ninja");
        expect(ninja.weapon.name).to.eql("Shuriken");
        expect(samurai.name).to.eql("Samurai");
        expect(samurai.weapon.name).to.eql("Katana");

    });

    it("Should be able to select a default to avoid ambiguous binding exceptions", () => {

        const TYPES = {
            Weapon: "Weapon"
        };

        const TAG = {
            throwable: "throwable"
        };

        interface Weapon {
            name: string;
        }

        @injectable()
        class Katana implements Weapon {
            public name: string;
            public constructor() {
                this.name = "Katana";
            }
        }

        @injectable()
        class Shuriken implements Weapon {
            public name: string;
            public constructor() {
                this.name = "Shuriken";
            }
        }

        const container = new Container();
        container.bind<Weapon>(TYPES.Weapon).to(Shuriken).whenTargetTagged(NAMED_TAG, TAG.throwable);
        container.bind<Weapon>(TYPES.Weapon).to(Katana).inSingletonScope().whenTargetIsDefault();

        const defaultWeapon = Resolver(container).get<Weapon>(TYPES.Weapon);
        const throwableWeapon = Resolver(container).getTagged<Weapon>(TYPES.Weapon, NAMED_TAG, TAG.throwable);

        expect(defaultWeapon.name).eql("Katana");
        expect(throwableWeapon.name).eql("Shuriken");

    });

});
