import { expect } from "chai";

describe("Metadata", () => {
  it("Should set its own properties correctly", () => {
    const m = { key: "power", value: 5 };
    expect(m.key).to.equals("power");
    expect(m.value).to.equals(5);
  });

});
