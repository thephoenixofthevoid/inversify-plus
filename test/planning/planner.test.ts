import { expect } from "chai";
import * as sinon from "sinon";
import { Target } from "../../src/Base/Target";
import { isCallStackOverflowError, tryFindingCircularDependency } from "../../src/Errors";
import { Id, inject } from "../../src/inversify";
import { injectable } from "../../src/inversify";
import { multiInject } from "../../src/inversify";
import { tagged } from "../../src/inversify";
import { targetName } from "../../src/inversify";
import { Container } from "../../src/inversify";
import { Factory } from "../../src/API/BindingOnSyntax";
import { Resolver } from "../../src/API/Resolver";
import { Context } from "../../src/Base/PlanElements";
import { TreeNode } from "../../src/Base/TreeNode";
import { INJECT_TAG, MULTI_INJECT_TAG, MODE_TAG } from "../../src/Constants";
import { Any } from "ts-toolbelt"
import { createTreeNode } from "../../src/configureTree";

function circularDependencyToException(error: Error, root: TreeNode) {
    if (isCallStackOverflowError(error)) {
        const richError = tryFindingCircularDependency(root);
        if (richError) return richError;
    }
    return error;
}

  
function plan(context: Context, isMultiInject: boolean,  targetType: any,  serviceIdentifier: Id, key?: Any.Key,  value?: any) {
    const target = Target.for(serviceIdentifier)
    target.set(MODE_TAG, targetType)
    if (isMultiInject) {
        target.set(MULTI_INJECT_TAG, serviceIdentifier);
    } else {
        target.set(INJECT_TAG, serviceIdentifier);
    }

    if (key !== undefined) target.set(key, value);

    try { 
        context.root = createTreeNode(target, null, context);
        context.root.initNode()
        context.root.validateResults();
        context.root.initDeps();
    } 
    catch (error) { 
        throw circularDependencyToException(error, context.root); 
    }
}

const MISSING_INJECTABLE_ANNOTATION = "Missing required @injectable annotation in:";
const MISSING_INJECT_ANNOTATION = "Missing required @inject or @multiInject annotation in:";
const AMBIGUOUS_MATCH = "Ambiguous match found for serviceIdentifier:";
const CIRCULAR_DEPENDENCY = "Circular dependency found:";
const NOT_REGISTERED = "No matching bindings found for serviceIdentifier:";

describe("Planner", () => {

    let sandbox: sinon.SinonSandbox;

    beforeEach(() => {
        sandbox = sinon.createSandbox();
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("Should be able to create a basic plan", () => {

        interface KatanaBlade { }

        @injectable()
        class KatanaBlade implements KatanaBlade { }

        interface KatanaHandler { }

        @injectable()
        class KatanaHandler implements KatanaHandler { }

        interface Katana { }

        @injectable()
        class Katana implements Katana {
            public handler: KatanaHandler;
            public blade: KatanaBlade;
            public constructor(
                @inject("KatanaHandler") @targetName("handler") handler: KatanaHandler,
                @inject("KatanaBlade") @targetName("blade") blade: KatanaBlade
            ) {
                this.handler = handler;
                this.blade = blade;
            }
        }

        interface Shuriken { }

        @injectable()
        class Shuriken implements Shuriken { }

        interface Ninja { }

        @injectable()
        class Ninja implements Ninja {
            public katana: Katana;
            public shuriken: Shuriken;
            public constructor(
                @inject("Katana") @targetName("katana") katana: Katana,
                @inject("Shuriken") @targetName("shuriken") shuriken: Shuriken
            ) {
                this.katana = katana;
                this.shuriken = shuriken;
            }
        }

        const ninjaId = "Ninja";
        const shurikenId = "Shuriken";
        const katanaId = "Katana";
        const katanaHandlerId = "KatanaHandler";
        const katanaBladeId = "KatanaBlade";

        const container = new Container();
        container.bind<Ninja>(ninjaId).to(Ninja);
        container.bind<Shuriken>(shurikenId).to(Shuriken);
        container.bind<Katana>(katanaId).to(Katana);
        container.bind<KatanaBlade>(katanaBladeId).to(KatanaBlade);
        container.bind<KatanaHandler>(katanaHandlerId).to(KatanaHandler);

        // Actual
        const actualPlan = new Context(container)
        plan(actualPlan, false, "Variable", ninjaId);
        const actualNinjaRequest = actualPlan.root[0];
        const actualKatanaRequest = actualNinjaRequest[0][0];
        const actualKatanaHandlerRequest = actualKatanaRequest[0][0];
        const actualKatanaBladeRequest = actualKatanaRequest[1][0];
        const actualShurikenRequest = actualNinjaRequest[1][0];

        //expect(actualNinjaRequest.serviceIdentifier).eql(ninjaId);
        expect(actualNinjaRequest.length).eql(2);

        // Katana
        //expect(actualKatanaRequest.serviceIdentifier).eql(katanaId);
        //expect(actualKatanaRequest.bindings.length).eql(1);
        expect(actualKatanaRequest.target.serviceIdentifier).eql(katanaId);
        expect(actualKatanaRequest.length).eql(2);

        // KatanaHandler
        //expect(actualKatanaHandlerRequest.serviceIdentifier).eql(katanaHandlerId);
        //expect(actualKatanaHandlerRequest.bindings.length).eql(1);
        expect(actualKatanaHandlerRequest.target.serviceIdentifier).eql(katanaHandlerId);

        // KatanaBlade
        //expect(actualKatanaBladeRequest.serviceIdentifier).eql(katanaBladeId);
        //expect(actualKatanaBladeRequest.bindings.length).eql(1);
        expect(actualKatanaBladeRequest.target.serviceIdentifier).eql(katanaBladeId);

        // Shuriken
        //expect(actualShurikenRequest.serviceIdentifier).eql(shurikenId);
        //expect(actualShurikenRequest.bindings.length).eql(1);
        expect(actualShurikenRequest.target.serviceIdentifier).eql(shurikenId);

    });

    it("Should throw when circular dependencies found", () => {

        interface A { }
        interface B { }
        interface C { }
        interface D { }

        @injectable()
        class D implements D {
            public a: typeof A;
            public constructor(
                @inject("A") a: typeof A
            ) { // circular dependency
                this.a = a;
            }
        }

        @injectable()
        class C implements C {
            public d: D;
            public constructor(
                @inject("D") d: D
            ) {
                this.d = d;
            }
        }

        @injectable()
        class B implements B { }

        @injectable()
        class A implements A {
            public b: B;
            public c: C;
            public constructor(
                @inject("B") b: B,
                @inject("C") c: C
            ) {
                this.b = b;
                this.c = c;
            }
        }

        const aId = "A";
        const bId = "B";
        const cId = "C";
        const dId = "D";

        const container = new Container();
        container.bind<A>(aId).to(A);
        container.bind<B>(bId).to(B);
        container.bind<C>(cId).to(C);
        container.bind<D>(dId).to(D);

        const throwErrorFunction = () => {
            Resolver(container).get(aId);
        };

        expect(throwErrorFunction).to.throw(
            `${CIRCULAR_DEPENDENCY} A --> C --> D --> A`
        );

    });

    it("Should only plan sub-dependencies when binding type is BindingType.Instance", () => {

        interface KatanaBlade { }

        @injectable()
        class KatanaBlade implements KatanaBlade { }

        interface KatanaHandler { }

        @injectable()
        class KatanaHandler implements KatanaHandler { }

        interface Katana { }

        @injectable()
        class Katana implements Katana {
            public handler: KatanaHandler;
            public blade: KatanaBlade;
            public constructor(
                @inject("KatanaHandler") @targetName("handler") handler: KatanaHandler,
                @inject("KatanaBlade") @targetName("blade") blade: KatanaBlade
            ) {
                this.handler = handler;
                this.blade = blade;
            }
        }

        interface Shuriken { }

        @injectable()
        class Shuriken implements Shuriken { }

        interface Ninja { }

        @injectable()
        class Ninja implements Ninja {
            public katanaFactory: Factory<Katana>;
            public shuriken: Shuriken;
            public constructor(
                @inject("Factory<Katana>") @targetName("katanaFactory") katanaFactory: Factory<Katana>,
                @inject("Shuriken") @targetName("shuriken") shuriken: Shuriken
            ) {
                this.katanaFactory = katanaFactory;
                this.shuriken = shuriken;
            }
        }

        const ninjaId = "Ninja";
        const shurikenId = "Shuriken";
        const katanaId = "Katana";
        const katanaHandlerId = "KatanaHandler";
        const katanaBladeId = "KatanaBlade";
        const katanaFactoryId = "Factory<Katana>";

        const container = new Container();
        container.bind<Ninja>(ninjaId).to(Ninja);
        container.bind<Shuriken>(shurikenId).to(Shuriken);
        container.bind<Katana>(katanaBladeId).to(Katana);
        container.bind<KatanaBlade>(katanaBladeId).to(KatanaBlade);
        container.bind<KatanaHandler>(katanaHandlerId).to(KatanaHandler);
        container.bind<Factory<Katana>>(katanaFactoryId).toFactory((context: Context) =>
            () => Resolver(context.container).get<Katana>(katanaId));

        const actualPlan =new Context(container);
        plan(actualPlan, false, "Variable", ninjaId);

        expect(actualPlan.root[0].target.serviceIdentifier).eql(ninjaId);
        expect(actualPlan.root[0][0].target.serviceIdentifier).eql(katanaFactoryId);
        expect(actualPlan.root[0][0][0].length).eql(0); // IMPORTANT!
        expect(actualPlan.root[0][1].target.serviceIdentifier).eql(shurikenId);
        expect(actualPlan.root[0][1][0].length).eql(0);
        expect(actualPlan.root[0][2]).eql(undefined);

    });

    it("Should generate plans with multi-injections", () => {

        interface Weapon { }

        @injectable()
        class Katana implements Weapon { }

        @injectable()
        class Shuriken implements Weapon { }

        interface Ninja { }

        @injectable()
        class Ninja implements Ninja {
            public katana: Weapon;
            public shuriken: Weapon;
            public constructor(
                @multiInject("Weapon") @targetName("weapons") weapons: Weapon[]
            ) {
                this.katana = weapons[0];
                this.shuriken = weapons[1];
            }
        }

        const ninjaId = "Ninja";
        const weaponId = "Weapon";

        const container = new Container();
        container.bind<Ninja>(ninjaId).to(Ninja);
        container.bind<Weapon>(weaponId).to(Shuriken);
        container.bind<Weapon>(weaponId).to(Katana);

        const actualPlan: any = new Context(container)
        plan(actualPlan, false, "Variable", ninjaId);

        // root request has no target
        //expect(actualPlan.root.serviceIdentifier).eql(ninjaId);
        expect(actualPlan.root.target.serviceIdentifier).eql(ninjaId);
        expect(actualPlan.root.target.has(MULTI_INJECT_TAG)).eql(false);

        // root request should only have one child request with target weapons/Weapon[]
        // expect(actualPlan.root[0].serviceIdentifier).eql("Weapon");
        expect(actualPlan.root[0][1]).eql(undefined);
        expect(actualPlan.root[0][0].target.name).eql("weapons");
        expect(actualPlan.root[0][0].target.serviceIdentifier).eql("Weapon");
        expect(actualPlan.root[0][0].target.has(MULTI_INJECT_TAG)).eql(true);

        // child request should have two child requests with targets weapons/Weapon[] but bindings Katana and Shuriken
        expect(actualPlan.root[0][0].length).eql(2);

        //expect(actualPlan.root[0][0][0].serviceIdentifier).eql(weaponId);
        expect(actualPlan.root[0][0][0].target.name).eql("weapons");
        expect(actualPlan.root[0][0][0].target.serviceIdentifier).eql(weaponId);
        expect(actualPlan.root[0][0][0].target.has(MULTI_INJECT_TAG)).eql(true);
        //expect(actualPlan.root[0][0][0].serviceIdentifier).eql("Weapon");
        expect(actualPlan.root[0][0][0].binding.serviceIdentifier).eql("Weapon");
        const shurikenImplementationType: any = actualPlan.root[0][0][0].binding.value;
        expect(shurikenImplementationType.name).eql("Shuriken");

        //expect(actualPlan.root[0][0][1].serviceIdentifier).eql(weaponId);
        expect(actualPlan.root[0][0][1].target.name).eql("weapons");
        expect(actualPlan.root[0][0][1].target.serviceIdentifier).eql("Weapon");
        expect(actualPlan.root[0][0][1].target.has(MULTI_INJECT_TAG)).eql(true);
        //expect(actualPlan.root[0][0][1].serviceIdentifier).eql("Weapon");
        expect(actualPlan.root[0][0][1].binding.serviceIdentifier).eql("Weapon");
        const katanaImplementationType: any = actualPlan.root[0][0][1].binding.value;
        expect(katanaImplementationType.name).eql("Katana");

    });

    it("Should throw when no matching bindings are found", () => {

        interface Katana { }
        @injectable()
        class Katana implements Katana { }

        interface Shuriken { }
        @injectable()
        class Shuriken implements Shuriken { }

        interface Ninja { }

        @injectable()
        class Ninja implements Ninja {
            public katana: Katana;
            public shuriken: Shuriken;
            public constructor(
                @inject("Katana") @targetName("katana") katana: Katana,
                @inject("Shuriken") @targetName("shuriken") shuriken: Shuriken
            ) {
                this.katana = katana;
                this.shuriken = shuriken;
            }
        }

        const ninjaId = "Ninja";
        const shurikenId = "Shuriken";

        const container = new Container();
        container.bind<Ninja>(ninjaId).to(Ninja);
        container.bind<Shuriken>(shurikenId).to(Shuriken);

        const throwFunction = () => {
            plan(new Context(container), false, "Variable", ninjaId);
        };

        expect(throwFunction).to.throw(`${NOT_REGISTERED} Katana`);

    });

    it("Should throw when an ambiguous match is found", () => {

        interface Katana { }

        @injectable()
        class Katana implements Katana { }

        @injectable()
        class SharpKatana implements Katana { }

        interface Shuriken { }
        class Shuriken implements Shuriken { }

        interface Ninja { }

        @injectable()
        class Ninja implements Ninja {
            public katana: Katana;
            public shuriken: Shuriken;
            public constructor(
                @inject("Katana") katana: Katana,
                @inject("Shuriken") shuriken: Shuriken
            ) {
                this.katana = katana;
                this.shuriken = shuriken;
            }
        }

        const ninjaId = "Ninja";
        const katanaId = "Katana";
        const shurikenId = "Shuriken";

        const container = new Container();
        container.bind<Ninja>(ninjaId).to(Ninja);
        container.bind<Katana>(katanaId).to(Katana);
        container.bind<Katana>(katanaId).to(SharpKatana);
        container.bind<Shuriken>(shurikenId).to(Shuriken);

        const throwFunction = () => {
            plan(new Context(container), false, "Variable", ninjaId);
        };

        expect(throwFunction).to.throw(`${AMBIGUOUS_MATCH} Katana`);

    });

    it("Should apply constrains when an ambiguous match is found", () => {

        interface Weapon { }

        @injectable()
        class Katana implements Weapon { }

        @injectable()
        class Shuriken implements Weapon { }

        interface Ninja { }

        const ninjaId = "Ninja";
        const weaponId = "Weapon";

        @injectable()
        class Ninja implements Ninja {
            public katana: Weapon;
            public shuriken: Weapon;
            public constructor(
                @inject(weaponId) @targetName("katana") @tagged("canThrow", false) katana: Weapon,
                @inject(weaponId) @targetName("shuriken") @tagged("canThrow", true) shuriken: Weapon
            ) {
                this.katana = katana;
                this.shuriken = shuriken;
            }
        }

        const container = new Container();
        container.bind<Ninja>(ninjaId).to(Ninja);
        container.bind<Weapon>(weaponId).to(Katana).whenTargetTagged("canThrow", false);
        container.bind<Weapon>(weaponId).to(Shuriken).whenTargetTagged("canThrow", true);

        const actualPlan = new Context(container);
        plan(actualPlan, false, "Variable", ninjaId);

        // root request has no target
        //expect(actualPlan.root.serviceIdentifier).eql(ninjaId);
        expect(actualPlan.root.target.serviceIdentifier).eql(ninjaId);
        expect(actualPlan.root.target.has(MULTI_INJECT_TAG)).eql(false);

        // root request should have 2 child requests
        expect(actualPlan.root[0][0].target.serviceIdentifier).eql(weaponId);
        expect(actualPlan.root[0][0].target.name).eql("katana");
        expect(actualPlan.root[0][0].target.serviceIdentifier).eql(weaponId);

        expect(actualPlan.root[0][1].target.serviceIdentifier).eql(weaponId);
        expect(actualPlan.root[0][1].target.name).eql("shuriken");
        expect(actualPlan.root[0][1].target.serviceIdentifier).eql(weaponId);

        expect(actualPlan.root[0][2]).eql(undefined);

    });

    it("Should be throw when a class has a missing @injectable annotation", () => {

        interface Weapon { }

        class Katana implements Weapon { }

        const container = new Container();
        container.bind<Weapon>("Weapon").to(Katana);

        const throwFunction = () => {
            plan(new Context(container), false, "Variable", "Weapon");
        };

        expect(throwFunction).to.throw(`${MISSING_INJECTABLE_ANNOTATION} Katana.`);

    });

    it("Should ignore checking base classes for @injectable when skipBaseClassChecks is set on the container", () => {
        class Test { }

        @injectable()
        class Test2 extends Test { }

        const container = new Container() //({skipBaseClassChecks: true});
        container.bind(Test2).toSelf();
        Resolver(container).get(Test2);
    });

    it("Should ignore checking base classes for @injectable on resolve when skipBaseClassChecks is set", () => {
        class Test { }

        @injectable()
        class Test2 extends Test { }

        const container = new Container()//({skipBaseClassChecks: true});
        Resolver(container).resolve(Test2);
    });

    it("Should throw when an class has a missing @inject annotation", () => {

        interface Sword { }

        @injectable()
        class Katana implements Sword { }

        interface Warrior { }

        @injectable()
        class Ninja implements Warrior {

            public katana: Katana;

            public constructor(
                katana: Sword
            ) {
                this.katana = katana;
            }
        }

        const container = new Container();
        container.bind<Warrior>("Warrior").to(Ninja);
        container.bind<Sword>("Sword").to(Katana);

        const throwFunction = () => {
            plan(new Context(container), false, "Variable", "Warrior");
        };

        expect(throwFunction).to.throw(`${MISSING_INJECT_ANNOTATION} argument 0 in class Ninja.`);

    });

    it("Should throw when a function has a missing @injectable annotation", () => {

        interface Katana { }

        @injectable()
        class Katana implements Katana { }

        interface Ninja { }

        @injectable()
        class Ninja implements Ninja {

            public katana: Katana;

            public constructor(
                katanaFactory: () => Katana
            ) {
                this.katana = katanaFactory();
            }
        }

        const container = new Container();
        container.bind<Ninja>("Ninja").to(Ninja);
        container.bind<Katana>("Katana").to(Katana);
        container.bind<Katana>("Factory<Katana>").to(Katana);

        const throwFunction = () => {
            plan(new Context(container), false, "Variable", "Ninja");
        };

        expect(throwFunction).to.throw(`${MISSING_INJECT_ANNOTATION} argument 0 in class Ninja.`);
    });
});
