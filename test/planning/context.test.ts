import { expect } from "chai";
import { Container} from "../../src/inversify";
import { Context } from "../../src/Base/PlanElements";
import { TreeNode } from "../../src/Base/TreeNode";
import { Target } from "../../src/Base/Target";

describe("Context", () => {

  it("Should set its own properties correctly", () => {

      const container = new Container();
      const context1  = new Context(container)

      const invalid: any = null;
      const context2: Context = new (Context)(invalid);

      expect(context1.container).not.to.eql(null);
      expect(context2.container).eql(null);
      //expect(context1.id).to.be.a("number");
      //expect(context2.id).to.be.a("number");
      //expect(context1.id).not.eql(context2.id);

  });

  it("Should be linkable to a Plan", () => {

      const container = new Container();
      const context = new Context(container)
      const target = Target.for("Ninja");

      const ninjaRequest = new TreeNode(
          null,
          context,
          target
      );

      context.root = ninjaRequest;
      expect(context.root.target.serviceIdentifier).eql("Ninja");
  });

});
