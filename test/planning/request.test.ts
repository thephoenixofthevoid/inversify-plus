import { expect } from "chai";
import { Container } from "../../src/inversify";
import { TreeNode } from "../../src/Base/TreeNode";
import { Context } from "../../src/Base/PlanElements";
import { Target } from "../../src/Base/Target";


describe("Request", () => {

  const identifiers = {
      Katana: "Katana",
      KatanaBlade: "KatanaBlade",
      KatanaHandler: "KatanaHandler",
      Ninja: "Ninja",
      Shuriken: "Shuriken",
  };

  it("Should set its own properties correctly", () => {

      const container = new Container();
      const context = new Context(container);

      const request1 = new TreeNode(
          null,
          context,
          Target.for(identifiers.Ninja)
      );

      expect(request1.target.serviceIdentifier).eql(identifiers.Ninja);
      // expect(Array.isArray(request1.bindings)).eql(true);
      // expect(Array.isArray(request2.bindings)).eql(true);
      //expect(request1.id).to.be.a("number");
      //expect(request2.id).to.be.a("number");
      //expect(request1.id).not.eql(request2.id);

  });

//   it("Should be able to add a child request", () => {

//       const container = new Container();
//       const context = new Context(container);

//       const ninjaRequest: Group = new Group(
//           null,
//           context,
//           new Target("Variable", "Ninja", identifiers.Ninja)
//       );

//       const child = new Group(
//         ninjaRequest,
//         context,
//         new Target("ConstructorArgument", "Katana", identifiers.Katana)
//     );
//       ninjaRequest.children.push(child);

//       const katanaRequest = ninjaRequest.children[0];

//       expect(katanaRequest.target.serviceIdentifier).eql(identifiers.Katana);
//       expect(katanaRequest.target.name).eql("Katana");
//       expect(katanaRequest.children.length).eql(0);

//       const katanaParentRequest: Candidate = katanaRequest.parent as any;
//       expect(katanaParentRequest.target.serviceIdentifier).eql(identifiers.Ninja);
//       expect(katanaParentRequest.target.name).eql("Ninja");
//       expect(katanaParentRequest.target.serviceIdentifier).eql(identifiers.Ninja);

//   });

});
