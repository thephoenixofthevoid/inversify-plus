import { expect } from "chai";
import { Target } from "../../src/Base/Target";
import { INJECT_TAG, MULTI_INJECT_TAG, NAMED_TAG, MODE_TAG, NAME_TAG, isServiceTag } from "../../src/Constants";

function isTagged(target: Target) {
    for (let m of target.keys()) 
        if (!isServiceTag(m)) return true;
    return false; 
}

describe("Target", () => {

    it("Should be able to create instances of untagged targets", () => {
        const target = Target.for("Katana");
        target.set(MODE_TAG, "ConstructorArgument")
        target.set(NAME_TAG, "katana")
        expect(target.serviceIdentifier).to.be.eql("Katana");
        expect(target.name).to.be.eql("katana");
        //expect(Array.isArray(target.metadata)).to.be.eql(true);
        //expect(target.size).to.be.eql(0);
    });

    it("Should be able to create instances of named targets", () => {
        const target = Target.for("Katana");
        target.set(MODE_TAG, "ConstructorArgument")
        target.set(NAME_TAG, "katana")
        target.set(NAMED_TAG, "primary");
        expect(target.serviceIdentifier).to.be.eql("Katana");
        expect(target.name).to.be.eql("katana");
        //expect(Array.isArray(target.metadata)).to.be.eql(true);
        //expect(target.size).to.be.eql(1);
        //expect(target.metadata[0].key).to.be.eql(NAMED_TAG);
        //expect(target.metadata[0].value).to.be.eql("primary");
    });

    it("Should be able to create instances of tagged targets", () => {
        const target = Target.for("Katana");
        target.set(NAME_TAG, "katana")
        target.set(MODE_TAG, "ConstructorArgument")
        target.set(NAME_TAG, "katana")
        target.set("power", 5);
        expect(target.serviceIdentifier).to.be.eql("Katana");
        expect(target.name).to.be.eql("katana");
        //expect(Array.isArray(target.metadata)).to.be.eql(true);
        //expect(target.size).to.be.eql(1);
        //expect(target.metadata[0].key).to.be.eql("power");
        //expect(target.metadata[0].value).to.be.eql(5);
    });

    it("Should be able to identify named metadata", () => {
        const target1 = Target.for("Katana");
        target1.set(NAME_TAG, "katana")
        target1.set(MODE_TAG, "ConstructorArgument")
        target1.set(NAME_TAG, "katana")
        target1.set(NAMED_TAG, "primary");
        expect(target1.has(NAMED_TAG)).to.be.eql(true);
        const target2 = Target.for("Katana");
        target2.set(MODE_TAG, "ConstructorArgument")
        target2.set(NAME_TAG, "katana")
        target2.set("power", 5);
        expect(target2.has(NAMED_TAG)).to.be.eql(false);
    });

    it("Should be able to identify multi-injections", () => {
        const target1 = Target.for("Katana");
        target1.set(NAME_TAG, "katana")
        target1.set(MODE_TAG, "ConstructorArgument")
        target1.set(NAME_TAG, "katana")
        target1.set(MULTI_INJECT_TAG, "Katana")
        expect(target1.has(MULTI_INJECT_TAG)).to.be.eql(true);
        const target2 = Target.for("Katana");
        target2.set(NAME_TAG, "katana")
        target2.set(MODE_TAG, "ConstructorArgument")
        expect(target2.has(MULTI_INJECT_TAG)).to.be.eql(false);
    });

    it("Should be able to match multi-inject for a specified service metadata", () => {
        const target1 = Target.for("Katana");
        target1.set(NAME_TAG, "katana")
        target1.set(MODE_TAG, "ConstructorArgument")
        target1.set(MULTI_INJECT_TAG, "Katana")
        target1.set(INJECT_TAG, "Shuriken")
        expect(target1.get(MULTI_INJECT_TAG) ==="Katana").to.be.eql(true);
        expect(target1.get(MULTI_INJECT_TAG) ==="Shuriken").to.be.eql(false);
    });

    it("Should be able to match named metadata", () => {
        const target1 = Target.for("Katana");
        target1.set(NAME_TAG, "katana")
        target1.set(MODE_TAG, "ConstructorArgument")
        target1.set(NAMED_TAG, "primary");
        expect(target1.get(NAMED_TAG) === "primary").to.be.eql(true);
        expect(target1.get(NAMED_TAG) === "secondary").to.be.eql(false);
    });

    it("Should be able to identify tagged metadata", () => {

        const target = Target.for("Katana");
        target.set(NAME_TAG, "katana")
        target.set(MODE_TAG, "ConstructorArgument")
        expect(isTagged(target)).to.be.eql(false);

        const target1 = Target.for("Katana");
        target1.set(NAME_TAG, "katana")
        target1.set(MODE_TAG, "ConstructorArgument")
        target1.set("power", 5);
        expect(isTagged(target1)).to.be.eql(true);

        const target2 = Target.for("Katana");
        target2.set(NAME_TAG, "katana")
        target2.set(MODE_TAG, "ConstructorArgument")
        target2.set(NAMED_TAG, "primary");
        expect(isTagged(target2)).to.be.eql(false);

        const target3 = Target.for("Katana");
        target3.set(NAME_TAG, "katana")
        target3.set(MODE_TAG, "ConstructorArgument")
        target3.set("power", 5);
        target3.set("speed", 5);
        expect(isTagged(target3)).to.be.eql(true);
    });

    it("Should be able to match tagged metadata", () => {
        const target1 = Target.for("Katana");
        target1.set(NAME_TAG, "katana")
        target1.set(MODE_TAG, "ConstructorArgument")
        target1.set("power", 5);
        expect(target1.get("power") === 5).to.be.eql(true);
        expect(target1.get("power") === 2).to.be.eql(false);
    });

    // it("Should contain an unique identifier", () => {
    //     const target1 = Target.for("Katana");
    //     target1.set(NAME_TAG, "katana")
    //     target1.set("power", 5);
    //     const target2 = Target.for("katana", "Katana");
    //     target2.set("power", 5);
    //     expect(target1.id).to.be.a("number");
    //     expect(target2.id).to.be.a("number");
    //     expect(target1.id).not.eql(target2.id);
    // });

});
