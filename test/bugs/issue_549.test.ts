import { Resolver } from "../../src/API/Resolver";
import { Container, inject, injectable } from "../../src/inversify";

const CIRCULAR_DEPENDENCY_IN_FACTORY = (...values: any[]) => `It looks like there is a circular dependency in one of the '${values[0]}' bindings. Please investigate bindings with service identifier '${values[1]}'.`;

describe("Issue 549", () => {

    it("Should throw if circular dependencies found with dynamics", () => {

        const TYPE = {
            ADynamicValue: Symbol.for("ADynamicValue"),
            BDynamicValue: Symbol.for("BDynamicValue")
        };

        @injectable()
        class A {
            public b: typeof B;
            public constructor(
                @inject(TYPE.BDynamicValue)  b: typeof B
            ) {
                this.b = b;
            }
        }

        @injectable()
        class B {
            public a: typeof A;
            public constructor(
                @inject(TYPE.ADynamicValue) a: typeof A
            ) {
                this.a = a;
            }
        }

        const container = new Container();
        container.bind(A).toSelf().inSingletonScope();
        container.bind(B).toSelf().inSingletonScope();

        container.bind(TYPE.ADynamicValue).toDynamicValue((ctx) => Resolver(ctx.container).get(A));
        container.bind(TYPE.BDynamicValue).toDynamicValue((ctx) => Resolver(ctx.container).get(B));

        function willThrow() {
            return Resolver(container).get<A>(A);
        }

        try {
            const result = willThrow();
            throw new Error(
                `This line should never be executed. Expected 'willThrow' to throw! ${JSON.stringify(result)}`
            );
        } catch (e) {

            const expectedErrorA = CIRCULAR_DEPENDENCY_IN_FACTORY("toDynamicValue", TYPE.ADynamicValue.toString());
            const expectedErrorB = CIRCULAR_DEPENDENCY_IN_FACTORY("toDynamicValue", TYPE.BDynamicValue.toString());
            const matchesErrorA = e.message.indexOf(expectedErrorA) !== -1;
            const matchesErrorB = e.message.indexOf(expectedErrorB) !== -1;

            if (!matchesErrorA && !matchesErrorB) {
                throw new Error(
                    "Expected 'willThrow' to throw:\n" +
                    `- ${expectedErrorA}\n` +
                    "or\n" +
                    `- ${expectedErrorB}\n` +
                    "but got\n" +
                    `- ${e.message}`
                );
            }

        }

    });

});
