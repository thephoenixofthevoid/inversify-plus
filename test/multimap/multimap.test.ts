import { expect } from "chai";
import * as sinon from "sinon";
import { MultiMap } from "../../src/Base/MultiMap";

describe("Middleware", () => {

    it("Can be instantiated", () => {
        const map1 = new MultiMap();

        map1.add("test", 1)
        map1.add("test", 2)
        map1.add("test", 3)

        const map2 = new MultiMap();

        map2.add("test1", 1)
        map2.add("test1", 2)
        map2.add("test1", 3)


        const map = new MultiMap(map1, map2);


        console.log(map)
    });
});
