import path from 'path'
import webpack from 'webpack'
import webpackDevServer from 'webpack-dev-server'
import ReactRefreshWebpackPlugin from '@pmmmwh/react-refresh-webpack-plugin'
import HtmlWebpackPlugin from 'html-webpack-plugin';

const config: webpack.Configuration = {
    mode: "development", 
    entry: path.join(__dirname, '/entry/index.tsx'),
    output: {
        path: path.join(__dirname, '/dist'),
        filename: 'bundle.js'
    },
    module: {
        rules: [
            {
                test: /\.less$/,
                use: [
                  {
                    loader: 'style-loader',
                  },
                  {
                    loader: 'css-loader',
                  },
                  {
                    loader: 'less-loader',
                    options: {
                      lessOptions: {
                        strictMath: true,
                      },
                    },
                  },
                ],
            },
            {
                test: /\.[jt]sx?$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                    options: {
                      presets: ['@babel/env', '@babel/typescript', "@babel/react" ],
                      //plugins: ['react-refresh/babel' ]
                    }
                },
            },
        ],
    },
    plugins: [
        new HtmlWebpackPlugin({ filename: './index.html' })
    //    new webpack.HotModuleReplacementPlugin(),
    //    new ReactRefreshWebpackPlugin(),
    ]
};

const options = {
    contentBase: './dist',
    hot: true,
    host: 'localhost',
}

//webpackDevServer.addDevServerEntrypoints(config, options);
const compiler = webpack(config, () => {
    console.log(1)
})
//const server = new webpackDevServer(compiler, options);

//server.listen(5000, 'localhost', () => {
//    console.log('dev server listening on port 5000');
//});
