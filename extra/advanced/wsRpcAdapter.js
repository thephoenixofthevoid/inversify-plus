"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const path_1 = __importDefault(require("path"));
const webpack_1 = __importDefault(require("webpack"));
const webpack_dev_server_1 = __importDefault(require("webpack-dev-server"));
const config = {
    devtool: 'cheap-module-eval-source-map',
    entry: [
        'webpack-dev-server/client?http://0.0.0.0:3000',
        'webpack/hot/only-dev-server',
        './scripts/index'
    ],
    devServer: {
        contentBase: './dist',
        hot: true,
    },
    output: {
        path: path_1.default.join(__dirname, 'dist'),
        filename: 'bundle.js'
    },
    plugins: [
        new webpack_1.default.HotModuleReplacementPlugin()
    ]
};
const options = {
    contentBase: './dist',
    hot: true,
    host: 'localhost',
};
webpack_dev_server_1.default.addDevServerEntrypoints(config, options);
const compiler = webpack_1.default(config);
const server = new webpack_dev_server_1.default(compiler, options);
server.listen(5000, 'localhost', () => {
    console.log('dev server listening on port 5000');
});

//# sourceMappingURL=wsRpcAdapter.js.map
