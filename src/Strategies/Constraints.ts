import { EntityRequest } from "../Base/PlanElements";
import { TreeNode } from "../Base/TreeNode";
import { ConstraintFunction } from "../Base/Binding";
import { Any } from "ts-toolbelt"
import { isServiceTag, NAMED_TAG } from "../Constants";
import { InstanceResolvingStrategy } from "./ResolvingStrategy";

export function traverseAncerstors(request: EntityRequest|TreeNode|null, constraint: ConstraintFunction): boolean {
    while (request instanceof EntityRequest && request.ancerstor !== null) 
        if (constraint(request = request.ancerstor)) return true;
    return false
}

export function taggedConstraint(key: Any.Key, value: any): ConstraintFunction {
    constraint.metaData = { key, value };
    return constraint;

    function constraint(r: EntityRequest | null) {
        if (r === null) return false;
        if (r.target === null) return false;
        return r.target.get(key) === value;
    }
}

export function typeConstraint(type: (Function | string)) {
    if (typeof type === "string") {
        return function (request: EntityRequest|null) {
            if (!request) return false;
            return type === request.binding.serviceIdentifier
        }
    } else {
        return function (request: EntityRequest|null) {
            if (!request) return false;
            if (request.binding.type !== InstanceResolvingStrategy) return false;
            return type === request.binding.value
        }
    }
}

export function atParent(constraint: ConstraintFunction) {
    return function (request: EntityRequest | TreeNode | null) {
        if (request instanceof EntityRequest && request.ancerstor) {
            return constraint(request.ancerstor)
        } else {
            return false;
        }
    }
}

export function atAncestor(constraint: ConstraintFunction) {
    return function (request: EntityRequest | TreeNode | null) {
        return traverseAncerstors(request, constraint)
    }
}

export function noAncestor(constraint: ConstraintFunction) {
    return function (request: EntityRequest | TreeNode | null) {
        return !traverseAncerstors(request, constraint)
    }
}

export function isDefault(request: EntityRequest | TreeNode | null) {
    if (request === null) return false;
    if (request instanceof TreeNode) return false;
    if (request.target.has(NAMED_TAG)) return false;

    for (let m of request.target.keys()) {
        if (isServiceTag(m)) continue;
        return false;
    }

    return true;
}