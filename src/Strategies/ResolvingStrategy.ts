import { InvalidInjectionPointError } from "../Errors";
import { Newable } from "../inversify";
import {
    EntityRequest
} from "../Base/PlanElements";
import { ClassMetadata } from "../Base/ClassMetadata";
import { POST_CONSTRUCT } from "../Constants";



export abstract class ResolvingStrategy<T = any> {
    constructor(readonly ctx: EntityRequest) {
        if (ctx.binding.value === null)
            throw new InvalidInjectionPointError(ctx);
    }

    public runResover() {
        try {
            this.ctx.result = this.resolve();
        } catch (error) {
            this.handleError(error);
        }
    }

    resolve() {
        return this.ctx.binding.value;
    }

    handleError(error: Error) {
        throw error;
    }
}

export class InvalidResolvingStrategy<T> extends ResolvingStrategy<T> {
    static get tag() {
        return "Invalid"
    }
}

export class ConstantValueResolvingStrategy<T> extends ResolvingStrategy<T> {
    static get tag() {
        return "ConstantValue"
    }
}

export class InstanceResolvingStrategy<T> extends ResolvingStrategy<T> {
    static get tag() {
        return "Instance"
    }

    get args() {
        const result: any = []
        for (const req of (this.ctx as EntityRequest)) {
            if (req.target.type !== "ConstructorArgument") continue;
            result.push(req.getResults())
        }
        return result
    }

    get props() {
        const result: any = {}
        for (const req of (this.ctx as EntityRequest)) {
            if (req.target.type !== "ClassProperty") continue;
            result[req.target.name] = req.getResults()
        }
        return result
    }

    resolve(): any {
        const constr: Newable<any> = this.ctx.binding.value;
        const result = Reflect.construct(constr, this.args)
        Object.assign(result, this.props)

        try {
            let postcMetadata = ClassMetadata.of(constr).getValue(POST_CONSTRUCT);
            if (postcMetadata) result[postcMetadata]();
        } catch (e) {
            throw new Error(
                `@postConstruct error in class ${constr.name}: ${e.message}`
            );
        }
        return result;
    }
}

export class DynamicValueResolvingStrategy<T> extends ResolvingStrategy<T> {

    static get tag() {
        return "DynamicValue"
    }

    resolve() {
        return this.ctx.binding.value!(this.ctx.context);
    }

    handleError(error: Error) {
        if (error instanceof RangeError || error.message === "Maximum call stack size exceeded")
            throw new Error(
                `It looks like there is a circular dependency in one of the 'to${this.ctx.binding.type.tag
                }' bindings. Please investigate bindings with service identifier '${this.ctx.binding.serviceIdentifier.toString()}'.`
            );
        throw error;
    }
}
