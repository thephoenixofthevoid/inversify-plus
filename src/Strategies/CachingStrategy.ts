import { EntityRequest } from "../Base/PlanElements";

export abstract class CachingStrategy<T = any> {
  public constructor(readonly context: EntityRequest) {}
  abstract getCache(): T | null;
  abstract setCache(value: T): void;
}

export class SingletonScoped<T> extends CachingStrategy<T> {
  static singletons = new WeakMap();
  getCache(): T | null {
    return SingletonScoped.singletons.get(this.context.binding);
  }
  setCache(value: T): void {
    SingletonScoped.singletons.set(this.context.binding, value);
  }
}

export class TransientScoped<T> extends CachingStrategy<T> {
  getCache(): T | null {
    return null;
  }
  setCache(value: T): void {}
}

export class RequestScoped<T> extends CachingStrategy<T> {
  static scopes = new WeakMap();
  get scope() {
    if (!RequestScoped.scopes.has(this.context.context))
      RequestScoped.scopes.set(this.context.context, new WeakMap());
    return RequestScoped.scopes.get(this.context.context);
  }
  getCache(): T | null {
    return this.scope.get(this.context.binding);
  }
  setCache(value: T): void {
    this.scope.set(this.context.binding, value);
  }
}
