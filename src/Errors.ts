import { NAMED_TAG, isServiceTag } from "./Constants";
import { EntityRequest } from "./Base/PlanElements";
import { TreeNode } from "./Base/TreeNode";
import { Binding } from "./Base/Binding";
import { Target } from "./Base/Target";
import { Container } from "./IocContainer";
import { Id } from "./inversify";

export function getFunctionName(v: any): string {
  if (v.name) return v.name;
  const match = v.toString().match(/^function\s*([^\s(]+)/);
  if (!match) return `Anonymous function: ${v.toString()}`;
  return match[1];
}

function metadataToString({ key, value }: any) {
  if (key === NAMED_TAG) return `named: ${value.toString()} `;
  return `tagged: { key:${key.toString()}, value: ${value.toString()} }`;
}

function getServiceIdentifierString(id: Id) {
  if (typeof id == "function") return id.name;
  if (typeof id == "symbol") return id.toString();
  return `${id}`;
}

class DIError extends Error {
  constructor(
    message: string,
    {
      node,
      target,
      printMetadata = false,
    }: { target: Target; container?: Container; printMetadata?: boolean, node?: TreeNode, }
  ) {
    const name = getServiceIdentifierString(target.serviceIdentifier);
    const parts = [message, ": ", name];

    if (printMetadata) {
      const tags: string[] = [...target.keys()]
        .filter((key) => key === NAMED_TAG || !isServiceTag(key))
        .map((key) => metadataToString({ key, value: target.get(key) }));
      if (tags.length) parts.push(`\n ${name} - ${tags.join("\n")}\n`);
    }
    if (node) {
      let bindings: Binding<Container>[] = node.extractor()
      if (bindings.length) parts.push("\nRegistered bindings:");
      for (let binding of bindings) {
        parts.push("\n ", getFunctionName(binding.value));
        if (!binding.constraint?.metaData) continue;
        parts.push(` - ${metadataToString(binding.constraint.metaData)}`);
      }
    }

    super(parts.join(""));
  }
}

class UnsatisfiedDependencyError extends DIError {
  constructor(node: TreeNode) {
    super("No matching bindings found for serviceIdentifier", {
      node,
      target: node.target,
      printMetadata: true,
    });
  }
}

class AmbiguousDependencyError extends DIError {
  constructor(node: TreeNode) {
    super("Ambiguous match found for serviceIdentifier", {
      node,
      target: node.target,
      printMetadata: false,
    });
  }
}

class InvalidInjectionPointError extends DIError {
  constructor(node: { target: Target }) {
    super("Invalid binding type", {
      target: node.target,
    });
  }
}

export {
  UnsatisfiedDependencyError,
  AmbiguousDependencyError,
  InvalidInjectionPointError,
};

function errorCircularDependency(current: EntityRequest | null) {
  const parts: Id[] = [];
  while (current) {
    parts.unshift(current.target.serviceIdentifier);
    current = current.ancerstor;
  }
  return new Error(
    `Circular dependency found: ${parts
      .map(getServiceIdentifierString)
      .join(" --> ")}`
  );
}

function checkCircularDependency(request: EntityRequest) {
  let current: EntityRequest | null = request;
  while ((current = current.ancerstor)) {
    if (current.target.serviceIdentifier === request.target.serviceIdentifier) {
      return true;
    }
  }
  return false;
}

export function tryFindingCircularDependency(rootRequest: TreeNode) {
  const order: (EntityRequest | TreeNode)[] = [rootRequest];
  var request;

  while ((request = order.shift())) {
    order.unshift(...request.values());
    if (!(request instanceof EntityRequest)) continue;
    if (checkCircularDependency(request))
      return errorCircularDependency(request);
  }

  return null; // Not found circular dependency
}

export function isCallStackOverflowError(error: Error) {
  if (error.message === "Maximum call stack size exceeded") {
    return true;
  }
  if (error instanceof RangeError) {
    return true;
  }
  return false;
}
