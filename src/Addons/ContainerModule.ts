import { Id } from "../inversify";
import { Container } from "../IocContainer";
import { BindingToSyntax } from "../API/BindingOnSyntax";

export type ContainerModuleCallBack<W> = (
    bind: <T>(id:Id<T>) => ReturnType<typeof BindingToSyntax>,
    unbind: <T>(id:Id<T>) => void, 
    isBound: <T>(id:Id<T>) => boolean, 
    rebind: <T>(id:Id<T>) => ReturnType<typeof BindingToSyntax>
) => W;

export class ContainerModule {
    constructor(
        readonly registry: ContainerModuleCallBack<void>
    ) {} 
}
export class AsyncContainerModule {
    constructor(
        readonly registry: ContainerModuleCallBack<Promise<void>>
    ) {}
}

const _mapping = new WeakMap();

export function Loader(container: Container) {
    return {
        load(currentModule: ContainerModule) {
            function o(bindingToSyntax: any) {
                _mapping.set(bindingToSyntax._binding, currentModule);
                return bindingToSyntax;
            }
            return currentModule.registry(id => o(container.bind(id)), id => container.unbind(id), id => container.isBound(id), id => o(container.rebind(id)));
        },
        async loadAsync(currentModule: AsyncContainerModule) {
            await this.load(currentModule)
        },
        unload(currentModule: ContainerModule): void { 
            container.bindings.removeByCondition(item => _mapping.get(item) === currentModule);
        }
    }
}