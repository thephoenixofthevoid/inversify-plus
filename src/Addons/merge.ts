import { Container } from "../IocContainer";

export function merge(A: Container, B: Container, C = new Container()) {
  C.bindings.import(A.bindings);
  C.bindings.import(B.bindings);
  return C;
}