
import assert from "nanoassert"
import { ClassMetadata, LazyServiceIdentifer } from "./Base/ClassMetadata";
import { INJECTABLE, INJECT_TAG, MODE_TAG, MULTI_INJECT_TAG, NAMED_TAG, NAME_TAG, OPTIONAL_TAG, POST_CONSTRUCT, SERVICE_ID, UNMANAGED_TAG } from "./Constants";
import { Any } from "ts-toolbelt"


export interface Newable<T=any> { 
    new (...args: any[]): T; 
}
export interface Abstract<T=any> { 
    prototype: T;
}

export type Id<T=any> = string|symbol|Newable<T>|Abstract<T>;

export function injectable() {
    return function<T> (target: T) {
        const meta = ClassMetadata.of(target).at()
        assert(!meta.has(INJECTABLE), 
            "Cannot apply @injectable decorator multiple times.");
        meta.set(INJECTABLE, true)
        return target;
    }
}

export function postConstruct(target: any, propertyKey: string, descriptor: PropertyDescriptor) {
    const meta = ClassMetadata.of(target).at()
    assert(meta.has(POST_CONSTRUCT) === false, 
        "Cannot apply @postConstruct decorator multiple times in the same class");
    meta.set(POST_CONSTRUCT, propertyKey)
}

export function annotation(info: object) {
    return function(target: any, targetKey: string, targetIndex?: number) {
        const meta = ClassMetadata.of(target).find(targetKey, targetIndex)
        if (targetKey !== undefined) {
            meta.set(NAME_TAG, targetKey)
            meta.set(MODE_TAG, "ClassProperty")
        }
        if (targetIndex !== undefined) {
            meta.set(MODE_TAG, "ConstructorArgument")
        }
        for (const tagKey of Reflect.ownKeys(info)) {
            meta.set(tagKey, Reflect.get(info, tagKey))
        }
    }
}



export function tagged(tagKey: Any.Key, value: any) {
    return annotation({ [tagKey]: value })
}
export function unmanaged () { 
    return annotation({ [UNMANAGED_TAG]: true })
}
export function optional() { 
    return annotation({ [OPTIONAL_TAG]: true })
}
export function targetName(name: string) { 
    return annotation({ [NAME_TAG]: name })
}
export function named(name: Any.Key) {
    return annotation({ [NAMED_TAG]: name })
}
export function multiInject(id: Id) {
    return annotation({ 
        [MULTI_INJECT_TAG]: id,
        [SERVICE_ID]: id
    }) 
}
export function inject(id: Id | LazyServiceIdentifer) {
    if (id === undefined) throw new Error(`@inject called with undefined.`);
    return annotation({ 
        [INJECT_TAG]: id,
        [SERVICE_ID]: id
    }) 
}