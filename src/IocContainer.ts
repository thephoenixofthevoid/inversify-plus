import { Id } from "./inversify";
import { Context, EntityRequest } from "./Base/PlanElements";
import { TreeNode } from "./Base/TreeNode";
import { Binding } from "./Base/Binding";
import { Target } from "./Base/Target";
import { isCallStackOverflowError, tryFindingCircularDependency } from "./Errors";
import { EventEmitter } from "events";
import { MultiMap } from "./Base/MultiMap";
import { BindingToSyntax } from "./API/BindingOnSyntax";
import { Any } from "ts-toolbelt"
import { createTreeNode } from "./configureTree";


export function* iterateBindingsOfContainer(p: Container, id: Id) {
  do {
    yield* p.bindings.get(id);
    if (p.parent === null) break;
  } while ((p = p.parent));
}

export class Container extends EventEmitter {
  public bindings = new MultiMap<Id, Binding>();

  constructor(public parent: Container | null = null) {
    super();
  }

  bind<T>(id: Id<T>) {
    const binding = new Binding<T>(id);
    this.bindings.add(id, binding);
    return BindingToSyntax<T>(binding);
  }

  rebind<T>(id: Id<T>) {
    this.unbind(id);
    return this.bind(id);
  }

  unbind(id: Id): this {
    this.bindings.delete(id);
    return this;
  }

  unbindAll(): this {
    this.bindings.clear();
    return this;
  }

  isBound(id: Id): boolean {
    let container = this;
    do {
      if (container.bindings.has(id)) return true;
    } while ((container = container.parent as any));
    return false;
  }

  _get<T>(target: Target): T | T[] {
    var context = new Context(this);
    this.emit("target", context, target);
    try {
      context.root = createTreeNode(target, null, context);
      context.root.initNode()
      context.root.validateResults();
      context.root.initDeps();
    } catch (error) {
      const isOverflow = isCallStackOverflowError(error);
      const richError = isOverflow
        ? tryFindingCircularDependency(context.root)
        : null;
      throw richError || error;
    }

    this.emit("planned", context, target);
    const result = context.root.getResults()
    this.emit("resolved", context, target);
    return result;
  }
}


export function isBoundTagged(container: Container, id: Id, key: Any.Key, value: any): boolean {
  const context = new Context(container);
  const target = Target.for(id);
  target.set(key, value)
  const group = new TreeNode(null, context, target)
  const request = new EntityRequest(group, context, null as unknown as Binding, target);

  for (let binding of iterateBindingsOfContainer(container, id)) {
    if (!binding.constraint) continue;
    if (binding.constraint(request)) return true;
  }
  return false;
}