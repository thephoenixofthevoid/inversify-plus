import { MODE_TAG, NAME_TAG, SERVICE_ID } from "../Constants";
import { Id } from "../inversify";
import { Any } from "ts-toolbelt"
import { LazyServiceIdentifer } from "./ClassMetadata";

export type TargetType  = "ConstructorArgument" | "ClassProperty" | "Variable";

export class Target extends Map<Any.Key, any> {
    get type(): TargetType { 
        return this.get(MODE_TAG); 
    }
    get name(): string { 
        return this.get(NAME_TAG) || ""; 
    }
    get serviceIdentifier(): Id {
        const id = this.get(SERVICE_ID);
        if (id instanceof LazyServiceIdentifer)
            return id.unwrap();
        return id;
    }
    static for(token: Id) {
        const target = new Target()
        target.set(SERVICE_ID, token)
        return target
    }
}