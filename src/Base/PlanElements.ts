

import { Container } from "../IocContainer";
import { Target } from "./Target";
import { Binding } from "./Binding";
import { ResolvingStrategy } from "../Strategies";
import { ClassMetadata } from "./ClassMetadata";
import { createTreeNode } from "../configureTree";
import { TreeNode } from "./TreeNode";
import { InstanceResolvingStrategy } from "../Strategies/ResolvingStrategy";

export function resolveSubdependencies(request: TreeNode | EntityRequest) {
  if (!(request instanceof EntityRequest)) return;
  if (request.binding.type !== InstanceResolvingStrategy) return;
  if (request.binding.value === null) return;
  for (let dependency of ClassMetadata.getClassDependencies(request.binding.value)) {
    const node = createTreeNode(dependency, request, request.context);
    request.push(node);
    node.initNode()
    node.validateResults();
    node.initDeps();
  }
}

export function ResolverSingle (this: TreeNode) {
  const list = this.map(it => it.getResults());
  this.result = list.shift();
  return this.result;
}

export function ResolverGroup(this: TreeNode) {  
  this.result = this.map(it => it.getResults());
  return this.result;
}

export class Context {
  public root: TreeNode;
  constructor(readonly container: Container) {}
}

export class EntityRequest extends Array<EntityRequest | TreeNode> {

  get ancerstor(): EntityRequest | null {
    let current: EntityRequest | TreeNode = this;
    do {
      if (current.parent === null) return null;
      current = current.parent;
    } while (!(current instanceof EntityRequest));
    return current;
  }

  public result: any = null;

  getResults() {
    if (this.binding === undefined) return undefined;
    const caching = new this.binding.scope(this)
    this.result = caching.getCache()
    if (this.result) return this.result;
      
    const resolver = new this.binding.type(this)
    resolver.runResover();
    if (this.binding.onActivation) {
      this.result = this.binding.onActivation(this.context, this.result);
    }
    caching.setCache(this.result);
    return this.result;
  }

  constructor(
    public parent: EntityRequest|TreeNode|null,
    public context: Context,
    public binding: Binding<any>,
    public target: Target
  ) {
    super();
  }
}
