import { Any } from "ts-toolbelt"
import { Id } from "../inversify";
import { CachingStrategy, TransientScoped } from "../Strategies/CachingStrategy";
import { InvalidResolvingStrategy, ResolvingStrategy } from "../Strategies/ResolvingStrategy";
import { Context, EntityRequest } from "./PlanElements";
import { TreeNode } from "./TreeNode";

export interface ConstraintFunction extends Function { 
    metaData?: { key: Any.Key, value: any }; 
    (request: EntityRequest | TreeNode | null): boolean; 
}

export interface ActivationFunction<T> extends Function { 
    (context: Context, injectable: T): T; 
}

export interface IResolvingStrategy {
    new (ctx: EntityRequest): ResolvingStrategy
    tag: string;
}

export interface ICachingStrategy {
    new (ctx: EntityRequest): CachingStrategy
}


export type BindingType = "ConstantValue" | "DynamicValue" | "Factory" | "Instance" | "Invalid" | "Provider";

export class Binding<T = any> {
    serviceIdentifier: Id<T>
    scope: ICachingStrategy =  TransientScoped;
    type: IResolvingStrategy = InvalidResolvingStrategy;
    value: any = null;
    constraint?: ConstraintFunction;
    onActivation?: ActivationFunction<T>;
    
    constructor(serviceIdentifier: Id<T>) {
        this.serviceIdentifier = serviceIdentifier
    }
}


