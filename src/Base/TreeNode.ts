import { Target } from "./Target";
import { Binding } from "./Binding";
import { EntityRequest, Context, resolveSubdependencies } from "./PlanElements";


export class TreeNode extends Array<EntityRequest> {
  public result: any = null;
  public filterer: any = null;
  public validator: any = null;
  public resolver: any = null;

  public extractor(): Binding[] {
    return [];
  }

  constructor(public parent: EntityRequest | null, public context: Context, public target: Target) {
    super();
  }

  public initDeps() {
    for (let request of this) {
      resolveSubdependencies(request);
    }
  }

  public initNode() {
    if (this.length)
      throw new Error("Runtime error");

    for (let binding of this.extractor()) {
      const request = new EntityRequest(this, this.context, binding, this.target);
      if (this.filterer(request))
        this.push(request);
    }
  }

  public validateResults() {
    return this.validator.call(this);
  }

  public getResults() {
    return this.resolver.call(this);
  }
}
