import { Any } from "ts-toolbelt"
import { Newable, Id } from "../inversify";
import { Layer, keysIn, upsertIn, getIn } from "./NestedMap";
import { INJECT_TAG, MODE_TAG, INJECTABLE, SERVICE_ID, UNMANAGED_TAG } from "../Constants";
import { getFunctionName } from "../Errors";
import { Target } from "../Base/Target";

const METADATA_SYMBOL = Symbol.for("inversify::metadata:alt");

function ensureConstructor(target: Newable|InstanceType<Newable>) {
    if (typeof target === "function") return target;
    return target.constructor;
}

export class ClassMetadata {
    readonly metadata = new Layer()
    private constructor(public target: Newable) {}

    getValue(key: Any.Key) {
        for (let it of getRelated(this.target)) {
            const value = it.at().get(key)
            if (value) return value
        }
    }

    static of(target: Newable|InstanceType<Newable>): ClassMetadata {
        target = ensureConstructor(target);
        if (!Reflect.hasOwnMetadata(METADATA_SYMBOL, target)) 
            Reflect.defineMetadata(METADATA_SYMBOL, new ClassMetadata(target), target)
        return Reflect.getOwnMetadata(METADATA_SYMBOL, target);
    }

    public find(key: string|undefined, index: number|undefined) {
        const path = []
        if (key   !== undefined) path.push("properties", key  );
        if (index !== undefined) path.push("arguments" , index);
        return this.at(...path)
    }

    public at(...path: Any.Key[]) {
        return upsertIn(this.metadata, path, Map as Newable<Map<any, any>>)
    }

    *getMetadata() {
        yield* getConstructorArgsMetadata(this.target);
        yield* getPropertiesMetadata(this.target);
    }

    static *getClassDependencies(target: Newable) {
        const meta = ClassMetadata.of(target)
        if (!meta.at().has(INJECTABLE))
            throw new Error(`Missing required @injectable annotation in: ${getFunctionName(target)}.`);
        for (let metadata of meta.getMetadata()) {
            if (metadata.get(UNMANAGED_TAG)) continue;
            yield new Target(metadata)
        }
    }
}


function* getRelated(ctor: Newable) {
    while (ctor && (ctor !== Object)) {
        yield ClassMetadata.of(ctor)
        ctor = Object.getPrototypeOf(ctor.prototype).constructor;
    }
}

function *getPropertiesMetadata(target: Newable) {
    for (const o of getRelated(target)) {
        for (const key of keysIn(o.metadata, [ "properties" ])) {
            const metadata = getIn(o.metadata, [ "properties", key ])
            if (metadata) yield metadata
        }
    }
}

function getConstructorArgsMetadata(target: Newable) {
    const outerList: any[] = []
    for (const o of getRelated(target)) {
        for (const key of keysIn(o.metadata, [ "arguments" ])) {
            if (!outerList[key as number]) 
                 outerList[key as number] = getIn(o.metadata, [ "arguments", key ])
        }
    }
    (Reflect.getMetadata("design:paramtypes", target) || []).forEach((V: any, I: number) => {
        if (outerList[I]) return;
        if (V === Object || V === Function || V === undefined)
                throw new Error(`Missing required @inject or @multiInject annotation in: argument ${I} in class ${getFunctionName(target)}.`);
        outerList[I] = new Map([[ INJECT_TAG, V ], [ SERVICE_ID, V ], [ MODE_TAG , "ConstructorArgument" ]])
    });
    return outerList
}

export class LazyServiceIdentifer<T = any> { 
    constructor(readonly unwrap: () => Id<T>) {}
}