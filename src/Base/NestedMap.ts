import { inspect } from "util";
import { Newable } from "../inversify";
import { Any } from "ts-toolbelt"

export class Layer extends Map<Any.Key, Layer> {
    value?: Map<any, any>;

    [inspect.custom]() {
        const result: any = new Map(this.value as any);
        for (let [key, value] of this.entries()) 
            result[key] = value;
        return result;
    }
}

export function getIn(map: Layer, keys: Any.Key[]) {
    for (const key of keys) {
        if (key === undefined) continue;
        let next = map.get(key);
        if (!next) return;
        map = next;
    }
    return map.value;
}

export function keysIn(map: Layer, keys: Any.Key[]) {
    for (const key of keys) {
        if (key === undefined) continue;
        let next = map.get(key);
        if (!next) return [];
        map = next;
    }
    return [ ...map.keys() ];
}

export function setIn(map: Layer, keys: Any.Key[], value: any) {
    for (const key of keys) {
        if (key === undefined) continue;
        let next = map.get(key);
        if (next === undefined) 
            map.set(key, (next = new Layer()));
        map = next;
    }
    map.value = value;
}

export function upsertIn<T>(map: Layer, keys: Any.Key[], constr: Newable<T>) {
    let value = getIn(map, keys);

    if (value === undefined) {
        const next = new constr()
        setIn(map, keys, next);
        return next;
    }

    return value;
}
