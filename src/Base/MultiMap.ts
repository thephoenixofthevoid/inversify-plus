export class MultiMap<K = any, V = any> {
  map: Map<K, Set<V>> = new Map();

  constructor(...iterables: Iterable<[K, V]>[]) {
    for (let iterable of iterables) {
      this.import(iterable);
    }
  }

  public import(iterable: Iterable<[K, V]>) {
    for (const [k, v] of iterable) {
      this.add(k, v);
    }
  }

  public get(key: K): V[] {
    const values = this.map.get(key);
    return values ? [...values] : [];
  }

  public add(key: K, value: V): MultiMap<K, V> {
    let values = this.map.get(key);
    if (!values) {
      values = new Set();
      this.map.set(key, values);
    }
    values.add(value);
    return this;
  }

  public has(key: K): boolean {
    return this.map.has(key);
  }

  public delete(key: K, value?: V): boolean {
    const values = this.map.get(key);
    if (!values) return false;
    if (!value) return this.map.delete(key);

    const deleted = values.delete(value);
    if (!values.size) this.map.delete(key);
    return deleted;
  }

  public keys(): IterableIterator<K> {
    return this.map.keys();
  }

  public get size() {
    return this.map.size;
  }

  public values(): IterableIterator<V> {
    return iterateMultiMapAsValues(this.map);
  }

  public entries(): IterableIterator<[K, V]> {
    return iterateMultiMapAsEntries(this.map);
  }

  public [Symbol.iterator](): IterableIterator<[K, V]> {
    return iterateMultiMapAsEntries(this.map);
  }

  public clear(): void {
    this.map.clear();
  }

  public forEach(callback: (value: V, key: K, map: MultiMap<K, V>) => boolean) {
    for (let [key, value] of this.entries()) {
      callback(value, key, this);
    }
  }

  public removeByCondition(condition: (value: V, key: K) => boolean) {
    removeByCondition(this, condition);
  }
}

function* iterateMultiMapAsEntries<K, V>(
  map: Map<K, Set<V>>
): Generator<[K, V]> {
  for (let [key, values] of map.entries()) {
    for (let value of values) yield [key, value] as [K, V];
  }
}

function* iterateMultiMapAsValues<K, V>(map: Map<K, Set<V>>): Generator<V> {
  for (let [key, values] of map.entries()) {
    for (let value of values) yield value;
  }
}

function removeByCondition<K, V>(
  multimap: MultiMap<K, V>,
  condition: (value: V, key: K) => boolean
) {
  for (let [key, value] of multimap.entries()) {
    if (condition(value, key)) {
      multimap.delete(key, value);
    }
  }
}
