import { Any } from "ts-toolbelt"
import { Id, Newable } from "../inversify";
import { INJECT_TAG, MODE_TAG, MULTI_INJECT_TAG, NO_CONSTRAINTS, SERVICE_ID } from "../Constants";
import { Container } from "../IocContainer";
import { Target } from "../Base/Target";

export function Resolver(container: Container) {
  return {
    get<T>(id: Id<T>): T {
      const target = new Target([
        [INJECT_TAG, id], 
        [MODE_TAG, "Variable"], 
        [SERVICE_ID, id ]
      ])
      return container._get<T>(target) as T;
    },
    getTagged<T>(id: Id<T>, key: Any.Key, value: any): T {
      const target = new Target([
        [INJECT_TAG, id], 
        [MODE_TAG, "Variable"], 
        [SERVICE_ID, id ],
        [key, value]
      ])
      return container._get<T>(target) as T;
    },
    getAll<T>(id: Id<T>): T[] {
      const target = new Target([
        [MULTI_INJECT_TAG, id], 
        [MODE_TAG, "Variable"], 
        [SERVICE_ID, id ],
        [NO_CONSTRAINTS, true]
      ])
      return container._get<T>(target) as T[];
    },
    getAllTagged<T>(id: Id<T>, key: Any.Key, value: any): T[] {
      const target = new Target([
        [MULTI_INJECT_TAG, id], 
        [MODE_TAG, "Variable"], 
        [SERVICE_ID, id ],
        [key, value]
      ])
      return container._get<T>(target) as T[];
    },
    resolve<T>(service: Newable<T>) {
      const t = new Container(container);
      t.bind<T>(service).toSelf();

      const target = new Target([
        [INJECT_TAG, service], 
        [MODE_TAG, "Variable"], 
        [SERVICE_ID, service ]
      ])
      return t._get<T>(target) as T;
    }
  };
}
