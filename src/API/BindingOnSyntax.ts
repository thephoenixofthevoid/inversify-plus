import { Context } from "../Base/PlanElements";
import { Binding, ConstraintFunction } from "../Base/Binding";
import { Any } from "ts-toolbelt"
import { taggedConstraint, atParent, atAncestor, noAncestor, typeConstraint, isDefault } from "../Strategies/Constraints";
import { TransientScoped, SingletonScoped, RequestScoped } from "../Strategies/CachingStrategy";
import { ConstantValueResolvingStrategy, InstanceResolvingStrategy, DynamicValueResolvingStrategy } from "../Strategies/ResolvingStrategy";
export type Factory<T = any> = (...args: any[]) => ((...args: any[]) => T) | T;


export const getSyntaxes = <T = any>(binding: Binding<T>) => {

    function assertToSelf<T>(id: Binding<T>) {
        if (typeof id.serviceIdentifier !== "function")
            throw new Error(
                "The toSelf function can only be applied when a constructor is used as service identifier"
            );
        return id.serviceIdentifier;
    }

    const toSyntax = {
        _binding: binding,
        to: (constructor: new (...args: any[]) => T) => {
            binding.type = InstanceResolvingStrategy
            binding.value = constructor as any
            return syntaxes.inWhenOnSyntax
        },
        toSelf: () => {
            binding.type = InstanceResolvingStrategy
            binding.value = assertToSelf<any>(binding)
            return syntaxes.inWhenOnSyntax
        },
        toConstantValue: (V: T) => {
            binding.type = ConstantValueResolvingStrategy
            binding.value = V as any
            return syntaxes.whenOnSyntax
        },
        toDynamicValue: <T2>(V: (context: Context) => T2) => {
            binding.type = DynamicValueResolvingStrategy
            binding.value = V as any
            return syntaxes.inWhenOnSyntax
        },
        toFactory: <T2>(V: (context: Context) => Factory<T2>) => {
            binding.type = DynamicValueResolvingStrategy
            binding.value = V as any
            return syntaxes.whenOnSyntax
        },
        toProvider: <T2>(V: (context: Context) => Factory<Promise<T2>>) => {
            binding.type = DynamicValueResolvingStrategy
            binding.value = V as any
            return syntaxes.whenOnSyntax
        }
    }

    const inSyntax = {
        _binding: binding,
        inRequestScope: () => {
            binding.scope = RequestScoped
            return syntaxes.whenOnSyntax
        },
        inSingletonScope: () => {
            binding.scope = SingletonScoped
            return syntaxes.whenOnSyntax
        },
        inTransientScope: () => {
            binding.scope = TransientScoped
            return syntaxes.whenOnSyntax
        }
    }

    const onSyntax = {
        _binding: binding,
        onActivation: (handler: (context: Context, injectable: T) => T) => {
            binding.onActivation = handler;
        }
    }

    const whenSyntax = {
        _binding: binding,
        when: (constraint: ConstraintFunction) => {
            binding.constraint = constraint
            return syntaxes.onSyntax
        },
        whenParent: (constraint: ConstraintFunction) => {
            binding.constraint = atParent(constraint)
            return syntaxes.onSyntax
        },
        whenAnyAncestorMatches: (constraint: ConstraintFunction) => {
            binding.constraint = atAncestor(constraint)
            return syntaxes.onSyntax
        },
        whenNoAncestorMatches: (constraint: ConstraintFunction) => {
            binding.constraint = noAncestor(constraint)
            return syntaxes.onSyntax
        },
        whenTargetTagged: (tag: Any.Key, value: any) => {
            binding.constraint = taggedConstraint(tag, value)
            return syntaxes.onSyntax
        },
        whenParentTagged: (tag: Any.Key, value: any) => {
            binding.constraint = atParent(taggedConstraint(tag, value))
            return syntaxes.onSyntax
        },
        whenAnyAncestorTagged: (tag: Any.Key, value: any) => {
            binding.constraint = atAncestor(taggedConstraint(tag, value))
            return syntaxes.onSyntax
        },
        whenNoAncestorTagged: (tag: Any.Key, value: any) => {
            binding.constraint = noAncestor(taggedConstraint(tag, value))
            return syntaxes.onSyntax
        },
        whenInjectedInto: (ancestor: (Function | string)) => {
            binding.constraint = atParent(typeConstraint(ancestor))
            return syntaxes.onSyntax
        },
        whenAnyAncestorIs: (ancestor: (Function | string)) => {
            binding.constraint = atAncestor(typeConstraint(ancestor))
            return syntaxes.onSyntax
        },
        whenNoAncestorIs: (ancestor: (Function | string)) => {
            binding.constraint = noAncestor(typeConstraint(ancestor))
            return syntaxes.onSyntax
        },
        whenTargetIsDefault: () => {
            binding.constraint = isDefault
            return syntaxes.onSyntax
        },
    }

    const syntaxes = {
        whenSyntax,
        onSyntax,
        inSyntax,
        inWhenOnSyntax: {
            ...inSyntax,
            ...whenSyntax,
            ...onSyntax
        },
        whenOnSyntax: {
            ...whenSyntax,
            ...onSyntax
        },
        toSyntax
    }

    return syntaxes
}

export const BindingToSyntax = <T = any>(B: Binding<T>) => {
    return getSyntaxes(B).toSyntax
};
