const ServiceTagSet = new Set<symbol>()

export function ServiceTag(description: string): symbol {
    const it = Symbol(description);
    ServiceTagSet.add(it)
    return it;
}

export function isServiceTag(tag: any): boolean {
    return ServiceTagSet.has(tag)
}

export const INJECTABLE        = ServiceTag("injectable");                    //
export const MODE_TAG          = ServiceTag("dislocation")
export const NAMED_TAG         = ServiceTag("named");                    // Used for named bindings
export const NAME_TAG          = ServiceTag("name");                     // The name of the target at design time
export const UNMANAGED_TAG     = ServiceTag("unmanaged");                // The for unmanaged injections (in base classes when using inheritance)
export const INJECT_TAG        = ServiceTag("inject");                   // The type of the binding at design time
export const MULTI_INJECT_TAG  = ServiceTag("multi_inject");             // The type of the binding at design type for multi-injections
export const OPTIONAL_TAG      = ServiceTag("optional");                 // The for optional injections
export const POST_CONSTRUCT    = ServiceTag("post_construct");           // used to identify postConstruct functions
export const NO_CONSTRAINTS    = ServiceTag("avoid_constraints");        // used to identify postConstruct functions
export const SERVICE_ID        = ServiceTag("SERVICE_ID");        // used to identify postConstruct functions