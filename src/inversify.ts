export { Container as Container } from "./IocContainer";
export * from "./Strategies/Constraints";
export * from "./decorators";

export function decorate(decorator: ClassDecorator|ParameterDecorator|MethodDecorator, target: any, par?: number | string): void {
    if (typeof par === "number") return void Reflect.decorate([ function (target: any, key: string) {  (decorator as ParameterDecorator)(target, key, par);  } as any], target);
    if (typeof par === "string") return void Reflect.decorate([decorator as MethodDecorator], target, par);
    return void Reflect.decorate([decorator as ClassDecorator], target);
}