import { NO_CONSTRAINTS } from "./Constants";
import { Target } from "./Base/Target";
import { Id } from "./inversify";
import { Binding } from "./Base/Binding";
import { Container } from "./IocContainer";
import { MULTI_INJECT_TAG, OPTIONAL_TAG } from "./Constants";
import { AmbiguousDependencyError, UnsatisfiedDependencyError } from "./Errors";
import { EntityRequest, ResolverGroup, ResolverSingle, Context } from "./Base/PlanElements";
import { TreeNode } from "./Base/TreeNode";


function getBindingChecker(target: Target) {
  if (target.has(NO_CONSTRAINTS)) return skip;
  return check;
  function skip(request: EntityRequest) {
    return true;
  }
  function check(request: EntityRequest) {
    if (request.binding.constraint) {
      return request.binding.constraint(request);
    }
    return true;
  }
}

function getNodeValidator(target: Target) {
  const isOptional = target.has(OPTIONAL_TAG);
  const isMultiple = target.has(MULTI_INJECT_TAG);
  if (!isOptional && !isMultiple)  return checkExistsUnambiguousMatch;
  if (isOptional && isMultiple)   return checkNothing;
  if (!isOptional && isMultiple)  return checkExistsMatch;
  if (isOptional && !isMultiple)  return checkNoAmbiguousMatch;
  throw new Error("Unexpected");
}
function checkNothing(this: TreeNode) {
  // Empty
}
function checkExistsUnambiguousMatch(this: TreeNode) {
  checkExistsMatch.call(this);
  checkNoAmbiguousMatch.call(this);
}
function checkExistsMatch(this: TreeNode) {
  if (this.length === 0)
    throw new UnsatisfiedDependencyError(this);
}
function checkNoAmbiguousMatch(this: TreeNode) {
  if (this.length > 1)
    throw new AmbiguousDependencyError(this);
}


function getResolver(target: Target) {
  if (target.has(MULTI_INJECT_TAG)) {
    return ResolverGroup
  } else {
    return ResolverSingle
  }
}


function getExtractor(target: Target) {
  if (target.has(MULTI_INJECT_TAG)) {
    return function () {
      let container = this.context.container as Container;
      let id = this.target.serviceIdentifier as Id;
      let bindings: Binding[] = [];
      do {
        bindings.unshift(...container.bindings.get(id));
      } while (container = container.parent as any);

      return bindings;
    };
  } else {
    return function () {
      let container = this.context.container as Container;
      let id = this.target.serviceIdentifier as Id;
      do {
        let bindings = container.bindings.get(id);
        if (bindings.length)
          return bindings;
      } while (container = container.parent as any);
      return [];
    };
  }
}

export function createTreeNode(target: Target, parent: EntityRequest | null, context: Context) {
  const node = new TreeNode(parent, context, target)

  node.extractor = getExtractor(target)
  node.resolver = getResolver(target)
  node.filterer = getBindingChecker(target)
  node.validator = getNodeValidator(target)

  return node; 
}